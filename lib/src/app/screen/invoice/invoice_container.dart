import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'invoice_screen/invoice_screen.dart';
import 'invoice_screen/models/invoice_profile_detail.dart';
import 'invoice_screen/models/invoice_status_model.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
class invoiceScreen extends  StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _invoiceScreenState();
  }

}

class _invoiceScreenState extends State<invoiceScreen> with BaseApi{
  List<InvoiceProfileModel> invoiceList = [];
  List<InvoiceStatusModel> invoiceListStatusList = [];
  List<InvoiceProfileModel> tempinvoiceList = [];
  InvoiceProfileModel _invoiceDetail;
  int  invoiceStatusId = 4;

  @override
  void initState() {
    token = Provider.of<UserState>(context,listen: false).getToken();
    super.initState();
    callInvoiceList();
    callInvoiceStatus();
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text('Invoice',style: TextStyle(
            color: Colors.black
          ),),
        ),
        // body: invoiceTabScreen(),
        // _invoiceList
        body: _invoiceList(),
      ),
    );
  }

  Widget _invoiceList(){
    return Column(
      children: <Widget>[
        dropDownInvoiceStatus(),
        Expanded(
          child: Content(),
        )        
      ],
    );
  }

  Widget dropDownInvoiceStatus(){
    return  Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        margin: EdgeInsets.all(15.0),
        decoration: BoxDecoration(              
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(
          color: Colors.black, style: BorderStyle.solid, width: 0.80),
        ),
        child: DropdownButton(
          isExpanded: true,
          hint: Text('Select Work Order Type'),
          items: invoiceListStatusList.map<DropdownMenuItem<int>>((value) {
            return DropdownMenuItem<int>(
              value: value.id,
              child: Text('${value.name}',textAlign: TextAlign.right,),
            );
          }).toList(),
          onChanged: (id) {
              invoiceStatusId = id;
              if(id == 4){
                invoiceList = tempinvoiceList;
              } else {
                invoiceList = tempinvoiceList.where( (invoice) => invoice.invoiceStatusId == id).toList();
            }

            sortArrayFromUnpiadFirst();
            setState(() {

            });
          },
          value: invoiceStatusId,
      ),
    );
  }

  Widget Content(){
    return invoiceList.isEmpty ? Text('No Data To Display') : ListView.builder(
      itemCount: invoiceList.length,
      itemBuilder: (BuildContext context,int index) {
        return GestureDetector(
          onTap: () async {
              await getInvoiceAPI(invoiceList[index].id);
              Navigator.push(context,
                MaterialPageRoute(builder: (context) => Invoice(invoice: _invoiceDetail,))
              );
          },
          child:Card(
            child: Container(
            height: 100,
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
                ),
                child: Container(
                  // color: Colors.red,
                  padding: EdgeInsets.all(5),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[                        
                        maincontent(invoiceList[index]),
                    ],
                  )
                ),
            ),
           )        ,
          )
        );
      },
    );
  }   

  Widget maincontent(InvoiceProfileModel invoice){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        // mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,            
            children: <Widget>[
              RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Invoice No.: '),
              TextSpan(text: '${invoice.invoiceNo}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
              Text('${invoice.totalAmount.toStringAsFixed(2)}',style: TextStyle(
                color: Colors.black,
                fontSize: 20,
                fontWeight: FontWeight.bold
              ),),
            ],
          ),
          Container(margin: EdgeInsets.only(bottom: 10.0),),
          RichText(
            text: TextSpan(
            style: TextStyle(
              color: Colors.black,
              fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Due Date To: '),
              TextSpan(text: '${invoice.dueDate}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
          Container(margin: EdgeInsets.only(bottom: 10.0),),
          
          RichText(
            text: TextSpan(
            style: TextStyle(
              color: Colors.black,
              fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Status: '),
              TextSpan(text: '${invoice.invoiceStatusName}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
        ],
      );
  }

getInvoiceAPI(id) async {
    String url = '${baseAPi}papi/invoice';
    Map data = {"apiRequest":{"action":"get"},"data":[{
      "id": id
    }]};
    // print(widget.invoice.id);
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
      },
    );
    print(json.decode(response.body));
    if (json.decode(response.body)['data'].length == 0) {
      showDialog(
              context: context,
              barrierDismissible: true,
              builder: (BuildContext context){
                return AlertDialog(
                  content: Text('No data from server'),
              );
          }
      );
    } else {
      _invoiceDetail = InvoiceProfileModel.fromJson(json.decode(response.body)['data'][0]);
    }
    
    
    print(_invoiceDetail.invoiceItems.length);
    setState(() {
      
    });
  }



callInvoiceStatus() async {
    String url = '${baseAPi}papi/invoicestatus';
    Map data = {"apiRequest":{"action":"list"}};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {"Content-Type": "application/json"},
    );
    (json.decode(response.body)['data'] as List).forEach((parcelStatus){      
      invoiceListStatusList.add(InvoiceStatusModel.fromJson(parcelStatus));
    });
    
    setState(() {});
  }


  callInvoiceList() async {
    String url = '${baseAPi}papi/invoice';
    Map data = {"apiRequest":{"action":"list-mobile"},"data":[{
          // "renterProfileId":currentUser.renterId
    }]};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
        },
    );
    print(response.statusCode);
    (json.decode(response.body)['data'] as List).forEach((parcel){
      invoiceList.add(InvoiceProfileModel.fromJson(parcel));        
    });
  tempinvoiceList = invoiceList;
  sortArrayFromUnpiadFirst();
  setState(() {
    
  });    
  }

  sortArrayFromUnpiadFirst(){
    invoiceList.sort((a,b) => a.invoiceStatusCode == "OUTSTANDING" ? 1 : -1);
  }

}