import 'package:chartium/src/app/models/renter_profile_model.dart';
import 'package:chartium/src/app/providers/renter_profile_provider.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'invoice_screen.dart';

import 'dart:convert';
import 'package:http/http.dart' as http;

import 'models/invoice_profile_detail.dart';
class historyScreen extends  StatefulWidget {
  @override
  _historyScreenState createState() => _historyScreenState();
}

class _historyScreenState extends State<historyScreen>  with BaseApi {
  List<InvoiceProfileModel> invoices = [];
  
  InvoiceProfileModel _invoiceDetail;
  UserModel currentUser;
  

  @override 
  void initState() {
    token = Provider.of<UserState>(context,listen: false).getToken();
    print(token);
    callHttp();
    super.initState();
  }

  callHttp() async {
    String url = '${baseAPi}papi/invoice';
    Map data = {"apiRequest":{"action":"list-mobile"}};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
      },
    );
    print(json.decode(response.body));
    (json.decode(response.body)['data'] as List).forEach((invoice){      
      if (InvoiceProfileModel.fromJson(invoice).invoiceStatusId == 3) {
          invoices.add(InvoiceProfileModel.fromJson(invoice));  
      }      
    });
    if (!mounted) return;
    setState(() {
    });
  }

  getInvoiceAPI(id) async {
    String url = '${baseAPi}papi/invoice';
    Map data = {"apiRequest":{"action":"get"},"data":[{
      "id": id
    }]};
    // print(widget.invoice.id);
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
      },
    );
    print(response.statusCode);
    print(json.decode(response.body));
    _invoiceDetail =InvoiceProfileModel.fromJson(json.decode(response.body)['data'][0]);
    print(_invoiceDetail.invoiceItems.length);
    setState(() {
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return Content(context);
  } 

  Widget Content(BuildContext context){
  //  List<int> list = List.generate(100, (i) => i+1);
    return invoices.isEmpty ? Text('No Data To Display') : ListView.builder(
      itemCount: invoices.length,
      itemBuilder: (BuildContext context,int index) {
        return GestureDetector(
          onTap: () async {
            await getInvoiceAPI(invoices[index].id);
              Navigator.push(context,
                MaterialPageRoute(builder: (context) => Invoice(invoice: _invoiceDetail)),
              );
          },
          child:Card(
            child: Container(
            height: 100,
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
                ),
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[                        
                        maincontent(invoices[index]),
                    ],
                  )
                ),
            ),
           )        ,
          )
        );
      },
    );
  }   

  Widget maincontent(InvoiceProfileModel invoice){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              // Text('${invoice.id}'),
              // Text('${_renterProfile.getFullName()}'),
              Text('Invoice No. :${invoice.roomNo}'),
            ],
          ),
          Text('Room No: ${invoice.roomNo}'),
          // Text('DueDate To: ${invoice.billingNo}'),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              // Text('${invoice.getTotalBilling()}')
            ],
          )
        ],
      );
  }
}