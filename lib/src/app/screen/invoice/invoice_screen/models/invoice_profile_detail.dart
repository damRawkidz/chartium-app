import 'package:intl/intl.dart';
class InvoiceProfileModel {
  String invoiceNo;
  String name;
  int roomId;
  String roomNo;
  String issueDate;
  String dueDate;
  String paidDate;
  int invoiceStatusId;
  String invoiceStatusCode;
  String invoiceStatusName;
  double totalAmount;
  List<InvoiceItems> invoiceItems;
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;
  String amount;
  var formatter = DateFormat("dd-MM-yyyy");
  final numberFormatter = NumberFormat("#,###.##");
  InvoiceProfileModel(
      {this.invoiceNo,
      this.name,
      this.roomId,
      this.roomNo,
      this.issueDate,
      this.dueDate,
      this.paidDate,
      this.invoiceStatusId,
      this.invoiceStatusCode,
      this.invoiceStatusName,
      this.totalAmount,
      this.invoiceItems,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.lastUpdateDate});

  String getTotalBilling(){
    int total = 0;
    this.invoiceItems.forEach((f) => {
          total += f.amount.toInt()
    });
    return   total.toStringAsFixed(2);
  }

  // getBalanceDue(){
  //   int total = 0;
  //   this.invoiceItems.forEach((f) => {
  //         total += f.amount.toInt()
  //   });
  //   int balanceDue = invoiceDetailsModels[0].balance - total;
  //   return balanceDue.toStringAsFixed(2);
  // }

  InvoiceProfileModel.fromJson(Map<String, dynamic> json) {
    invoiceNo = json['invoiceNo'];
    name = json['name'];
    roomId = json['roomId'];
    roomNo = json['roomNo'];
    issueDate = json['issueDate'] == null ? '' :  formatter.format(DateTime.parse((json['issueDate'] as String).substring(0,10))) ;
    dueDate = json['dueDate'] == null ? '' : formatter.format(DateTime.parse((json['dueDate'] as String).substring(0,10)));
    paidDate = json['paidDate']  == null ? '' : formatter.format(DateTime.parse((json['paidDate'] as String).substring(0,10))) ;
    invoiceStatusId = json['invoiceStatusId'];
    invoiceStatusCode = json['invoiceStatusCode'];
    invoiceStatusName = json['invoiceStatusName'];
    totalAmount =json['totalAmount'];
    amount =  numberFormatter.format(json['totalAmount']);
    if (json['invoiceItems'] != null) {
      invoiceItems = new List<InvoiceItems>();
      json['invoiceItems'].forEach((v) {
        invoiceItems.add(new InvoiceItems.fromJson(v));
      });
    }
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['invoiceNo'] = this.invoiceNo;
    data['name'] = this.name;
    data['roomId'] = this.roomId;
    data['roomNo'] = this.roomNo;
    data['issueDate'] = this.issueDate;
    data['dueDate'] = this.dueDate;
    data['paidDate'] = this.paidDate;
    data['invoiceStatusId'] = this.invoiceStatusId;
    data['invoiceStatusCode'] = this.invoiceStatusCode;
    data['invoiceStatusName'] = this.invoiceStatusName;
    data['totalAmount'] = this.totalAmount;
    if (this.invoiceItems != null) {
      data['invoiceItems'] = this.invoiceItems.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    return data;
  }
}

class InvoiceItems {
  int invoiceId;
  String name;
  double amount;
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;

  InvoiceItems(
      {this.invoiceId,
      this.name,
      this.amount,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.lastUpdateDate});

  InvoiceItems.fromJson(Map<String, dynamic> json) {
    invoiceId = json['invoiceId'];
    name = json['name'];
    amount = json['amount'];
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['invoiceId'] = this.invoiceId;
    data['name'] = this.name;
    data['amount'] = this.amount;
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    return data;
  }
}

