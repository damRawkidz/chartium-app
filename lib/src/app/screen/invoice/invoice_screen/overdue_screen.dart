import 'package:chartium/src/app/models/renter_profile_model.dart';
import 'package:chartium/src/app/providers/renter_profile_provider.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


import 'dart:convert';
import 'package:http/http.dart' as http;
import 'invoice_screen.dart';
import 'models/invoice_profile_detail.dart';
class overDuleScreen extends  StatefulWidget {
  @override
  _overDuleScreenState createState() => _overDuleScreenState();
}

class _overDuleScreenState extends State<overDuleScreen>  with BaseApi {
  List<InvoiceProfileModel> invoices = [];
  UserModel currentUser;
  RenterProfileModel _renterProfile;

  @override
  Widget build(BuildContext context) {
    return Content(context);
  } 

  @override 
  void initState() {
    // currentUser = Provider.of<UserState>(context,listen: false).getUser();
    token = Provider.of<UserState>(context,listen: false).getToken();
    _renterProfile = Provider.of<RenterProfileProvider>(context,listen: false).getRenterProfile();
    callHttp();
    super.initState();
  }

  callHttp() async {
    String url = '${baseAPi}papi/invoice';
    Map data = {"apiRequest":{"action":"list-mobile"}};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
      },
    );
    print(json.decode(response.body));
    (json.decode(response.body)['data'] as List).forEach((invoice){      
      if (InvoiceProfileModel.fromJson(invoice).invoiceStatusId == 2) {
          invoices.add(InvoiceProfileModel.fromJson(invoice));  
      }      
    });
    if (!mounted) return;
    setState(() {
    });
  }

  Widget Content(BuildContext context){
    return invoices.isEmpty ? Text('No Data To Display') : ListView.builder(
      itemCount: invoices.length,
      itemBuilder: (BuildContext context,int index) {
        return GestureDetector(
          onTap: (){
              Navigator.push(context,
                MaterialPageRoute(builder: (context) => Invoice(invoice: invoices[index],)),
              );
          },
          child:Card(
            child: Container(
            height: 100,
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
                ),
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Column(
                    // crossAxisAlignment: CrossAxisAlignment.start,
                    // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[                        
                        maincontent(invoices[index]),
                    ],
                  )
                ),
            ),
           )        ,
          )
        );
      },
    );
  }   

  Widget maincontent(InvoiceProfileModel invoice){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              // Text('${invoice.id}'),
              Text('${_renterProfile.getFullName()}'),
              Text('Invoice No. :${invoice.invoiceNo}'),
            ],
          ),
          Text('Room No: ${invoice.roomNo}'),
          // Text('DueDate To: ${invoice.billingNo}'),
          Row(
            mainAxisAlignment: MainAxisAlignment.end,
            children: <Widget>[
              // Text('${invoice.getTotalBilling()}')
            ],
          )
        ],
      );
  }
}