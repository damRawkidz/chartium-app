import 'dart:convert';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'models/invoice_profile_detail.dart';

class Invoice extends StatefulWidget  {
  InvoiceProfileModel invoice;

  Invoice({Key key,@required this.invoice}):super(key:key);

  @override
  _InvoiceState createState() => _InvoiceState();
}

class _InvoiceState extends State<Invoice>  with BaseApi{
  UserModel currentUser;
  List<Widget> invoicesDetail = [];
  

  callHttp() async {
    String url = '${baseAPi}papi/invoice';
    Map data = {"apiRequest":{"action":"get"},"data":[{
      "id": widget.invoice.id
    }]};
    print(widget.invoice.id);
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
      },
    );
    print(response.statusCode);
    print(json.decode(response.body));
    // _invoiceDetail =InvoiceProfileModel.fromJson(json.decode(response.body)['data'][0]);
    // print(_invoiceDetail.invoiceItems.length);
    setState(() {
      
    });
  }

  @override
  void initState() {
    currentUser = Provider.of<UserState>(context,listen: false).getUser();
    // callHttp();
    invoicesDetail = widget.invoice.invoiceItems.map( (f) => 
                    _containertext(
                      '${f.name}',
                      '${f.amount.toStringAsFixed(2)}'
                      )).toList(); 
    // invoicesDetail = List.generate(100, (i) => _containertext(
    //                   '${i}',
    //                   '${i}'
    //                   ));        
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text('Invoice Detail',style: TextStyle(
            color: Colors.black
          ),),
        ),
        body: content(context),
      ),
    );
  }


  Widget content(BuildContext context) {
    return  Container(
        // color: Colors.red,
        width: double.infinity,
        height: double.infinity,
        child :SingleChildScrollView(
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  width:  MediaQuery.of(context).size.width,
                  padding: EdgeInsets.all(20),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                        Row(                          
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: <Widget>[
                                Text('${widget.invoice.getTotalBilling()}',style: TextStyle(
                                  color: Colors.black,
                                  fontSize: 25,
                                  fontWeight: FontWeight.bold
                                  ))
                          ],
                        ),
                        
                      RichText(
                        text: TextSpan(
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15
                        ),              
                        children: <TextSpan>[
                          TextSpan(text: 'Issue Date: '),
                          TextSpan(text: '${widget.invoice.issueDate}',style: TextStyle(fontWeight: FontWeight.bold))
                        ]
                        ),
                      ),
                      RichText(
                        text: TextSpan(
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15
                        ),              
                        children: <TextSpan>[
                          TextSpan(text: 'Due Date: '),
                          TextSpan(text: '${widget.invoice.dueDate}',style: TextStyle(fontWeight: FontWeight.bold))
                        ]
                        ),
                      ),
                      RichText(
                        text: TextSpan(
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15
                        ),              
                        children: <TextSpan>[
                          TextSpan(text: 'Invoice No: '),
                          TextSpan(text: '${widget.invoice.invoiceNo}',style: TextStyle(fontWeight: FontWeight.bold))
                        ]
                        ),
                      ),
                      RichText(
                        text: TextSpan(
                        style: TextStyle(
                          color: Colors.black,
                          fontSize: 15
                        ),              
                        children: <TextSpan>[
                          TextSpan(text: 'Invoice Status: '),
                          TextSpan(text: '${widget.invoice.invoiceStatusName}',style: TextStyle(fontWeight: FontWeight.bold))
                        ]
                        ),
                      ),
                      
                    ],
                ),
                )
              ],
            ),
            // _lineWithShadow(),
            Padding(
              padding: const EdgeInsets.only(left:20.0,bottom: 10.0),
              child: RichText(
                text: TextSpan(
                style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
                ),              
                children: <TextSpan>[
                  TextSpan(text: 'Invoice Name: '),
                  TextSpan(text: '${widget.invoice.name}',style: TextStyle(fontWeight: FontWeight.bold))
                ]
                ),
              ),
            ),  
            Padding(
              padding: const EdgeInsets.only(left:20.0,right: 20.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Description',style: TextStyle(
                    color: Colors.black,
                    fontSize: 15
                  ),),
                  Text('Sub Total',style: TextStyle(
                    color: Colors.black,
                    fontSize: 15
                  ),)
                ],
              ),
            ),            
            Container(
              child: SingleChildScrollView(                
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  crossAxisAlignment: CrossAxisAlignment.start,
                    children:invoicesDetail,
                ),
              )          
            ),            
          ],
        ),
        )
    );
  }

Widget _lineWithShadow(){
  return Container(
    width: double.infinity,   
    height: 1,
    decoration: BoxDecoration(      
      boxShadow: [
        BoxShadow(
          color: Color.fromRGBO(0, 0, 0, 0.5),
          blurRadius: 0.1, 
          spreadRadius: 0.5,
          offset: Offset(
            1.0, 
            2.0, 
          ),
        )
  ],
    ),
  );
}

Widget _containertext(String detail,String totle){
  return  Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                // padding: EdgeInsets.all(20.0),
                padding: EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                  top: 10.0,
                  // bottom: 10.0,
                ),
                child:  Text('$detail',style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold
                  ),)
              ),
              Container(
                padding: EdgeInsets.only(
                  left: 20.0,
                  right: 20.0,
                  top: 10.0,
                  // bottom: 10.0,
                ),
                child:  Text('$totle',style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold
                  ),)
              )
      ],
      );
  }
}