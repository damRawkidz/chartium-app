
import 'package:flutter/material.dart';

import 'history_screen.dart';
import 'outstanding_screen.dart';
import 'overdue_screen.dart';
class invoiceTabScreen extends  StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _invoiceTabScreenState();
  }

}
class _invoiceTabScreenState extends State<invoiceTabScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 3,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          // leading: Builder(
          //         builder: (BuildContext context) {
          //           return IconButton(
          //           icon: const Icon(Icons.question_answer),
          //           color: Colors.black,
          //           tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
          //         );
          //       },
          // ),
          title: Text(
                'Invoice',
                style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          // backgroundColor: Colors.white,
          // actions: <Widget>[ 
          //       IconButton(
          //         icon: Icon(Icons.playlist_play),
          //         tooltip: 'Air it',
          //         color: Colors.black,
          //         // onPressed: _airDress,
          //       ),
          //       IconButton(
          //         icon: Icon(Icons.playlist_add),
          //         tooltip: 'Restitch it',
          //         color: Colors.black,
          //         // onPressed: _restitchDress,
          //       ),
          //     ],    
          bottom: TabBar(
            labelColor: Colors.black,
            indicatorColor: Color.fromRGBO(180, 151, 96, 1),
            unselectedLabelColor: Color.fromRGBO(180, 151, 96, 1),            
            tabs: <Widget>[
              Tab(text: 'Out Standing'),
              Tab(text: 'Overdue'),
              Tab(text: 'Paid')
            ],
          ),
        ),
        body: TabBarView(children: <Widget>[
            outStandingScreen(),
            overDuleScreen(),
            historyScreen(),            
        ],
        ),
      ),
    );
  }
  
}