class TopicModel {
  int accountId;
  String accountName;
  String accountSurName;
  int roomId;
  String roomNo;
  int workOrderId;
  String workOrderNo;
  String workOrderItemCode;
  String workOrderItemName;
  int topicStatusId;
  String topicStatusCode;
  String topicStatusName;
  int unreadMessage;
  int customerUnreadMessage;
  List<Messages> messages;
  String accountImageUrl;  
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;
  String lastMessage;

  TopicModel(
      {this.accountId,
      this.accountName,
      this.accountSurName,
      this.roomId,
      this.roomNo,
      this.workOrderId,
      this.workOrderNo,
      this.workOrderItemCode,
      this.workOrderItemName,
      this.topicStatusId,
      this.topicStatusCode,
      this.topicStatusName,
      this.unreadMessage,
      this.customerUnreadMessage,
      this.messages,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.lastUpdateDate,
      this.accountImageUrl,
      this.lastMessage});

  TopicModel.fromJson(Map<String, dynamic> json) {
    accountId = json['accountId'];
    accountName = json['accountName'];
    accountSurName = json['accountSurName'];
    roomId = json['roomId'];
    roomNo = json['roomNo'];
    workOrderId = json['workOrderId'];
    workOrderNo = json['workOrderNo'];
    workOrderItemCode = json['workOrderItemCode'];
    workOrderItemName = json['workOrderItemName'];
    topicStatusId = json['topicStatusId'];
    topicStatusCode = json['topicStatusCode'];
    topicStatusName = json['topicStatusName'];
    unreadMessage = json['unreadMessage'];
    customerUnreadMessage = json['customerUnreadMessage'];
    accountImageUrl =json['accountImageUrl'];
    lastMessage =json['lastMessage'];
    if (json['messages'] != null) {
      messages = new List<Messages>();
      json['messages'].forEach((v) {
        messages.add(new Messages.fromJson(v));
      });
    }
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accountId'] = this.accountId;
    data['accountName'] = this.accountName;
    data['accountSurName'] = this.accountSurName;
    data['roomId'] = this.roomId;
    data['roomNo'] = this.roomNo;
    data['workOrderId'] = this.workOrderId;
    data['workOrderNo'] = this.workOrderNo;
    data['workOrderItemCode'] = this.workOrderItemCode;
    data['workOrderItemName'] = this.workOrderItemName;
    data['topicStatusId'] = this.topicStatusId;
    data['topicStatusCode'] = this.topicStatusCode;
    data['topicStatusName'] = this.topicStatusName;
    data['unreadMessage'] = this.unreadMessage;
    data['customerUnreadMessage'] = this.customerUnreadMessage;
    if (this.messages != null) {
      data['messages'] = this.messages.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    return data;
  }
}

class Messages {
  int topicId;
  String message;
  int messageStatusId;
  String messageStatusCode;
  String messageStatusName;
  int replyById;
  int accountId;
  int roomId;
  int workOrderId;
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;
  
  Messages(
      {this.topicId,
      this.message,
      this.messageStatusId,
      this.messageStatusCode,
      this.messageStatusName,
      this.replyById,
      this.accountId,
      this.roomId,
      this.workOrderId,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.lastUpdateDate});

  Messages.fromJson(Map<String, dynamic> json) {
    topicId = json['topicId'];
    message = json['message'];
    messageStatusId = json['messageStatusId'];
    messageStatusCode = json['messageStatusCode'];
    messageStatusName = json['messageStatusName'];
    replyById = json['replyById'];
    accountId = json['accountId'];
    roomId = json['roomId'];
    workOrderId = json['workOrderId'];
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['topicId'] = this.topicId;
    data['message'] = this.message;
    data['messageStatusId'] = this.messageStatusId;
    data['messageStatusCode'] = this.messageStatusCode;
    data['messageStatusName'] = this.messageStatusName;
    data['replyById'] = this.replyById;
    data['accountId'] = this.accountId;
    data['roomId'] = this.roomId;
    data['workOrderId'] = this.workOrderId;
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    return data;
  }
}