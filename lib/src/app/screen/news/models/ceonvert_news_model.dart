import 'news_models.dart';

class ConvertNewsModels {
  String newsType;
  List<NewsModels> details;

  ConvertNewsModels.fromJson(Map<String,dynamic> json):
  newsType =json['newsType'],
  details = (json['details'] as List<dynamic>).map((f) => NewsModels.fromJson(f)).toList();
}