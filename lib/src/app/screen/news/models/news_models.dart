import 'package:intl/intl.dart';

class NewsModels {
  int newsTypeId;
  String newsTypeCode;
  String newsTypeName;
  List<News> news;

  NewsModels(
      {this.newsTypeId, this.newsTypeCode, this.newsTypeName, this.news});

  NewsModels.fromJson(Map<String, dynamic> json) {
    newsTypeId = json['newsTypeId'];
    newsTypeCode = json['newsTypeCode'];
    newsTypeName = json['newsTypeName'];
    if (json['news'] != null) {
      news = new List<News>();
      json['news'].forEach((v) {
        news.add(new News.fromJson(v));
      });
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['newsTypeId'] = this.newsTypeId;
    data['newsTypeCode'] = this.newsTypeCode;
    data['newsTypeName'] = this.newsTypeName;
    if (this.news != null) {
      data['news'] = this.news.map((v) => v.toJson()).toList();
    }
    return data;
  }
}

class News {
  String name;
  bool isGlobal;
  int propertyId;
  String propertyName;
  int newsTypeId;
  String newsTypeCode;
  String newsTypeName;
  String description;
  String activeDate;
  String expireDate;
  String imageUrl;
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;
  // HH:mm a
  var _formatter = DateFormat("dd-MM-yyyy");
  News(
      {this.name,
      this.isGlobal,
      this.propertyId,
      this.propertyName,
      this.newsTypeId,
      this.newsTypeCode,
      this.newsTypeName,
      this.description,
      this.activeDate,
      this.expireDate,
      this.imageUrl,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.lastUpdateDate});

  News.fromJson(Map<String, dynamic> json) {
    name = json['name'];
    isGlobal = json['isGlobal'];
    propertyId = json['propertyId'];
    propertyName = json['propertyName'];
    newsTypeId = json['newsTypeId'];
    newsTypeCode = json['newsTypeCode'];
    newsTypeName = json['newsTypeName'];
    description = json['description'];
    activeDate = _formatter.format(DateTime.parse(json['activeDate']));
    expireDate = json['expireDate'];
    imageUrl = json['imageUrl'];
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['isGlobal'] = this.isGlobal;
    data['propertyId'] = this.propertyId;
    data['propertyName'] = this.propertyName;
    data['newsTypeId'] = this.newsTypeId;
    data['newsTypeCode'] = this.newsTypeCode;
    data['newsTypeName'] = this.newsTypeName;
    data['description'] = this.description;
    data['activeDate'] = this.activeDate;
    data['expireDate'] = this.expireDate;
    data['imageUrl'] = this.imageUrl;
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    return data;
  }
}