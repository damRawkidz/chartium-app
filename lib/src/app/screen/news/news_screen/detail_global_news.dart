import 'package:chartium/src/app/screen/news/models/news_models.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
class detailGlobalNews extends  StatelessWidget  with BaseApi{
  News news;
  detailGlobalNews({Key key,@required this.news}) : super(key:key);

  @override
  Widget build(BuildContext context) {        
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            color: Colors.black,
            icon: Icon(Icons.arrow_back),
            onPressed: () => {
              Navigator.pop(context)
            },
          ),
          title: Text('${news.name}',style: TextStyle(
            color: Colors.black
          ),),
          // actions: <Widget>[
          //   IconButton(
          //   color: Colors.black,
          //   icon: Icon(Icons.share),
          //   onPressed: () => {
          //     showCupertinoModalPopup(
          //       context: context,
          //       builder: (BuildContext context) => CupertinoActionSheet(
          //         title: const Text('Select Share'),                
          //         actions: <Widget>[
          //             CupertinoActionSheetAction(
          //             child:  Container(
          //               child: Row(
          //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                 children: <Widget>[
          //                   Container(
          //                     child: Row(
          //                       children: <Widget>[
          //                         Image.asset('assets/images/Facebook.webp'),
          //                         // Container(child: Image.asset('assets/fb.png')),
          //                         Container(margin:EdgeInsets.only(right: 30.0)),
          //                         Text('FACEBOOK'),
          //                       ],
          //                     ),
          //                   ),
          //                   Icon(Icons.chevron_right)
          //                 ],
          //               ),
          //             ), 
          //             onPressed: () { 
          //               print('Click print');
          //             }
          //           ),
          //           CupertinoActionSheetAction(
          //             child:  Container(
          //               child: Row(
          //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                 children: <Widget>[
          //                   Container(
          //                     child: Row(
          //                       children: <Widget>[
          //                         Image.asset('assets/images/Instagram.webp'),
          //                         Container(margin:EdgeInsets.only(right: 30.0)),
          //                         Text('INSTRAGRAM'),
          //                       ],
          //                     ),
          //                   ),
          //                   Icon(Icons.chevron_right)
          //                 ],
          //               ),
          //             ), 
          //             onPressed: () { 
          //               print('Click print');
          //             }
          //           ),
          //           CupertinoActionSheetAction(
          //             child:  Container(
          //               child: Row(
          //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                 children: <Widget>[
          //                   Container(
          //                     child: Row(
          //                       children: <Widget>[
          //                         Image.asset('assets/images/Twitter.webp'),
          //                         Container(margin:EdgeInsets.only(right: 30.0)),
          //                         Text('TWITTER'),
          //                       ],
          //                     ),
          //                   ),
          //                   Icon(Icons.chevron_right)
          //                 ],
          //               ),
          //             ), 
          //             onPressed: () { 
          //               print('Click print');
          //             }
          //           ),                                      
          //           CupertinoActionSheetAction(
          //             child:  Container(
          //               child: Row(
          //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
          //                 children: <Widget>[
          //                   Container(
          //                     child: Row(
          //                       children: <Widget>[
          //                         Icon(Icons.more_horiz),
          //                         Container(margin:EdgeInsets.only(right: 30.0)),
          //                         Text('OTHER'),
          //                       ],
          //                     ),
          //                   ),
          //                   Icon(Icons.chevron_right)
          //                 ],
          //               ),
          //             ), 
          //             onPressed: () { 
          //               print('Click print');
          //             }
          //           ),
          //         ]
          //       ),
          //     )
          //   },
          // ),
          // ],
        ),
        body: body(context),
      ),
    );
  }

  Widget body(BuildContext context){
    //  const String url = 'https://upload.wikimedia.org/wikipedia/commons/5/5f/Cherprang_Areekul_2018_Tiangong.jpg';
    return SingleChildScrollView(
      // controller: controller,
      child: Column(
        children: <Widget>[
            Container(
              child: news.imageUrl.isNotEmpty ? Image.network('$baseAPi${news.imageUrl}') : Image.asset('assets/images/mock.png')
            ),
          // Container(
          //   // color: Colors.red,
          //   width: double.infinity,
          //   height: 300.0,
          //   child: news.newsImages.isEmpty ? 
          //   Image.asset('assets/images/mock.png'):            
          //   ListView.builder(
          //     scrollDirection: Axis.horizontal,
          //     physics: ClampingScrollPhysics(),
          //     shrinkWrap: true,
          //     itemCount: news.newsImages.length,
          //     itemBuilder: (BuildContext context,int index){
          //       return Container(
          //         width: MediaQuery.of(context).size.width,
          //         child: FittedBox(
          //           fit: BoxFit.fill,
          //           child: Image.network('$baseAPi${news.newsImages[index].imageUrl}',height: 300.0,)
          //         ),
          //       );                
          //     },
          //   ),
          // ),
        // Image.network(url),
        // news.newsImages.isNotEmpty ?Image.network('$baseAPi${news.newsImages[0].imageUrl}') :
        // Image.asset('assets/images/mock.png'),
        Container(
          padding: EdgeInsets.all(10), 
              child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                  Text('Publish: ${news.activeDate}'),
                  Text('Expire Date: ${ formatter.format(DateTime.parse(news.expireDate.substring(0,10)))}'),
                  // Text('by Hotel group'),
              ],
        ),
        ),
        Container(
          padding: EdgeInsets.all(10.0),
          child: Column(
            children: <Widget>[
              Text('${news.name}',style: TextStyle(
                color: Colors.red
              ),),
              Container(margin: EdgeInsets.only(bottom: 10),),
              Text('${news.description}'),
            ],
          ),
        ),
        
        // Text('PACKEAGE'),
        // Text('DETAIL'),
        // Text('DETAIL'),
        // Text('DETAIL'),
      ],
    )
    );
  }
}