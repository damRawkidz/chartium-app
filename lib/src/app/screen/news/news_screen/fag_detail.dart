import 'package:flutter/material.dart';

class fagDetail extends StatelessWidget {
  String title ='';
  String detail = '';
  List<Widget> details = [];
  fagDetail({Key key,@required this.title,@required this.detail}):super();
  @override  
  Widget build(BuildContext context) {
    this.details  = this.detail.split(',').map((f) => Text(f.trimLeft())).toList();
    return MaterialApp(      
      home: Scaffold(
        appBar: AppBar(
          backgroundColor:Colors.white,
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          title: Text('FAQ',style: TextStyle(
            color: Colors.black
          ),),
        ),        
        body: MakeContent(this.title),
      ),
    );
  }

  Widget MakeContent(String title){
    return Container(
      padding: EdgeInsets.all(15.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
            // Text('Date'),
            Text('$title',style: TextStyle(
              fontSize: 20,
            ),),
            content()
        ],
      ),
    );

  }

  Widget content(){
    return SizedBox(
      // height: 150,
      child: Container(
        margin: EdgeInsets.only(top: 20),
        padding: EdgeInsets.all(10.0),
        decoration: BoxDecoration(
            color: Colors.white,
            border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
            borderRadius: BorderRadius.circular(5.0),
            boxShadow: [
              BoxShadow(
                color: Color.fromRGBO(0, 0, 0, 0.5),
                blurRadius: 0.1, 
                spreadRadius: 0.1,
                offset: Offset(
                  1.0, 
                  2.0, 
                ),
              )
            ],
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: details,
        )        
      ),
    );
}
}