
import 'dart:convert';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/screen/news/models/news_models.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'detail_global_news.dart';
class residentScreen extends  StatefulWidget {
  @override
  _residentScreenState createState() => _residentScreenState();
}

class _residentScreenState extends State<residentScreen> with BaseApi {
  UserModel currentUser;
  String token;
  List<NewsModels> newsList = [];
  
    callHttp() async {
    String url = '${baseAPi}papi/newsgroup';
    Map data = {"apiRequest":{"action":"list-mobile-local"}};    
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
      },
    );
    print(response.body);
    (json.decode(response.body)['data'] as List).forEach((news){      
      newsList.add(NewsModels.fromJson(news));
    });
    print(newsList[0].news.length);
    setState(() {
    });
  }

  @override
  void initState() { 
    super.initState();    
    // currentUser = Provider.of<UserState>(context,listen: false).getUser();
    token = Provider.of<UserState>(context,listen: false).getToken();
    callHttp();
    
  }

  @override
  Widget build(BuildContext context) {
    return SingleChildScrollView(
      child: newsList.isEmpty ? Text('No Data to Display') : Column(
        children: newsList.map((news) => makeListWidget(context,news)).toList()
    ),
    );
    
  }

  Widget makeListWidget(BuildContext context,NewsModels news){
    return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child:  Text('${news.newsTypeName}',style: TextStyle(
              color: Color.fromRGBO(180, 151, 96, 1)
              ),
            ),
          ),
          SizedBox(
            height: 160.0,
            child: createList(context,news.news)
          )
        ],
      ),
    );
  }

  Widget createList (BuildContext context,List<News> list) {
    
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      itemCount: list.length,
      itemBuilder: (BuildContext context,int index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(context,
                   MaterialPageRoute(builder: (context) => detailGlobalNews(news: list[index])),
              );
            },
            child: Card(
            child: Container(
              padding: EdgeInsets.all(10),
              width: list.length > 1 ? MediaQuery.of(context).size.width - 50 : MediaQuery.of(context).size.width,
              height: 400,
              child:Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                    ContainerImage(list[index]),
                    ContainerContent(context,list[index]),
                ],
              )
            ),
          ),
        );          
      },
    );
  }

  Widget ContainerImage(News news){
    // String url = 'https://scontent.fbkk8-3.fna.fbcdn.net/v/t31.0-8/26171463_1833351416715230_2166791531763826436_o.jpg?_nc_cat=111&_nc_eui2=AeGHtlihyUvmjcDWUvraVAmljxQgJ_ZPMTryIHHpQ4RPSmJra-78NLRzz3OTOtHirOIURQEwvtbswjJ3JtuPBiaOW6ajp8EQ1DmNy_Y_CwvO6w&_nc_oc=AQnLEDE-CH08SzxuhJ26N8uv5RIrBzACrS4UVXzntv41EDV5VG4Uv7AMJh86MyAUHAY&_nc_ht=scontent.fbkk8-3.fna&oh=c1cba603a922d268c794ee9561927a29&oe=5DB16172';
      return  Expanded(
        child: FittedBox(
          // child: Image.asset('assets/images/mock.png'),
          child: news.imageUrl.isNotEmpty ? Image.network('$baseAPi${news.imageUrl}') : Image.asset('assets/images/mock.png'),
          fit: BoxFit.fill,
        ),
      );      
  }

  Widget ContainerContent(BuildContext context,News newsModels) {
    return  Expanded(      
      flex: 2,
      child: Container(
        padding: EdgeInsets.all(5),
        // color: Colors.red,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text('${newsModels.name}',style: TextStyle(
              color: Colors.red
              ),
            ),
            // detail(newsModels.description),
            Text('${newsModels.description}',style: TextStyle(
              color: Color.fromRGBO(139, 153, 159, 1)
              ),
              overflow: TextOverflow.ellipsis,
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text('${newsModels.activeDate}',style: TextStyle(
                      color: Color.fromRGBO(139, 153, 159, 1)
                      ),
                    ),
                    Text('Read More',
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: Color.fromRGBO(180, 151, 96, 1)
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );    
  }
  
}