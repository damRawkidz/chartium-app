import 'package:flutter/material.dart';

import 'fag_detail.dart';

class FAQ extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return makeMainScreen(context);
  }


  Widget makeMainScreen(BuildContext context){
    List<Widget> detail = [
      infoText(),
      headerText(),
      _titleFAQ('How to create work order ?','1. Click tab “ Work Order” on footer menu, 2. Click “+” and press description and detail of work order, 3. Click “OK” program will display summary new work order. You can add more to 3 maximum, 4. Click “Submit” to finish.',context),
      _titleFAQ('How to receive parcel ?','1. Click tab “Parcel” on footer menu, 2. Click your new parcel and show QR Code to staff, 3. Staff will scan QR Code on your mobile and give parcel',context),      
      _titleFAQ('How to create message ?','1. Click message icon on title menu, 2. Choose your topic. if you choose "other" you need to type your message, 3. Click “Send” and waiting for reply, 4. It will notification on mobile when staff reply you.',context),
    ];
    return MaterialApp(      
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,          
          leading: IconButton( 
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          title: Text('FAQ',style: TextStyle(
            color: Colors.black
          ),),
        ),
        body: Container(
          child: Column(
            mainAxisSize: MainAxisSize.max,
            mainAxisAlignment: MainAxisAlignment.start,
            children: detail
          )
        ),
      ),
    );
  }

  Widget infoText(){
    const String masseage = '* You Can send question through the from below. We will respond within 24 hours?';
      return Container(
        padding: EdgeInsets.all(10.0),
        child: Text('$masseage',style: TextStyle(
          color: Color.fromARGB(234, 119, 34, 1)
        ),),
      );
  }

  Widget headerText(){
    const String masseage = 'FREQUENTLY ASKED QUESTIONS?';
    return Container(
      padding: EdgeInsets.all(10.0),
      color: Color.fromRGBO(247, 247, 247, 1),
      child:  Align(
        alignment: Alignment.centerLeft,
        child: Text('$masseage',style: TextStyle(
        color: Color.fromRGBO(189, 184, 184, 1)
      ),)
      )      
    );
  }
  
  Widget _titleFAQ(String title,String date,BuildContext context){
    return GestureDetector(
          onTap: () {
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => fagDetail(title: title,detail: date,)),
            );
          },
          child: SizedBox(
        height: 100.0,
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Expanded(
              flex: 1,
              child: Container(       
                padding: EdgeInsets.all(10.0),
                decoration: BoxDecoration(
                  border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
                  
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    // _textDate(date),
                    _titleFAG(title,date,context)
                  ],
                ),
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget _textDate(String date){
    return Text('$date',
      style: TextStyle(
          color: Colors.grey
      ),
    );
  }
  Widget _titleFAG(String title,String date,BuildContext context){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('$title',style: TextStyle(
          color: Colors.black,
          fontSize: 20
        ),),
        // IconButton(
        //   icon: Icon(Icons.chevron_right),
        //   onPressed: (){
        //     // Navigator.push(context,
        //     //   MaterialPageRoute(builder: (context) => fagDetail(title: title,detail: date,)),
        //     // );
        //   },
        // )
      ],
    );
  }
}