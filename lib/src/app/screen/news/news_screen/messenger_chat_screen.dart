import 'dart:convert';

import 'package:chartium/src/app/components/bubble_chat.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
class messengerChat extends StatefulWidget {
  int topicId;
  messengerChat({Key key,@required this.topicId}):super(key:key);
  @override
  _messengerChatState createState() => _messengerChatState();
}
class _messengerChatState extends State<messengerChat> with BaseApi{
  UserModel currentUser;
  final _inputController = TextEditingController(); 
  final _scrollController =ScrollController(); 
  // List<Widget> list = List.generate(10, (i) => BubbleChat(message:'${i + 1}',isSender: Random().nextBool(),));  
  List<Widget> list = [];

  @override
  void initState() { 
    super.initState();
    print(widget.topicId);
    // currentUser = Provider.of<UserState>(context,listen: false).getUser();
    token = Provider.of<UserState>(context,listen: false).getToken();
    callHttp();  
  }


  callHttp() async {
    String url = '${baseAPi}papi/message';
    Map data = {"apiRequest":{"action":"get"},"data":[
          {"topicId": widget.topicId}
      ]};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
          "Content-Type": "application/json",
          "Authorization":"bearer "+token
        },
    );
    print(json.decode(response.body)['data']);
    print(response.statusCode);
    (json.decode(response.body)['data'] as List).forEach((topic){      
        list.add(BubbleChat(message:'${topic['message']}',isSender: topic['replyById'] != null));
    });
    // print(convertList.length);
    setState(() {
    });
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
          resizeToAvoidBottomInset:false,
          appBar: AppBar(
            centerTitle: true,
            leading: IconButton( 
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
                Navigator.pop(context);
              },            
            ),
            backgroundColor: Colors.white,
            title: Text('Message Detail',style: TextStyle(
              color: Colors.black
            ),),
          ),
          body: _mainContent(context),
        ),
    );
  }

  Widget _mainContent(BuildContext context){
    return Container(   
      // color: Colors.red,
      child: Column(
        children: <Widget>[            
            _textChat(),
            _contentChat(),
        ],
      ),
    );
  }

  Widget _textChat(){     
    return Expanded(
      flex: 10,
      child: Container(        
        child: ListView.builder(
          itemCount: list.length,
          itemBuilder: (BuildContext context,int index){
            return list[index];
          },
        ),
      ),      
    );
  }

  Widget _contentChat(){
        return 
        // Expanded(
        // flex: 1,
        // child: 
        TextFormField(        
        controller: _inputController,
        keyboardType: TextInputType.emailAddress,
        decoration: InputDecoration(
          // prefixIcon: Icon(Icons.camera_alt),
          suffixIcon: IconButton(
            icon: Icon(Icons.send),
            onPressed: (){
              setState(()  {
                  var value =_inputController.text;
                  if (value != '') {
                    list.add(BubbleChat(message:value.toString(),isSender: false,));
                    
                  //   _scrollController.animateTo(
                  //     _scrollController.position.maxScrollExtent + 20,
                  //     duration: const Duration(milliseconds: 200),
                  //     curve: Curves.easeOut
                  // );
                  interactMessage();
                  _inputController.clear();
                  }
              });
            },
          ),
          // labelText: 'Email Address',
          hintText: 'Message Here',
          // border: OutlineInputBorder()
      )
    );
        // );
  }
  
  interactMessage() async {
    String url = '${baseAPi}papi/message';
    Map data = {"apiRequest":{"action":"new"},"data":[{
        "topicId":widget.topicId,
        "message": _inputController.text,
      }]};
    var body = json.encode(data);
    print(body);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token,
        },
    );
    print(response.statusCode);
    print(response.body);
    print('interact success');
    // await _list_MessageApi();
  }
}