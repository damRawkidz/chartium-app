import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/screen/parcel/parcel_screen/parcel.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:provider/provider.dart';

class notificationScreen extends StatefulWidget {
  @override
  _notificationScreenState createState() => _notificationScreenState();
}

class _notificationScreenState extends State<notificationScreen> with BaseApi{
  UserModel currentUser;
  @override
  Widget build(BuildContext context) {
    return mainContainer(context);
  }

  @override
  void initState() { 
    super.initState();
    currentUser = Provider.of<UserState>(context,listen: false).getUser();
  }

  callHttp() async {
    String url = '${baseAPi}papi/topic';
    Map data = {"apiRequest":{"action":"get-customer"},"data":[
          // {"accountId": currentUser.id}
      ]};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {"Content-Type": "application/json"},
    );
    print(json.decode(response.body)['data']);
    print(response.statusCode);
    // (json.decode(response.body)['data'] as List).forEach((news){      
    //     list.add(GroupNewsModels.fromJson(news));      
    // });
    // print(convertList.length);
    setState(() {
    });
  }

  Widget mainContainer(BuildContext context){
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,          
          leading: IconButton( 
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          // actions: <Widget>[
          //   IconButton( 
          //   icon: Icon(Icons.plus_one),
          //   color: Colors.black,
          //   onPressed: null            
          // ) 
          // ],
          title: Text('Notifications',style: TextStyle(
              color: Colors.black
            ),
          ),
        ),        
        body: content(context),
        
      ),
    );
  }

  Widget content(BuildContext context){
   List<int> list = List.generate(10000, (i) => i+1);
    return ListView.builder(
      itemCount: list.length,
      itemBuilder: (BuildContext context,int index) {
        return GestureDetector(
          onTap: (){
            Navigator.push(context,
                MaterialPageRoute(builder: (context) => Parcel(parcel: null))
              );
            },
          child:  Container(
            height: 120,
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
                ),
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Row(
                    children: <Widget>[
                        ContainerImage(),
                        ContainerContent(context),
                    ],
                  )
                ),
            ),
          )          
        );
      },
    );
  } 

  Widget ContainerImage(){
    // String url = 'https://scontent.fbkk8-3.fna.fbcdn.net/v/t31.0-8/26171463_1833351416715230_2166791531763826436_o.jpg?_nc_cat=111&_nc_eui2=AeGHtlihyUvmjcDWUvraVAmljxQgJ_ZPMTryIHHpQ4RPSmJra-78NLRzz3OTOtHirOIURQEwvtbswjJ3JtuPBiaOW6ajp8EQ1DmNy_Y_CwvO6w&_nc_oc=AQnLEDE-CH08SzxuhJ26N8uv5RIrBzACrS4UVXzntv41EDV5VG4Uv7AMJh86MyAUHAY&_nc_ht=scontent.fbkk8-3.fna&oh=c1cba603a922d268c794ee9561927a29&oe=5DB16172';
      return Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Image.asset('assets/images/noimage.jpg',width: 100,height: 100,),
        ],
      );
    // return Image.network('$url',width: 100,height: 100,);
      // return  Expanded(
      //   flex: 1,
      //   child: FittedBox(                  
      //     child: Image.network('$url'),
      //     fit: BoxFit.fill,
      //   ),
      // );      
  }

  Widget ContainerContent(BuildContext context) {
    return  Expanded(      
      flex: 2,
      child: Container(
        padding: EdgeInsets.only(top: 20,left: 5,right: 5,bottom: 5),
        // color: Colors.red,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(              
              children: <Widget>[
                Text('New Parcel',style: TextStyle(
                    color: Colors.red,
                    fontSize: 10
                  ),
                ),
                Container(margin: EdgeInsets.only(right: 5)),
                Text('Parcel: Number :GR4202234234234TH',style: TextStyle(
                    color: Color.fromRGBO(139, 153, 159, 1),
                    fontSize: 10
                  ),
                ),   
                Spacer(),                
                Text('sadsads', 
                  style: TextStyle(
                    color: Colors.red,
                    fontSize: 10
                  ),
                ),
              ]
            ),
            Row(
              children: <Widget>[
                Text('How to use aplication',style: TextStyle(                    
                    fontSize: 20,
                    fontWeight: FontWeight.bold
                  ), 
                ),  
              ],
            ),
            Row(
              children: <Widget>[
                Text('To',style: TextStyle(
                    color: Color.fromRGBO(139, 153, 159, 1),                    
                  )),
                Container(margin:EdgeInsets.only(left: 5)),
                Text('request via contractasdsdsdsdasds')
              ],
            ),
            Text('recive to 26-02-2019 11:00:56', style: TextStyle(
                    color: Color.fromRGBO(139, 153, 159, 1),                    
                  ))
          ],
        ),
      ),
    );    
  }
}