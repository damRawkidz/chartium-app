import 'dart:convert';
// import 'package:chartium/src/app/providers/proprty_id_provider.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/screen/news/models/news_models.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'detail_global_news.dart';

class globalNewsScreen extends StatefulWidget {
  @override
  _globalNewsScreenState createState() => _globalNewsScreenState();
}

class _globalNewsScreenState extends State<globalNewsScreen> with BaseApi {
  UserModel currentUser;
  List<NewsModels> list = [];
  List convertList = [];
  String token = '';
  @override
  Widget build(BuildContext context) {  
    return mainLayout(context);
  }
  
  @override
  void initState() { 
    super.initState();
    currentUser = Provider.of<UserState>(context,listen: false).getUser();
    token = Provider.of<UserState>(context,listen: false).getToken();
    print(currentUser);
    callHttp();
  }

  callHttp() async {
    String url = '${baseAPi}papi/newsgroup';
    Map data = {"apiRequest":{"action":"list-mobile-global"}};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
          "Content-Type": "application/json",
          "Authorization":"bearer "+token
        },
    );
    print(json.decode(response.body)['data']);
    print(response.statusCode);
    (json.decode(response.body)['data'] as List).forEach((news){      
        list.add(NewsModels.fromJson(news));      
    });
    setState(() {
    });
  }

  Widget mainLayout(BuildContext context){
    return SingleChildScrollView(
      child: list.isEmpty ? Text('No Data to Display') : Column(
        children: list.map((news) => makeListWidget(context,news)).toList()
      ),
    );
  }

  Widget makeListWidget(BuildContext context,NewsModels news){
    return Container(
      padding: EdgeInsets.all(5),
      child: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child:  Text('${news.newsTypeName}',style: TextStyle(
              color: Color.fromRGBO(180, 151, 96, 1)
              ),
            ),
          ),
          SizedBox(
            height: 160.0,
            child: createList(context,news.news)
          )
        ],
      ),
    );
  }

  Widget createList (BuildContext context,List<News> list) {
    return ListView.builder(
      scrollDirection: Axis.horizontal,
      physics: ClampingScrollPhysics(),
      shrinkWrap: true,
      itemCount: list.length,
      itemBuilder: (BuildContext context,int index) {
          return GestureDetector(
            onTap: () {
              Navigator.push(context,
                   MaterialPageRoute(builder: (context) => detailGlobalNews(news: list[index])),
              );
            },
            child: Card(            
            child: Container(
              padding: EdgeInsets.all(10),
              width: list.length > 1 ? MediaQuery.of(context).size.width - 50 : MediaQuery.of(context).size.width,
              height: 150,
              child:Row(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                    ContainerImage(list[index]),
                    ContainerContent(context,list[index]),
                ],
              )
            ),
          ),
        );          
      },
    );
  }

  Widget ContainerImage(News news){    
      return  Expanded(
        child: FittedBox(
          child: news.imageUrl.isNotEmpty ? Image.network('$baseAPi${news.imageUrl}') : Image.asset('assets/images/mock.png'),
          fit: BoxFit.fill,
        ),
      );      
  }

  Widget ContainerContent(BuildContext context,News newsModels) {
    return  Expanded(      
      flex: 2,
      child: Container(
        padding: EdgeInsets.all(5),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text('${newsModels.name}',style: TextStyle(
              color: Colors.red
              ),
            ),
            Text('${newsModels.name}',style: TextStyle(
              color: Colors.red
              ),
              overflow: TextOverflow.ellipsis,
            ),
            Expanded(
              child: Align(
                alignment: Alignment.bottomCenter,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Text('${newsModels.activeDate}',style: TextStyle(
                      color: Color.fromRGBO(139, 153, 159, 1)
                      ),
                    ),
                    Text('Read More',
                      style: TextStyle(
                        decoration: TextDecoration.underline,
                        color: Color.fromRGBO(180, 151, 96, 1)
                      ),
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );    
  }
  
}