import 'dart:convert';

import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';

import 'messenger_screen.dart';
class questionScreen extends StatefulWidget {
  @override
  _questionScreenState createState() => _questionScreenState();
}

class _questionScreenState extends State<questionScreen>  with BaseApi{
  int _radioValue = 0;
  bool enableInput =false;
  List<String>  _question = ['Promotion','Ask for the lastest service work','Contact work outstanding','How to cancel service','Popular service'];
  UserModel currentUser;
  final remarkController  = TextEditingController();
  
  @override
  void initState() {
    //  currentUser = Provider.of<UserState>(context,listen: false).getUser();
     token = Provider.of<UserState>(context,listen: false).getToken();
    super.initState();
  }
  saveGeneralMsg() async {
    String url = '${baseAPi}papi/message';
    Map data = {"apiRequest":{"action":"new-mobile"},"data":[{
      "message": _radioValue != 5 ? _question[_radioValue] : remarkController.text
    }]};
    print(data);
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
          "Content-Type": "application/json",
          "Authorization":"bearer "+token
        },
    );
    // print(json.decode(response.body)['data']);
    // print(response.statusCode);
    
    setState(() {
    });
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(            
            content: Text('${json.decode(response.body)['apiResponse']['desc']}'),
          );
        }
      );
    Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (context) => messengerScreen()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomPadding: false,
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text('Message',style: TextStyle(
            color: Colors.black
          ),),
          leading: IconButton( 
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pop(context);
            },            
          )
        ),
        // body: content(context),
        body: newLayout(context),
      ),
    );
  }

  Widget newLayout(BuildContext context){
    return LayoutBuilder(
    builder: (BuildContext context, BoxConstraints viewportConstraints) {
      return SingleChildScrollView(
        child: ConstrainedBox(          
          constraints: BoxConstraints(
            minHeight: viewportConstraints.maxHeight,
          ),
          child: IntrinsicHeight(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.all(20),
                child: Text('Question for you'),
                width: double.infinity,
              ),          
              Padding(                
                padding: const EdgeInsets.only(
                  left: 10,
                  right: 10,
                  top: 5,
                  bottom: 5
                ),
                child: Row(
                children: <Widget>[
                  Radio(
                    groupValue: _radioValue, 
                    onChanged: onChangeSelectRadio, 
                    value: 0,
                  ),
                  Text('Promotion'),
                ],
            ),
          ),
            Padding(
              padding: const EdgeInsets.only(
                left: 10,
                right: 10,
                top: 5,
                bottom: 5
              ),
              child: Row(
                children: <Widget>[
                  Radio(
                    groupValue: _radioValue, 
                    onChanged: onChangeSelectRadio, 
                    value: 1,
                  ),
                  Text('Ask for the lastest service work'),
                ],
              ),
            ),
          Padding(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
              top: 5,
              bottom: 5
            ),
            child: Row(
              children: <Widget>[
                Radio(
                  groupValue: _radioValue, 
                  onChanged: onChangeSelectRadio, 
                  value: 2,
                ),
                Text('Contact work outstanding'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
              top: 5,
              bottom: 5
            ),
            child: Row(
              children: <Widget>[
                Radio(
                  groupValue: _radioValue, 
                  onChanged: onChangeSelectRadio, 
                  value: 3,
                ),
                Text('How to cancel service'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
              top: 5,
              bottom: 5
            ),
            child: Row(
              children: <Widget>[
                Radio(
                  groupValue: _radioValue, 
                  onChanged: onChangeSelectRadio, 
                  value: 4,
                ),
                Text('Popular service'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
              top: 5,
              bottom: 5
            ),
            child: Row(
              children: <Widget>[
                Radio(
                  groupValue: _radioValue, 
                  onChanged: onChangeSelectRadio, 
                  value: 5,
                ),
                Text('Others'),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
              top: 5,
              bottom: 5
            ),
            child: GestureDetector(            
              child: TextFormField(
                enabled: enableInput,
                maxLines: null,
                controller: remarkController,
                keyboardType: TextInputType.multiline,
                decoration:InputDecoration(
                    labelText: 'Specify a Message or Question',
                    hintText: 'Specify a Message or Question',
                    border: OutlineInputBorder()
                )
              ),
            ),
          ),
          Spacer(),
          Padding(
            padding: const EdgeInsets.only(
              left: 10,
              right: 10,
              top: 5,
              bottom: 5
            ),
            child: SizedBox(
              width: double.infinity,
              child: RaisedButton(                
                  color: Colors.deepOrange,
                  child: Text('Sent',style: TextStyle(
                    color: Colors.white
                  ),),
                  onPressed: () async {                    
                    await saveGeneralMsg();
                  },
              ),
            ),
          )        
              ],
            ),
          ),
        ),
      );
    },
  );
  }


  void onChangeSelectRadio(int value){
    setState(() {
      setState(() {
        if (value == 5) {
        enableInput =true;
      } else {
        enableInput =false;
        remarkController.clear();
      }    
      });
      _radioValue = value;
    });
  }
}