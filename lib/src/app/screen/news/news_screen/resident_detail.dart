import 'package:chartium/src/app/components/icon/email_icon.dart';
import 'package:chartium/src/app/components/icon/fax_icon.dart';
import 'package:chartium/src/app/components/icon/telephone_icon.dart';
import 'package:chartium/src/app/screen/news/models/news_models.dart';

import 'package:flutter/material.dart';

class residentDetail extends StatelessWidget   {  
  
  NewsModels title;
  residentDetail({Key key,@required this.title}) :super(key:key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(      
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: IconButton(
            color: Colors.black,
            icon: Icon(Icons.arrow_back),
            onPressed: () => {
              Navigator.pop(context)
            },
          ),     
          title: Text('Press Contract',style: TextStyle(color: Colors.black)),          
        ),
        body: content(context),
      ),
    );  
  }

  Widget content(BuildContext context){
    return Container(
      padding: EdgeInsets.all(20),
      // color: Colors.red,
      height: 230,
      child:Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Text('CHATRIUM HOTELS & RESIDENCES',style: TextStyle(
              // color: Colors.grey
          )),          
          Text('KANOKRAT KIRKLAND',style: TextStyle(
              color: Colors.grey
          )),
          Text('Grop Director of Marketing Communications',style: TextStyle(
              color: Colors.grey
          )),
          Spacer(),
          Text('CHATRIUM HOTELS & RESIDENCES,291 Soi Naradhiwas Rajanagarindra 24,New Sathon Road, Bangkok 10120 Thailand',style: TextStyle(
              color: Colors.grey
          )),
          Spacer(),
          Row(
            children: <Widget>[
              FaxIcon(),          
              Container(margin:EdgeInsets.only(right: 5),),
              Text('Fax: ++66 (0) 2672 0190'),
            ],
          ),
          Row(
            children: <Widget>[
                TelePhoneIcon(),
                Container(margin:EdgeInsets.only(right: 5),),
                Text('Tel: ++66 (0) 2672 0190'),
            ],
          ),          
          Row(
            children: <Widget>[
              EmailIcon(),
              Container(margin:EdgeInsets.only(right: 5),),
              Text('Email: rawkidz@hotmail.com'),
            ],
          )
          
        ],
      ),
    );
  }
}

