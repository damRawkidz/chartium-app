import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/screen/news/news_screen/resident.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'fag.dart';
import 'global_news.dart';
import 'messenger_screen.dart';
import 'notification.dart';
class newsTabScreen extends  StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _newsTabScreenState();
  }

}

class _newsTabScreenState extends State<newsTabScreen> {
  UserModel currentUser;
  String token;
  @override
  void initState() { 
    super.initState();
    token = Provider.of<UserState>(context,listen: false).getToken();
    print(token);
  }





  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 2,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          leading: Builder(
                  builder: (BuildContext context) {
                    return IconButton(
                    icon: Icon(Icons.help_outline),
                    color: Colors.black,
                    tooltip: MaterialLocalizations.of(context).openAppDrawerTooltip,
                    onPressed: (){
                      Navigator.push(context,
                        MaterialPageRoute(builder: (context) => FAQ()),
                      );
                    },
                  );
                },
          ),
          title: Text(
                'News',
                style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          actions: <Widget>[ 
                IconButton(                  
                  icon: Icon(Icons.chat_bubble_outline),
                  tooltip: 'Air it',
                  color: Colors.black,
                  onPressed: (){
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => messengerScreen()),
                    );
                  },
                ),
                // IconButton(
                //   icon: Icon(Icons.notifications_none),
                //   tooltip: 'Restitch it',
                //   color: Colors.black,
                //   // onPressed: (){
                //   //   Navigator.push(context,
                //   //       MaterialPageRoute(builder: (context) => notificationScreen()),
                //   //   );
                //   // },
                // ),
              ],    
          bottom: TabBar(
            labelColor: Colors.black,
            indicatorColor: Color.fromRGBO(180, 151, 96, 1),
            unselectedLabelColor: Color.fromRGBO(180, 151, 96, 1),            
            tabs: <Widget>[
              Tab(text: 'Global'),
              Tab(text: 'My Resident')
            ],
          ),
        ),
        body: TabBarView(children: <Widget>[
            globalNewsScreen(),
            residentScreen()
        ],
        ),
      ),
    );
  }
}