import 'package:flutter/material.dart';

import 'news_screen/news_tab.dart';

class newsContainerScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {

    return newsContainerScreenState();
  }
}

class newsContainerScreenState extends State<newsContainerScreen> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Container',
      home: Scaffold(
        body: Container(
          child: newsTabScreen(),
        ),
      ),
    );
  }

}