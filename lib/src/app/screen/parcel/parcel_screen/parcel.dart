import 'package:chartium/src/app/models/renter_profile_model.dart';
import 'package:chartium/src/app/providers/renter_profile_provider.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:qr_flutter/qr_flutter.dart';

import 'models/new_parcel_screen.dart';

class Parcel extends StatelessWidget with BaseApi {
  NewsParcelModel parcel;
  RenterProfileModel _renterProfile;
  int id;
  Parcel({Key key,@required this.parcel}):super(key:key);
  @override
  Widget build(BuildContext context) {
    _renterProfile = Provider.of<RenterProfileProvider>(context,listen: false).getRenterProfile();
    id = Provider.of<UserState>(context,listen: false).getId();
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () { Navigator.pop(context);}, 
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
          ),
          centerTitle: true,
          title: Text('Parcel',style: TextStyle(
            color: Colors.black
          ),),
          backgroundColor: Colors.white,
        ),
        body: content(context),
      ),
    );
  }


  Widget content(BuildContext context){
    return SingleChildScrollView(
      child: Container(
      padding: EdgeInsets.all(20.0),
      width: double.infinity,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          header(context),
          qrCode(),
          footer(),
          Container(            
            padding: EdgeInsets.only(top: 50.0),
            child: Text('Please pick up parcel with in 08:30 - 18:00 every day',style: TextStyle(
              color: Colors.red
            ),),
          ),
          Container(            
            padding: EdgeInsets.only(top: 50.0),
            child: Text('Except public holiday',style: TextStyle(
              color: Colors.red
            ),),
          )
        ],
      ),
    )
    );
  }

  Widget header(BuildContext context){
    return Container(
      width: double.infinity,
      decoration: BoxDecoration(
        color: Color.fromRGBO(180, 151, 96, 1),
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(5.0),
          topRight: Radius.circular(5.0)
        ),
        border: Border.all(color: Color.fromRGBO(139, 153, 159, 1)),
      ),
      child: Container(
        margin:EdgeInsets.all(20),
        // child: Center(child: Text('data'),),
        child: Center(
          child: Text('${parcel.parcelTypeCode}',style: TextStyle(
            color: Colors.white,
            fontWeight: FontWeight.bold,
            fontSize: 15
          ),),
        ),
      ),
    );
  }

  
  Widget qrCode(){
    return Container(
      height: 400.0,
      width: double.infinity,
      decoration: BoxDecoration(
        border: Border(
          left: BorderSide(
            color: Color.fromRGBO(139, 153, 159, 1)
          ),
          right: BorderSide(
            color: Color.fromRGBO(139, 153, 159, 1)
          ),
        )
      ),
      child: Container(
        width: double.infinity,
        child: RepaintBoundary(
          child: QrImage(
            // data:'http://chatrium.thamming.com/updateParcel/${parcel.id}_${id}',
            data:'${parcel.id}',
            gapless: true,
            errorCorrectionLevel: QrErrorCorrectLevel.L,
            size: 25.0,            
            onError: (err){
              print(err);
            },
          ),
        ),
      ),
      
    );
  }

Widget footer(){
  return Container(
    width: double.infinity,
    decoration: BoxDecoration(        
        border: Border(
          left: BorderSide(
            color: Color.fromRGBO(139, 153, 159, 1)
          ),
          right: BorderSide(
            color: Color.fromRGBO(139, 153, 159, 1)
          ),
          bottom: BorderSide(
            color: Color.fromRGBO(139, 153, 159, 1),            
          )
        ),
        // borderRadius: BorderRadius.only(
        //   bottomLeft: Radius.circular(5.0),
        //   bottomRight: Radius.circular(5.0)
        // ),        
    ),
    child: Container(
      margin:EdgeInsets.all(20) ,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Room No.: '),
              TextSpan(text: '${parcel.roomNo}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
          RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Parcel To: '),
              TextSpan(text: '${parcel.to}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
          RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Parcel No.: '),
              TextSpan(text: '${parcel.code}',style: TextStyle(fontWeight: FontWeight.bold))

            ]
            ),
          ),
           RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Arrival Date.: '),
              TextSpan(text: '${parcel.arrivalDate ==  null ? '' : formatter.format(DateTime.parse(parcel.arrivalDate.substring(0,10)))}',style: TextStyle(fontWeight: FontWeight.bold))              
            ]
            ),
          ),
           parcel.parcelStatusId == 2 ? RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children:  <TextSpan>[
              TextSpan(text: 'Received Date: '),
              TextSpan(text: '${parcel.receiveDate ==  null ? '' : formatter.format(DateTime.parse(parcel.receiveDate.substring(0,10)))}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ):Container(),
          parcel.parcelStatusId == 3 ? RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children:  <TextSpan>[
              TextSpan(text: 'Return Date: '),
              TextSpan(text: '${parcel.returnDate ==  null ? '' : formatter.format(DateTime.parse(parcel.returnDate.substring(0,10)))}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ):Container(),                    
        ],
      ),      
    ),
  );
}
}