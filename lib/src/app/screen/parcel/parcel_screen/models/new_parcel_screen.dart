import 'package:intl/intl.dart';
class NewsParcelModel {
  int roomId;
  String roomNo;
  int accountId;
  String accountTitle;
  String accountFirstName;
  String accountLastName;
  String to;
  String code;
  int parcelTypeId;
  String parcelTypeCode;
  String parcelTypeName;
  int parcelStatusId;
  String parcelStatusCode;
  String parcelStatusName;
  String arrivalDate;
  String receiveDate;
  int receiveById;
  String receiveByTitle;
  String receiveByFirstName;
  String receiveByLastName;
  String receiverName;
  String receiverImageUrl;
  String receiverSignatureImageUrl;
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;
  String returnDate;
  var _formatter = DateFormat("dd-MM-yyyy");
  NewsParcelModel(
      {this.roomId,
      this.roomNo,
      this.accountId,
      this.accountTitle,
      this.accountFirstName,
      this.accountLastName,
      this.to,
      this.code,
      this.parcelTypeId,
      this.parcelTypeCode,
      this.parcelTypeName,
      this.parcelStatusId,
      this.parcelStatusCode,
      this.parcelStatusName,
      this.arrivalDate,
      this.receiveDate,
      this.receiveById,
      this.receiveByTitle,
      this.receiveByFirstName,
      this.receiveByLastName,
      this.receiverName,
      this.receiverImageUrl,
      this.receiverSignatureImageUrl,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.lastUpdateDate,
      this.returnDate});

  NewsParcelModel.fromJson(Map<String, dynamic> json) {
    returnDate =json['returnDate'];
    roomId = json['roomId'];
    roomNo = json['roomNo'];
    accountId = json['accountId'];
    accountTitle = json['accountTitle'];
    accountFirstName = json['accountFirstName'];
    accountLastName = json['accountLastName'];
    to = json['to'];
    code = json['code'];
    parcelTypeId = json['parcelTypeId'];
    parcelTypeCode = json['parcelTypeCode'];
    parcelTypeName = json['parcelTypeName'];
    parcelStatusId = json['parcelStatusId'];
    parcelStatusCode = json['parcelStatusCode'];
    parcelStatusName = json['parcelStatusName'];
    arrivalDate = json['arrivalDate'];
    receiveDate = json['receiveDate'];
    receiveById = json['receiveById'];
    receiveByTitle = json['receiveByTitle'];
    receiveByFirstName = json['receiveByFirstName'];
    receiveByLastName = json['receiveByLastName'];
    receiverName = json['receiverName'];
    receiverImageUrl = json['receiverImageUrl'];
    receiverSignatureImageUrl = json['receiverSignatureImageUrl'];
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['roomId'] = this.roomId;
    data['roomNo'] = this.roomNo;
    data['accountId'] = this.accountId;
    data['accountTitle'] = this.accountTitle;
    data['accountFirstName'] = this.accountFirstName;
    data['accountLastName'] = this.accountLastName;
    data['to'] = this.to;
    data['code'] = this.code;
    data['parcelTypeId'] = this.parcelTypeId;
    data['parcelTypeCode'] = this.parcelTypeCode;
    data['parcelTypeName'] = this.parcelTypeName;
    data['parcelStatusId'] = this.parcelStatusId;
    data['parcelStatusCode'] = this.parcelStatusCode;
    data['parcelStatusName'] = this.parcelStatusName;
    data['arrivalDate'] = this.arrivalDate;
    data['receiveDate'] = this.receiveDate;
    data['receiveById'] = this.receiveById;
    data['receiveByTitle'] = this.receiveByTitle;
    data['receiveByFirstName'] = this.receiveByFirstName;
    data['receiveByLastName'] = this.receiveByLastName;
    data['receiverName'] = this.receiverName;
    data['receiverImageUrl'] = this.receiverImageUrl;
    data['receiverSignatureImageUrl'] = this.receiverSignatureImageUrl;
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    data['returnDate'] = this.returnDate;
    return data;
  }

}