import 'dart:convert';

import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/parcel/parcel_screen/models/new_parcel_screen.dart';
import 'package:chartium/src/app/screen/parcel/parcel_screen/models/parcel_status.dart';
import 'package:chartium/src/app/screen/parcel/parcel_screen/parcel.dart';
import 'package:http/http.dart' as http;
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class parcelContainerScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _parcelState();
  }
  
}
class _parcelState extends State<parcelContainerScreen> with BaseApi{
  List<ParcelStatus> parcelStatusList = [];
  List<NewsParcelModel> parcels = [];
  List<NewsParcelModel> tempParcels = [];
  int  parcelStatusId = 7;
  @override
  void initState() { 
    token = Provider.of<UserState>(context,listen: false).getToken();
    super.initState();
    callParcel(); 
    callHttp();  
  }
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Parcel',style: TextStyle(
            color: Colors.black
          ),),
        ),
        // body: parcelTabScreen(),
        body: newParcels(),
      ),
    );
  }

  Widget newParcels(){
    return Column(
      children: <Widget>[
        dropDownWorkOrderType(),
        Expanded(
          child: listParcels(),
        )        
      ],
    );
  }

  Widget dropDownWorkOrderType(){
    return  Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        margin: EdgeInsets.all(15.0),
        decoration: BoxDecoration(              
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(
          color: Colors.grey, style: BorderStyle.solid, width: 0.80),
        ),
        child: DropdownButton(
          isExpanded: true,
          hint: Text('Select Work Order Type'),
          items: parcelStatusList.map<DropdownMenuItem<int>>((value) {
            return DropdownMenuItem<int>(
              value: value.id,
              child: Text('${value.name}',textAlign: TextAlign.right,),
            );
          }).toList(),
          onChanged: (id) {
              parcelStatusId = id;
              if(id == 7){
                parcels = tempParcels;
                
              } else {
                parcels = tempParcels.where( (parcel) => parcel.parcelStatusId == id).toList();
                
              }

            sortArray();
            
            setState(() {

            });
          },
          value: parcelStatusId,
      ),
    );
  }


  Widget listParcels(){
    return parcels.isEmpty ? Text('No Data To Display') : ListView.builder(
      itemCount: parcels.length,
      itemBuilder: (BuildContext context,int index) {
        return GestureDetector(
          onTap: (){
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => Parcel(parcel:parcels[index])),
              );
          },
          child:Card(
            child: Container(
            height: 110,
              child: Container(
                padding: EdgeInsets.all(5),
                decoration: BoxDecoration(
                    border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
                ),
                child: Container(
                  // color: Colors.red,
                  padding: EdgeInsets.all(5),
                  child: Row(
                    children: <Widget>[
                      img(),
                      maincontent(parcels[index]),
                    ],
                  )
                ),
            ),
           )        ,
          )
        );
      },
    );
  }



  Widget img(){
    return Image.asset('assets/images/Parcel.webp',width: 100,height: 90);
  }

  Widget maincontent(NewsParcelModel newSParcel){
      return Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          // Row(
          //   children: <Widget>[
          //     Text('To: '),
          //     Text('${newSParcel.to}',style: TextStyle(
          //   fontWeight: FontWeight.bold
          // ),),
          //   ],
          // ),
          RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'To: '),
              TextSpan(text: '${newSParcel.to}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
          RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Parcel Code: '),
              TextSpan(text: '${newSParcel.code}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
          // Text('Parcel No: ${newSParcel.code}'),
          // Text('Parcel Type: ${newSParcel.parcelTypeName}'),
          RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Parcel Type: '),
              TextSpan(text: '${newSParcel.parcelTypeCode}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),          
          // Text('Status: ${newSParcel.parcelStatusName} '),
          RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Status: '),
              TextSpan(text: '${newSParcel.parcelStatusName}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
        ],
      );
  }

  callHttp() async {
    String url = '${baseAPi}papi/ParcelStatus';
    Map data = {"apiRequest":{"action":"list"}};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {"Content-Type": "application/json"},
    );
    (json.decode(response.body)['data'] as List).forEach((parcelStatus){      
      parcelStatusList.add(ParcelStatus.fromJson(parcelStatus));
    });
    setState(() {});
  }

  callParcel() async {
    String url = '${baseAPi}papi/Parcel';
    Map data = {"apiRequest":{"action":"list-mobile"},"data":[{
          // "renterProfileId":currentUser.renterId
      }]};
    print(token);
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
        },
    );
    print(response.body);
    (json.decode(response.body)['data'] as List).forEach((parcel){
      parcels.add(NewsParcelModel.fromJson(parcel));        
    });
  tempParcels = parcels;
  sortArray();
  setState(() {
    
  });    
  }

  sortArray(){
    parcels.sort((a,b) => b.createDate.compareTo(a.createDate));
  }
  
}