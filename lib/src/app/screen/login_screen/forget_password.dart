import 'dart:convert';

import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/profile/models/account_models.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'forget_password_success_screen.dart';
import 'package:http/http.dart' as http;

class forgetPasswordScreen extends StatefulWidget  {
  @override
  _forgetPasswordScreenState createState() => _forgetPasswordScreenState();
}

class _forgetPasswordScreenState extends State<forgetPasswordScreen> with BaseApi {
  final _inputPasswordController = TextEditingController();
  final _inputFirstNameController = TextEditingController();
  final _inputLastnamedController = TextEditingController();
  final _inputEmailController = TextEditingController();

  final formkey = GlobalKey<FormState>();
  int id;
  AccountModel account;
  bool isShowPassword = false;

  @override
  void initState() {
    id = Provider.of<UserState>(context,listen: false).getId();
    token = Provider.of<UserState>(context,listen: false).getToken();  
    print(token);

    if(token !=null){
      callHttp();
    }    

    super.initState();
  }

  @override
  Widget build(BuildContext context) {      
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            onPressed: () { Navigator.pop(context);}, 
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('Reset Password',style: TextStyle(
            color: Colors.black
          ),),
        ),
        body: content(context),
      ),
    );
  }

  Widget content(BuildContext context){
    return SingleChildScrollView(
          child: Form(
            key: formkey,
            child: Container(
            padding: EdgeInsets.all(10),
              child: Column(        
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                  _firstName(),
                  Container(margin: EdgeInsets.only(top: 20.0),),
                  _lastName(),
                  Container(margin: EdgeInsets.only(top: 20.0),),
                  _email(),
                  Container(margin: EdgeInsets.only(top: 20.0),),
                  _inputPassword(),
                  Container(margin: EdgeInsets.only(top: 20.0),),
                  _submitButton(context),
            ],
        ),
      ),
          ),
    );
  } 

  Widget _title(){
    return Container(
      margin: EdgeInsets.only(bottom: 50),
      child: Text('Please enter your email or mobile number and we will send you OTP to verify later',
      style: TextStyle(color: Color.fromRGBO(189, 184, 184, 1)),),
    );  
  }

  Widget _inputPassword(){
    return Container(
      child: TextFormField(
      controller: _inputPasswordController,
      obscureText: isShowPassword ? false : true,
      decoration: InputDecoration(
          // labelText: 'Password',
          hintText: 'Password',
          border: OutlineInputBorder(),
          suffixIcon: IconButton(
          onPressed: () =>  {
          setState(()  {
            isShowPassword = !isShowPassword;
          })
          },
          icon: Icon(Icons.remove_red_eye),
        ),
        ),
        validator: (String text){
          if (text.isEmpty) {
            return 'Plase enter password';
          }

          return null;
        },
      ),
    );   
  }

  Widget _email(){
    return Container(
      child: TextFormField(
      controller: _inputEmailController,
      decoration: InputDecoration(
          hintText: 'you@example.com',  
          border: OutlineInputBorder()
        ),
      validator: (String text){
        if (text.isEmpty) {
          return 'Plase enter email';
        }

        return null;
      },
      ),      
    );   
  }

  Widget _firstName(){
    return Container(
      child: TextFormField(
      controller: _inputFirstNameController,
      // obscureText: true,
      decoration: InputDecoration(
          // labelText: 'Password',
          hintText: 'Firstname',  
          border: OutlineInputBorder()
        ),
        validator: (String text){
          if (text.isEmpty) {
            return 'Plase enter firstname';
          }

          return null;
        },
      ),
    );   
  }

  Widget _lastName(){
    return Container(
      child: TextFormField(
      controller: _inputLastnamedController,
      // obscureText: true,
      decoration: InputDecoration(
          // labelText: 'Password',
          hintText: 'Lastname',  
          border: OutlineInputBorder()
        ),
      validator: (String text){
        if (text.isEmpty) {
          return 'Plase enter lastname';
        }

        return null;
      },
      ),
    );   
  }

  Widget _submitButton(BuildContext context){
    return SizedBox(
      width: double.infinity,
      child: RaisedButton(      
      color: Colors.deepOrange,
      child: Text('Send',style: TextStyle(
        color:Colors.white
      ),),
      onPressed: (){
      showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(            
            title: Text('Reset Password'),
            content: Text('Are you sure to change password?'),
            actions: <Widget>[
              FlatButton(
                child: Text('Sure'),
                onPressed: (){
                    
                    Navigator.of(context, rootNavigator: true).pop();

                    if (!formkey.currentState.validate()) return _errorDialog(context,'Please enter require form');

                    if (token !=null) {
                        _changePassword(context);    
                    } else {
                      getAccountWhenForgetPassword();                      
                    }
                },
              ),
              FlatButton(
                child: Text('Cancel'),
                onPressed: () {
                  Navigator.of(context, rootNavigator: true).pop();
                },
              )
            ],
          );
        }
      );
        // Navigator.push(context,MaterialPageRoute(builder: (context) => checkEmailScreen()),);
      }      
      )
    );
  }

  getAccountWhenForgetPassword() async {
    String url = '${baseAPi}papi/accountmobile';
    Map data = {"apiRequest":{"action":"get"},"data":[{
      "firstName": _inputFirstNameController.text,
      "lastName": _inputLastnamedController.text,
      "email": _inputEmailController.text
    }]};
  var body = json.encode(data);
  var response = await http.post(url,
    body: body,
    headers: {
        "Content-Type": "application/json",
        // "Authorization":"bearer "+token
      },
  );  
  // print(response.statusCode);

  List list = json.decode(response.body)['data'];
  
    if (list.length != 0) {
      account = AccountModel.fromJson(json.decode(response.body)['data'][0]);
      _changePasswordWithOutToken();
    } else {
     _errorDialog(context,'information not corrlect');
    
    }
  }

  callHttp() async {
  String url = '${baseAPi}papi/account';
  Map data = {"apiRequest":{"action":"get"},"data":[{
    "id": id
    }]};
  var body = json.encode(data);
  var response = await http.post(url,
    body: body,
    headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
      },
  );  
  print(response.statusCode);
  // print(response.body);
  account = AccountModel.fromJson(json.decode(response.body)['data'][0]);
  print(account);
  }

  _successDialog(){
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(            
            title: Text('Reset Success'),
          );
        }
      );
  }

  _errorDialog(BuildContext context,String content){
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text('Can not reset password'),
            // content: Text('information not corrlect'),
            content: Text('$content'),
          );
        }
    );
  }

  

_changePasswordWithOutToken() async {
    String url = '${baseAPi}papi/accountmobile';
    Map data = {"apiRequest":{"action":"change_password"},"data":[{
        "roomId": account.roomId,
        "title": account.title,
        "firstName": account.firstName,
        "lastName": account.lastName,
        "email": account.email,
        "imageUrl": account.imageUrl,
        "password": _inputPasswordController.text,
        "countryId": account.countryId,
        "countryCode": account.countryCode,
        "countryName": account.countryName,
        "propertyId": account.propertyId,
        "roomNo": account.roomNo,
        "id": account.id,
        "createBy": account.createBy
      }]};
    var body = json.encode(data);
    var response = await http.post(url,
     body: body,
      headers: {
        "Content-Type": "application/json",
        // "Authorization":"bearer "+token
      },
    );  
    print(response.statusCode);
    _inputPasswordController.clear();
    
    Navigator.of(context, rootNavigator: true).pop();
    _successDialog();
  }


  _changePassword(BuildContext context) async {
    String url = '${baseAPi}papi/account';
    Map data = {"apiRequest":{"action":"change_password"},"data":[{
        "roomId": account.roomId,
        "title": account.title,
        "firstName": account.firstName,
        "lastName": account.lastName,
        "email": account.email,
        "imageUrl": account.imageUrl,
        "password": _inputPasswordController.text,
        "countryId": account.countryId,
        "countryCode": account.countryCode,
        "countryName": account.countryName,
        "propertyId": account.propertyId,
        "roomNo": account.roomNo,
        "id": account.id,
        "createBy": account.createBy
      }]};
    var body = json.encode(data);
    var response = await http.post(url,
     body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
      },
    );
    print('_changePassword');
    print(response.statusCode);
    _inputPasswordController.clear();
    // Navigator.of(context, rootNavigator: true).pop();
    _successDialog();
  }
}