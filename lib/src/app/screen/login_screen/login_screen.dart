import 'package:chartium/src/app/components/chartium_progress_bar.dart';
import 'package:chartium/src/app/providers/id_provider.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/main/main_screen.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'forget_password.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'models/userModel.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoginScreenState();
  }

}
class LoginScreenState extends State<LoginScreen> with BaseApi{
  bool isRememberMe = false;
  UserModel currentUser;
  final _inputEmailController = TextEditingController();
  final _inputPasswordController = TextEditingController();
  bool isShowPorgress = false;
  bool isShowPassword = false;
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider(
      builder: (context) => UserState(),
      child: 
      MaterialApp(        
        home: Scaffold(
          resizeToAvoidBottomInset:false,
          body: isShowPorgress ? ChartiumProgressBar() : mainContainer()
        ),
      ),
    );     
  }

  Widget mainContainer (){
    return   Container(
      
          padding:EdgeInsets.all(40.0),
          child: Form(
            child:  Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              makeText(),
              Container(margin: EdgeInsets.only(bottom: 20),),
              Container(
                padding: EdgeInsets.only(left: 10),
                child: label('Email'),
              ),
              inputUsername(),
              Container(margin: EdgeInsets.only(bottom: 20),),
              Container(
                padding: EdgeInsets.only(left: 10),
                child: label('Password'),
              ),
              inputPassword(),
              // Container(margin: EdgeInsets.only(bottom: 20),),
              forgetPassword(),
              Container(margin: EdgeInsets.only(bottom: 20),),
              submitButton(context),
              Container(margin: EdgeInsets.only(bottom: 20),),
          ],
        ),
        )
    );
  }

  Widget makeText(){
    return Text('Welcome Renter',style: TextStyle(
        // fontWeight: FontWeight.bold
    ),);
  }

  Widget forgetPassword(){    
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
                child: Row(children: <Widget>[
                  Checkbox(                  
                    onChanged: (bool value) => setState(() {isRememberMe = value;}), 
                    value: isRememberMe,
                    materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,                    
                ),
              Text('Remember me'),
              ],
            ),
          ),
          GestureDetector(
            onTap: (){
              Navigator.push(context,
                   MaterialPageRoute(builder: (context) => forgetPasswordScreen()),
              );
            },
            child: Text('Reset Password'),
          )          
        ], 
      ),
    );
  }
  Widget label(String text){
    return Container(
      margin: EdgeInsets.only(bottom: 10),
      child: Align(
        alignment: Alignment.centerLeft,
        child: Text('$text'),
        ),
    );
  }
  Widget inputUsername(){
    return Container(
      padding: EdgeInsets.only(left: 10),
      child: TextFormField(
      controller:  _inputEmailController,
      keyboardType: TextInputType.emailAddress,
      decoration: InputDecoration(
        hintText: 'you@example.com',
        border: OutlineInputBorder()
      ),
    ),
    );
  }
  Widget inputPassword(){
    return Container(
      padding: EdgeInsets.only(left: 10),
      child: TextFormField(
      controller:  _inputPasswordController,
      obscureText: isShowPassword ? false : true,
      decoration: InputDecoration(
        suffixIcon: IconButton(
          onPressed: () =>  {
          setState(()  {
            isShowPassword = !isShowPassword;
          })
          },
          icon: Icon(Icons.remove_red_eye),
        ),
        // labelText: 'Password',
        hintText: 'Password',
        border: OutlineInputBorder()
      ),
    ),
    );
    
  }

  Widget submitButton(BuildContext context){
    return Container(
      padding: EdgeInsets.only(left: 10),
      child: SizedBox(
      width: double.infinity,
      child: RaisedButton(
      color: Colors.deepOrange,
      child: Text('LOGIN',style: TextStyle(
        color: Colors.white
      ),),
      onPressed: (){
        setState(()  {
          isShowPorgress = true;
          login(context);
        });
        
      },
      ),
    ),
    );
  }

  login(BuildContext ctx) async {
    try {
      String url = '${baseAPi}papi/login';
    Map data = {      
        "email": _inputEmailController.text,
        "password":_inputPasswordController.text,
        "source":'mobile'
    };
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {"Content-Type": "application/json"},
    );

      if (response.statusCode == 200) {
          Provider.of<UserState>(ctx,listen: false).setUserModel(UserModel.fromJson(json.decode(response.body)));
          print("-----------");
          Provider.of<UserId>(ctx,listen: false).setId(json.decode(response.body)['id']);                    
          if (isRememberMe) {
            _setSharedPeFerrence(UserModel.fromJson(json.decode(response.body))); 
          }
          Navigator.of(context).pushReplacement(
            MaterialPageRoute(builder: (context) => MainScreen(page: 0))
          );
     
    } else {
      showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(            
            title: Text('Can not log in!'),
            content: Text('Please check your User name and Password'),
          );
        }
      );
      setState(() {
        this.isShowPorgress = false;
      });
    }  
    } catch (e) {
      showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(
            title: Text('Can not log in!'),
            content: Text('Please check your User name and Password'),
          );
        }
      );
    }
  
  }

  

  void _setSharedPeFerrence(UserModel user) async {
    SharedPreferences sp = await SharedPreferences.getInstance();
    print('store on sp');
    await sp.setString('token', user.token);
    await sp.setString('name', user.name);
    await sp.setString('url', user.imageUrl);
    await sp.setString('pty', user.propertyName);
    await sp.setString('id', user.id.toString());
  }
}