import 'package:flutter/material.dart';

class checkEmailScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed:  (){
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          title: Text('Reset Password',style: TextStyle(
            color: Colors.black
          ),),
        ),
        body: content(context),
      ),
    );
  }


  Widget content(BuildContext context){
    return Container(
      padding: EdgeInsets.all(40),
      child: Column(
        children: <Widget>[
          _text(),
          Image.asset('assets/images/message.webp'),
          Container(margin: EdgeInsets.only(bottom: 20),),
          _submitButton(context)          
        ],
      ),
    );
  }
  Widget _text(){
    return Container(
      margin: EdgeInsets.only(bottom: 20),
      child: Text('Please Check your Email',style: TextStyle(
        color: Colors.orange
      ),),
    );
  }
  Widget _submitButton(BuildContext context){
    return SizedBox(
      width: double.infinity,      
      child: RaisedButton(
        color: Colors.deepOrange,
        child: Text('Go to Email'),
        onPressed: (){
          print('Handle redirect to Email');
        },
      ),
    );
  }
}