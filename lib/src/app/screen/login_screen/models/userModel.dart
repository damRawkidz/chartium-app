// MasterAccount
class UserModel {
  int id;
  String name;
  String token;
  String refreshToken;
  String tokenExpiryDate;
  String propertyName;
  String imageUrl;
  List<String> permissions;
  

  UserModel(
      {this.name,
      this.refreshToken,
      this.token,
      this.tokenExpiryDate,
      this.permissions,
      this.propertyName,
      this.imageUrl});

  UserModel.fromJson(Map<String, dynamic> json) {
    id =json['id'];
    name = json['name'];
    refreshToken = json['refreshToken'];
    token = json['token'];
    tokenExpiryDate = json['tokenExpiryDate'];
    permissions = json['permissions'].cast<String>();
    propertyName = json['propertyName'];
    imageUrl = json['imageUrl'];
  }

  setName(String _name){
    this.name = _name;
  }

  setToken(String _token){
    this.token =_token;
  }


  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['name'] = this.name;
    data['refreshToken'] = this.refreshToken;
    data['token'] = this.token;
    data['tokenExpiryDate'] = this.tokenExpiryDate;
    data['permissions'] = this.permissions;
    data['propertyName'] = this.propertyName;
    data['imageUrl'] = this.imageUrl;
    return data;
  }
  
  
}