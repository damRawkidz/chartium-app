import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:chartium/src/app/providers/id_provider.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/screen/news/news_screen/messenger_screen.dart';

import 'package:http/http.dart' as http;
import 'package:chartium/src/app/screen/invoice/invoice_container.dart';
import 'package:chartium/src/app/screen/news/news_container_screen.dart';
import 'package:chartium/src/app/screen/parcel/parcel_container.dart';
import 'package:chartium/src/app/screen/profile/profile.dart';
import 'package:chartium/src/app/screen/work_order/work_order_container.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:provider/provider.dart';

class MainScreen extends StatefulWidget {
  
  int page = -1;
  MainScreen({Key key,this.page}) : super(key:key);
  @override
  State<StatefulWidget> createState() {
    return NewsScreenState();
  }
}

class NewsScreenState extends State<MainScreen> with BaseApi{
  bool isIOS = null;
  final FirebaseMessaging _fcm = FirebaseMessaging();
  StreamSubscription iosSubscription;
  UserModel currentUser;
  String fcmToken;
  String action;
  @override
  void initState()  {    
    super.initState();
    
    userId = Provider.of<UserId>(context,listen: false).getid();
    currentUser = Provider.of<UserState>(context,listen: false).getUser();
    if (widget.page != -1) {
        setState(() {
          this.stateIndex = widget.page;
      });
    }
    if (Platform.isIOS) {
      iosSubscription = _fcm.onIosSettingsRegistered.listen((data) {
        //print(data);
        _saveDeviceToken();
      });

      _fcm.requestNotificationPermissions(IosNotificationSettings());
    } else {
      _saveDeviceToken();
    }
    // _fcm.requestNotificationPermissions();
    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('data from push ${message['data']}');
        print("print ${message['data']['action']}");
        if(message['data']['action'] != null){
          this.action = message['data']['action'];
        } else {
          this.action = message['action'];
        }
        

        showDialog(
          context: context,
          builder: (context) => AlertDialog(
            content: ListTile(
              title: Text(message['notification']['title']),
              subtitle: Text(message['notification']['body']),
            ),
            actions: <Widget>[
              FlatButton(
                color: Colors.amber,
                child: Text('Ok'),
                onPressed: () => routesByaction(),
              ),
            ],
          ),
        );
      },
      onLaunch: (Map<String, dynamic> message) async {
        print("onLaunch: $message");
        // TODO optional
      },
      onResume: (Map<String, dynamic> message) async {
        print("onResume: $message");
        // TODO optional
      },
    );
  }
  routesByaction(){
    switch (this.action) {
      case 'parcel':
        setState(() {
          this.stateIndex = 2;
      });
      break;
      case 'workorder':
        setState(() {
          this.stateIndex = 1;
      });
      break;
      case 'invoice':
        setState(() {
        this.stateIndex = 3;
      });
      break;
      case 'message':
        // ChatWorkOrderScreen
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => messengerScreen()),
        );
      break;
      default:Navigator.of(context).pop();

    }
    Navigator.of(context, rootNavigator: true).pop();
  }

  @override
  void dispose() {
    if (iosSubscription != null) iosSubscription.cancel();
    super.dispose();
  }

  _saveDeviceToken() async {
      print("Get token");
      fcmToken = await _fcm.getToken();
      print(fcmToken);
      registerToken();
    }
    
    int stateIndex = 0;
    void _incrementTab(int index) {
    setState(() {
      this.stateIndex = index;
    });
  }

  registerToken() async {
  print('check platfrom');
  bool isIOS = Theme.of(context).platform == TargetPlatform.iOS;
  print(isIOS);
  print(isIOS ? "I":"A");
  String url = '${baseAPi}papi/device';
    Map data = {
    "apiRequest": {
    "action": "update"
    },"data": [
      {
        "type":currentUser.permissions[0],
        "userId":userId,
        "deviceType": isIOS ? "I":"A",
        "token":fcmToken
      }
    ]
  };
  print(data);
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {"Content-Type": "application/json"},
    );
    print(response.body);
  }

  @override
  Widget build(BuildContext context) {
    return mainContainer(stateIndex);
  }






  Widget mainContainer(int stateIndex) {
    final List<Widget> _pages = [
      newsContainerScreen(),
      workOrderContainerScreen(),
      parcelContainerScreen(),
      invoiceScreen(),
      profile()
    ];
    
    return MaterialApp(
      title: 'Material App',
      
      home: Scaffold(
          body: _pages[stateIndex],
          bottomNavigationBar: _createBottomNavigator(stateIndex)),
    );
  }



  Widget _createBottomNavigator(int stateIndex) {
    return BottomNavigationBar(
      currentIndex: stateIndex,
      type: BottomNavigationBarType.shifting,
      items: <BottomNavigationBarItem>[
        BottomNavigationBarItem(
            icon: Icon(Icons.surround_sound, color: Color.fromARGB(255, 0, 0, 0)),
            title: Text('News',style: TextStyle(color: Colors.black),)
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.assignment, color: Color.fromARGB(255, 0, 0, 0)),
            title: Text('Work Order',style: TextStyle(color: Colors.black),)
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.local_shipping, color: Color.fromARGB(255, 0, 0, 0)),
            title: Text('Parcel',style: TextStyle(color: Colors.black),)
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.monetization_on, color: Color.fromARGB(255, 0, 0, 0)),
            title: Text('Invoice',style: TextStyle(color: Colors.black))
        ),
        BottomNavigationBarItem(
            icon: Icon(Icons.group, color: Color.fromARGB(255, 0, 0, 0)),
            title: Text('Profile',style: TextStyle(color: Colors.black))                        
        )
      ],
      onTap: _incrementTab,
    );
  }
}