import 'package:chartium/src/app/providers/id_provider.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/screen/main/main_screen.dart';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../login_screen/login_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  UserModel _userState;
  
  @override
  void initState() {
    super.initState();
    Future.delayed(const Duration(seconds: 2),() async {
      SharedPreferences sp = await SharedPreferences.getInstance();
      // sp.containsKey('token')
      if (sp.containsKey('token')) {
          String token =  sp.getString('token'); 
          String name = sp.getString('name');
          String url = sp.getString('url');
          String id = sp.getString('id');
          String pty = sp.getString('pty');
          setState(() {});
          print('image url of user'+url);
          Provider.of<UserState>(context,listen: false).restoreState(token,name,url,id,pty);
          // Provider.of<UserState>(context,listen: false).updateImageUrl(url);
          Provider.of<UserId>(context,listen: false).setId(int.parse(id));
          Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => MainScreen(page: 0)),
          );
      }  else {
          Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => LoginScreen()),
          );
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Title',
      home: Scaffold(
        body: Container(
        alignment: FractionalOffset.center,
        child: Image.asset('assets/images/logo.webp'), 
        ),
      ),
    );
  }
}