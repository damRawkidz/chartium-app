import 'dart:convert';

import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/models/work_order_status.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/new_work_order_detail.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/work_order_detail_screen.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/work_order_pending_detail_screen.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/work_order_screen.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'models-v2/work_order_c.dart';
class workOrderContainerScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    
    return _workOrderContainerState();
  }
}

class _workOrderContainerState extends State<workOrderContainerScreen> with BaseApi {
  List<WokrOrderC> workOrderList = [];
  List<WorkOrderStatusModel> workOrderStatusList = [];
  List<WokrOrderC> tempworkOrderList = [];
  int  parcelStatusId = 6;
  
  @override
  void initState() {    
    super.initState();
    token = Provider.of<UserState>(context,listen: false).getToken();
    print(token);
    callWorkOrderStatus();
    callWorkOrder();
  }
  @override
  Widget build(BuildContext context) {    
    return MaterialApp(      
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text('Work Order',style: TextStyle(
            color: Colors.black
          ),),
          actions: <Widget>[
            IconButton(
                  icon: Icon(Icons.add),
                  tooltip: 'Air it',
                  color: Colors.black,
                  onPressed: (){
                      Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => WorkOrderScreen()),
                      ).then((value) => {
                        print(value),
                      });
                  },
                ),
          ],
        ),
        body: _workOrderList(),
      ),
    );
  }

  Widget _workOrderList(){
    return Column(
      children: <Widget>[
        dropDownWorkOrderType(),
        Expanded(
          child: listWorkOrder(),
        )        
      ],
    );
  }

  Widget dropDownWorkOrderType(){
    return  Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        margin: EdgeInsets.all(15.0),
        decoration: BoxDecoration(              
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(
          color: Colors.grey, style: BorderStyle.solid, width: 0.80),
        ),
        child: DropdownButton(
          isExpanded: true,
          hint: Text('Select Work Order Type'),
          items: workOrderStatusList.map<DropdownMenuItem<int>>((value) {
            return DropdownMenuItem<int>(
              value: value.id,
              child: Text('${value.name}',textAlign: TextAlign.right,),
            );
          }).toList(),
          onChanged: (id) {
              print(id);
              parcelStatusId = id;
              if(id == 6){
                workOrderList = tempworkOrderList;
              } else {
                workOrderList = tempworkOrderList.where( (parcel) => parcel.workOrderStatusId == id).toList();
            }
            setState(() {

            });
          },
          value: parcelStatusId,
      ),
    );
  }


  Widget listWorkOrder(){
return 
        workOrderList.isEmpty ? Text('No Data To Display') :  ListView.builder(
        itemCount: workOrderList.length,
        itemBuilder: (BuildContext context,int index) {
          return GestureDetector(
            onTap: (){
              var workOrderId = workOrderList[index].workOrderStatusId;
              if (workOrderId == 1) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => newWorkOrderDetail(workOrder: workOrderList[index])),
                );                
              } else if (workOrderId == 2) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => newWorkOrderDetail(workOrder: workOrderList[index])),
                );                
              } else if (workOrderId == 3) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => newWorkOrderDetail(workOrder: workOrderList[index])),
                );
              } else if(workOrderId == 4) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => newWorkOrderDetail(workOrder: workOrderList[index])),
                );
              } else if (workOrderId == 5) {
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => WorkOrderDetailScreen(workOrder: workOrderList[index])),
                );
              } else if (workOrderId == 8){
                  Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => WorkOrderPendingScreen(workOrder: workOrderList[index])),
                );
              }
            },
            child:Card(
              child: Container(
              height: 120,
                child: Container(
                  decoration: BoxDecoration(
                      border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
                  ),
                  child: Container(
                    padding: EdgeInsets.all(5),
                    child: Row(
                      children: <Widget>[
                          ContainerImage(workOrderList[index].workOrderImages),
                          ContainerContent(context,workOrderList[index]),
                      ],
                    )
                  ),
                ),
              ),
            )          
          );
        },
      );
  }

  Widget ContainerImage(List<WorkOrderImages> url){
      return Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
           url.isEmpty ? Image.asset('assets/images/noimage.jpg',width: 100,height: 100)
            : Image.network('$baseAPi'+url[0].name,width: 100,height: 100)
        ],
      );
  }

  Widget ContainerContent(BuildContext context,WokrOrderC workOrder) {
    return  Expanded(      
      flex: 2,
      child: Container(
        padding: EdgeInsets.only(top: 10,left: 5,right: 5,bottom: 5),
        // color: Colors.red,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(
                top: 5.0
              ),
              child: RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Case Id: '),
              TextSpan(text: '${workOrder.workOrderNo}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
              //   child: Text('Case Id: ${workOrder.workOrderNo}',style: TextStyle(
              //   color:  Color.fromRGBO(139, 153, 159, 1)
              // ),),
            ),
            // Text('Work Order Item: ${workOrder.workOrderTypeName}'),
            RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Work Order Item: '),
              TextSpan(text: '${workOrder.workOrderTypeName}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
            // Text('Service Date: ${ formatter.format(DateTime.parse(workOrder.requestDate.substring(0,10)))} ${workOrder.requestTime.substring(0,5)}'),
            RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
              TextSpan(text: 'Service Date: '),
              TextSpan(text: '${ formatter.format(DateTime.parse(workOrder.requestDate.substring(0,10)))}',style: TextStyle(fontWeight: FontWeight.bold))
            ]
            ),
          ),
            Padding(
              padding: const EdgeInsets.only(
                bottom: 5.0
              ),
              // child: Text('Status: ${workOrder.workOrderStatusCode}'),
              child: RichText(
              text: TextSpan(
              style: TextStyle(
                color: Colors.black,
                fontSize: 15
            ),              
            children: <TextSpan>[
                TextSpan(text: 'Status: '),
                TextSpan(text: '${workOrder.workOrderStatusName}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ) ,
            ),
            // Spacer(),
          ],
        ),
      ),
    );    
  }

  callWorkOrderStatus() async {
    String url = '${baseAPi}papi/workorderstatus';
    Map data = {"apiRequest":{"action":"list"}};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {"Content-Type": "application/json"},
    );
    (json.decode(response.body)['data'] as List).forEach((parcelStatus){      
      workOrderStatusList.add(WorkOrderStatusModel.fromJson(parcelStatus));
    });
  }

  callWorkOrder() async {
    String url = '${baseAPi}papi/workorder';
    Map data = {"apiRequest":{"action":"list-mobile"}};
    print(token);
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
        },
    );
    print(response.statusCode);
    (json.decode(response.body)['data'] as List).forEach((parcel){
      print(parcel);
      workOrderList.add(WokrOrderC.fromJson(parcel));        
    });
  tempworkOrderList = workOrderList;
  setState(() {
    
  });    
  }
}