import 'package:chartium/src/app/screen/work_order/models-v2/work_order_c.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';

class AcknowledgeWorkOrderDetail extends StatelessWidget with BaseApi{
  WokrOrderC workOrder;
  AcknowledgeWorkOrderDetail({Key key,@required this.workOrder}):super();
  @override
  Widget build(BuildContext context) {    
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          title: Text('Room Clean ',style: TextStyle(
            color: Colors.black
          ),),
        ),
        body: content(context),
      ),
    );
  }
  Widget content(BuildContext context){
    return Container(
      width: double.infinity,
      height: 250.0,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        // color: Colors.red,
        border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
      ),
      child: ContainerContent(context),
    );
  }

  Widget ContainerContent(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Text('${workOrder.roomNo}',style: TextStyle(
              color: Colors.grey,
              fontSize: 20
            ),),
            Text('Case ID:${workOrder.workOrderNo}',style: TextStyle(
              color: Colors.grey,
              fontSize: 15
            ),),
          ],
        ),
        Container(margin: EdgeInsets.only(top: 10.0),),
        Row(
          children: <Widget>[
              Text('${workOrder.getFullname()}',style: TextStyle(
                fontSize: 18
              ),)   
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('${workOrder.workOrderTypeName}'),
                Text('Service Date: ${ formatter.format(DateTime.parse(workOrder.requestDate.substring(0,10)))} ${workOrder.requestTime.substring(0,5)}'),
                Text('Assign to : Maid Somsri'),
              ],
            ),
            ContainerImage()
          ],
        ),
        Row(
          mainAxisAlignment: MainAxisAlignment.end,
          children: <Widget>[
            Text('${workOrder.workOrderStatusName}'),
            Container(margin: EdgeInsets.only(right: 10),)
          ],
        )
      ],
    );
  }





  Widget ContainerImage(){
    // String url = 'https://scontent.fbkk8-3.fna.fbcdn.net/v/t31.0-8/26171463_1833351416715230_2166791531763826436_o.jpg?_nc_cat=111&_nc_eui2=AeGHtlihyUvmjcDWUvraVAmljxQgJ_ZPMTryIHHpQ4RPSmJra-78NLRzz3OTOtHirOIURQEwvtbswjJ3JtuPBiaOW6ajp8EQ1DmNy_Y_CwvO6w&_nc_oc=AQnLEDE-CH08SzxuhJ26N8uv5RIrBzACrS4UVXzntv41EDV5VG4Uv7AMJh86MyAUHAY&_nc_ht=scontent.fbkk8-3.fna&oh=c1cba603a922d268c794ee9561927a29&oe=5DB16172';
      return Row(
        // crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          workOrder.workOrderImages.isEmpty ? Image.asset('assets/images/noimage.jpg',width: 100,height: 100)
            : Image.network('$baseAPi'+workOrder.workOrderImages[0].name,width: 100,height: 100)
        ],
      );
  }
}