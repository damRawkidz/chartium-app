import 'dart:convert';
import 'dart:math';

import 'package:chartium/src/app/components/bubble_chat.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/screen/work_order/models-v2/work_order_c.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;

class ChatWorkOrderScreen extends StatefulWidget {
  WokrOrderC workOrder;
  ChatWorkOrderScreen({Key key,this.workOrder}):super(key:key);
  @override
  _ChatWorkOrderScreenState createState() => _ChatWorkOrderScreenState();
}

class _ChatWorkOrderScreenState extends State<ChatWorkOrderScreen>  with BaseApi{
  UserModel currentUser;  
  bool _isNewTopic;
  int topicId;
  final _messageController = TextEditingController();
  final _scrollController = ScrollController(); 
  // List<Widget> list = List.generate(10, (i) => BubbleChat(message:'${i + 1}',isSender: Random().nextBool(),));  
  List<Widget> list = [];
  @override
   initState()  { 
    // 580b12db-6264-49ab-aadb-4d864d6706ad id for RM007
    token = Provider.of<UserState>(context,listen: false).getToken();
    print(widget.workOrder.topic);
    currentUser = Provider.of<UserState>(context,listen: false).getUser();     
    
    if (widget.workOrder.topic != null) {      
      // callHttp();
      _isNewTopic =false;
      topicId = widget.workOrder.topic.id;
     _list_MessageApi();
    } else {
      _isNewTopic =true;
      print('new msg');
    }    
    super.initState();
  }

  _list_MessageApi() async {
    String url = '${baseAPi}papi/message';
    Map data = {"apiRequest":{"action":"get"},"data":[{
        "topicId":topicId
      }]};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
          "Content-Type": "application/json",
          "Authorization":"bearer "+token
        },
    );
    print(response.statusCode);
    print(response.body);
    // print('call from list message');
    (json.decode(response.body)['data'] as List).forEach((msg) => {
    // print(msg['replyById'].toString().isEmpty),
        list.add(BubbleChat(message:'${msg['message']}',isSender: msg['replyById'].toString().isNotEmpty))
    });
    setState(() {
      
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        home: Scaffold(
            resizeToAvoidBottomInset:false,        
            appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            backgroundColor: Colors.white,
            title: Text('Room Clean ',style: TextStyle(
              color: Colors.black
            ),),
          ),
          body: Column(
          children: <Widget>[
            Expanded(
                child: Column(
                  children: <Widget>[
                      // content(context),
                      iconChat(),
                      _textChat(),
                      // _contentChat()
                  ],
                ),
            )
          ],
        ),
        ),
    );
  }

  Widget iconChat(){
    return 
    // Expanded(      
    //   flex: 1,
    //   child: 
      Container(        
        padding: EdgeInsets.all(10.0),
        width: double.infinity,
        decoration: BoxDecoration(          
        border: Border(
          bottom: BorderSide(
            color: Color.fromRGBO(139, 153, 159, 1)
          ),          
        )
      ),        
        child: Row(          
          children: <Widget>[            
            Text('Chat',
              style: TextStyle(
              fontSize: 20.0
            ),),
            Container(margin: EdgeInsets.only(right: 10),),
            Icon(Icons.chat),
            Spacer(),
            // IconButton(
            //   icon: Icon(Icons.arrow_drop_down),
            //   onPressed: (){
            //     // print('object');
            //   },
            // )
          ],
        ),
      // ),
    );
  }

  Widget _textChat(){
    return Expanded(
    // flex: 12,
      child: Container(        
        child: ListView.builder(          
          controller: _scrollController,
          itemCount: list.length,        
          itemBuilder: (BuildContext context, int index) {
            return list[index];
          },
          
        ),
      ),      
    );
  }

  Widget _contentChat(){
        return 
        // Expanded(
        // flex: 1,
        // child: 
        TextFormField(        
        controller: _messageController,
        keyboardType: TextInputType.text,
        decoration: InputDecoration(
          prefixIcon: Icon(Icons.camera_alt),          
          suffixIcon: IconButton(
            icon: Icon(Icons.send),
            onPressed: (){
              setState(()  {
                  var value =_messageController.text;
                  if (value != '') {
                    list.add(BubbleChat(isSender: true, message: value,));
                    _scrollController.animateTo(
                    _scrollController.position.maxScrollExtent + 20,
                    duration: const Duration(milliseconds: 200)
                    , curve: Curves.easeOut);

                    if (_isNewTopic) {
                    sendNewMessage();  
                    print('new msg');
                  } else {
                    print('old msg');
                    interactMessage();
                  }
                  _messageController.clear();
                  }
              });
            },
          ),
          hintText: 'Message Here',
      )
        );
  }

  interactMessage() async {
    String url = '${baseAPi}papi/message';
    Map data = {"apiRequest":{"action":"new"},"data":[{
        "topicId":topicId,
        "message": _messageController.text,
      }]};
    print(data);
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {
            "Content-Type": "application/json",
            "Authorization":"bearer "+token
        },
    );
    print(response.statusCode);
    print(response.body);
    print('interact success');
    // await _list_MessageApi();
  }

  sendNewMessage() async {
    String url = '${baseAPi}papi/message';
    Map data = {"apiRequest":{"action":"new-mobile"},"data":[{
        "message": _messageController.text,
        // "roomId":'580b12db-6264-49ab-aadb-4d864d6706ad',
        "workOrderId":widget.workOrder.id
      }]};
    var body = json.encode(data);
    // print(body);
    var response = await http.post(url,
      body: body,
      headers: {
          "Content-Type": "application/json",
          "Authorization":"bearer "+token
        },
    );
    // print(response.statusCode);
    print('new msg');
    print(response.body);
    topicId =json.decode(response.body)['data'][0]['topicId'];
    _isNewTopic = false;
    setState(() {
    });
  }
}