import 'dart:convert';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/screen/work_order/models-v2/work_order_c.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/work_order_screen.dart';
import 'package:http/http.dart' as http;
import 'package:chartium/src/app/providers/work_order_provider.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
class CreateWorkOrder extends StatefulWidget  {
  @override
  _CreateWorkOrderState createState() => _CreateWorkOrderState();
}

class _CreateWorkOrderState extends State<CreateWorkOrder> with BaseApi {
  UserModel currentUser;
  List<WokrOrderC> list = [];
    
   @override
  void initState() {
    // currentUser = Provider.of<UserState>(context,listen: false).getUser();
    token = Provider.of<UserState>(context,listen: false).getToken();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {    
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset:false,        
            appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: (){
                // print('object');
                // Navigator.of(context).pop();
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => WorkOrderScreen()),
                  );
              },
            ),
            actions: <Widget>[
              IconButton(
                icon: Icon(Icons.add),
                color: Colors.black,
                onPressed: (){
                Navigator.pushReplacement(context,
                    MaterialPageRoute(builder: (context) => WorkOrderScreen()),
                  );
                },
              )              
            ],
            backgroundColor: Colors.white,
            centerTitle: true,
            title: Text('Work Cart',style: TextStyle(
              color: Colors.black
            ),),
          ),
        body: 
        Container(          
          height: double.infinity,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
               _content(context),
              submitButton(context),
            ],
          ),
        )               
      ),
    );
  }

  Widget _content(BuildContext context){
    list = Provider.of<WorkOrderProvider>(context,listen: false).getWorkOrderList();
    return Expanded(            
        child: Container(
        child: ListView.builder(      
          itemCount: list.length,
          shrinkWrap:true,
          itemBuilder: (BuildContext context,int index) {
            // return Text('data');
            return GestureDetector(
              child:Card(
                child: Container(
                height: 130,
                  child: Container(
                    decoration: BoxDecoration(
                        border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
                    ),
                    child: Container(
                      padding: EdgeInsets.all(5),
                      child: ContainerContent(context,list[index],index),                           
                    ),
                ),
              )        ,
              )
            );
          },
        ),
      ),
    );
  } 

  Widget ContainerContent(BuildContext context,WokrOrderC workOrder,int index) {
    return  
    // Expanded(      
    //   flex: 2,
    //   child: 
      Container(
        padding: EdgeInsets.only(top: 10,left: 5,right: 5,bottom: 5),
        // color: Colors.red,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[                  
                  // Text('${workOrder.workOrderTypeName}',style: TextStyle(
                  //   color:  Color.fromRGBO(139, 153, 159, 1)
                  // ),),
                  Text('NEW',style: TextStyle(
                    color:  Color.fromRGBO(139, 153, 159, 1)
                  ),),
                  // Text('${workOrder.roomNo}',style: TextStyle(
                  //   fontWeight: FontWeight.bold,
                  //   color:  Colors.black
                  // ),),
                ],
              ),              
              Text('${workOrder.remark}',style: TextStyle(
                // fontSize: 20
              ),),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Service Date: ${workOrder.requestDate} ${workOrder.requestTime}',style: TextStyle(
                    color:  Color.fromRGBO(139, 153, 159, 1)
                  ),),
                  IconButton(
                    icon: Icon(Icons.delete),
                    onPressed: ()  {
                      list.removeAt(index);
                      // if(list.isEmpty){
                      //   Navigator.push(context,
                      //       MaterialPageRoute(builder: (context) => WorkOrderScreen(nameRoom: null,)),
                      //   );
                      // }
                      setState(() {
                        
                      });
                      },
                  )
                ],
              )
          ],
        ),
      // ),
    );    
  }

  Widget submitButton(BuildContext context){
    return Container(
        padding: EdgeInsets.all(10.0),
        child: SizedBox(
          width: double.infinity,
          child: RaisedButton(
            color: Colors.deepOrange,
            child: Text('Submit',style: TextStyle(
              color: Colors.white
            ),),
            onPressed: (){
              _saveWorkOrder(context);
            },
        ),
      ),
    );
  }

  Future _saveWorkOrder(BuildContext context) async {
   String url = '${baseAPi}papi/workorder';
    var request = json.encode({"apiRequest":{"action":"new-mobile"},
      "data": list.map((workOrder) => {
            "remark":workOrder.remark,
            "workOrderItemId": workOrder.workOrderItemId,            
            "requestDate": workOrder.requestDate,
            "requestTime": workOrder.requestTime.toString().split(' ')[0],
            "workOrderImages":workOrder.workOrderImages.map((image) => {              
              "name":image.name
            }).toList()
      }).toList()
    });
    print(request);
    var response = await http.post(url,
      body: request,
      headers: {
          "Content-Type": "application/json",
          "Authorization":"bearer "+token
        },
    );
    print(response.statusCode);
    Provider.of<WorkOrderProvider>(context,listen: false).clearState();
    // final snackBar = SnackBar(
    //   content: Text('${json.decode(response.body)['apiResponse']['desc']}')     
    // );
    // Scaffold.of(context).showSnackBar(snackBar);
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(builder: (context) => WorkOrderScreen()),
    );
  }
}



