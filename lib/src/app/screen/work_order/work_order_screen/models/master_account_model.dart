class MasterAccount {
    String emailId;
    String prefixId;
    String prefixCode;
    String prefixName;
    String name;
    String surName;
    String middleName;
    String password;
    String renterId;
    String roomNo;
    bool status;
    String createDate;
    String lastUpdateDate;
    String id;

    MasterAccount.fromJson(Map<String,dynamic> json):
    emailId = json['emailId'],
    prefixId  =json['prefixId'],
    prefixCode =json['prefixCode'],
    prefixName =json['prefixName'],
    name =json['name'],
    surName = json['surName'],
    middleName = json['middleName'],
    password = json['password'],
    renterId =json['renterId'],
    roomNo =json['roomNo'],
    status =json['status'],
    createDate = json['createDate'],
    lastUpdateDate = json['lastUpdateDate'],
    id = json['id'];
}