import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/models/master_account_model.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/models/residents_model.dart';

class RoomModel {
    String roomNo;
    String arrivalDate;
    String departureDate;
    String prefixId;
    String prefixCode;
    String prefixName;
    String firstName;
    String surName;
    String middleName;
    String nationalityId;
    String nationalityCode;
    String nationality;
    String passport;
    String dateOfBirth;
    String address1;
    String address2;
    bool status;
    String createDate;
    String lastUpdateDate;
    List<UserModel> masterAccountsList;
    List<Resident> renterResidentList;
    String id;

    RoomModel.fromJson(Map<String,dynamic> json):
    roomNo = json['roomNo'],
    arrivalDate =json['arrivalDate'],
    prefixId =json['prefixId'],
    prefixCode =json['prefixCode'],
    prefixName =json['prefixName'],
    firstName =json['firstName'],
    surName =json['surName'],
    middleName =json['middleName'],
    nationalityId =json['nationalityId'],
    nationalityCode =json['nationalityCode'],
    nationality =json['nationality'],
    passport =json['passport'],
    dateOfBirth =json['dateOfBirth'],
    address1 =json['address1'],
    address2 =json['address2'],
    status =json['status'],
    createDate =json['createDate'],
    lastUpdateDate =json['lastUpdateDate'];
    // masterAccountsList = (json['masterAccountsList'] as List).map((i) => UserModel.formJson(i)).toList();

}