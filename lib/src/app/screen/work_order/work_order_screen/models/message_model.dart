class MessageModel {
  int topicId;
  String message;
  String accountId;
  String roomId;
  int workOrderId;
  int id;

  MessageModel(
      {this.topicId,
      this.message,
      this.accountId,
      this.roomId,
      this.workOrderId,
      this.id});

  MessageModel.fromJson(Map<String, dynamic> json) {
    topicId = json['topicId'];
    message = json['message'];
    accountId = json['accountId'];
    roomId = json['roomId'];
    workOrderId = json['workOrderId'];
    id = json['id'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['topicId'] = this.topicId;
    data['message'] = this.message;
    data['accountId'] = this.accountId;
    data['roomId'] = this.roomId;
    data['workOrderId'] = this.workOrderId;
    data['id'] = this.id;
    return data;
  }
}