class WorkOrderStatusModel {
  String code;
  String name;
  int id;
  int rowStatus;

  WorkOrderStatusModel({this.code, this.name, this.id, this.rowStatus});

  WorkOrderStatusModel.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
    id = json['id'];
    rowStatus = json['rowStatus'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['name'] = this.name;
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    return data;
  }
}