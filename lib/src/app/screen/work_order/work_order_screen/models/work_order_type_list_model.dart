class WorkOrderTyoeListModel {   
  String caseId;
  String trackId;
  String roomNo;
  String workOrderTypeId;
  String renterId;
  String renter;
  String workOrderTypeName;
  String serviceDate;
  String serviceTime;
  String workStatus;
  bool status;
  String imageUrl;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;
  String id;

   WorkOrderTyoeListModel.fromJson(Map<String,dynamic> json){
     imageUrl =json['imageUrl'] == null ? '' : json['imageUrl'];
     caseId = json['caseId'];
     trackId = json['trackId'];
     roomNo =json['roomNo'];
     workOrderTypeId =json['workOrderTypeId'];
     renterId =json['renterId'];
     renter =json['renter'];
     workOrderTypeName =json['workOrderTypeName'];
     serviceDate =json['serviceDate'];
     serviceTime =json['serviceTime'];
     workStatus =json['workStatus'];
     status =json['status'];
     createBy =json['createBy'];
     createDate  =json['createDate'];
     lastUpdateBy =json['lastUpdateBy'];
     lastUpdateDate =json['lastUpdateDate'];
     id =json['id'];
   }
}