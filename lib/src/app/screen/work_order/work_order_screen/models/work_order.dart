class WorkOrder {
  String roomNumber;
  String workOrderTypeName;
  String renterRoomProfileId;
  String renterProfileId;
  String maidName;
  List<WorkOrderImages> workOrderImagesModel;
  List<WorkOrderTracking> workOrderTrackingModel;  
  
  WorkOrder.fromJson(Map<String,dynamic> json):
  renterProfileId =json['renterProfileId'],
  renterRoomProfileId =json['renterRoomProfileId'],
  roomNumber =json['roomNumber'],
  workOrderTypeName =json['workOrderTypeName'],
  maidName =json['maidName'],
  workOrderImagesModel = (json['workOrderImagesModel'] as List).map((i) => WorkOrderImages.fromJson(i)).toList(),
  workOrderTrackingModel = (json['workOrderTrackingModel'] as List).map((f) => WorkOrderTracking.fromJson(f)).toList();
}
class WorkOrderTracking {
    String workOrderTypeId;
    String description;
    String serviceDate;
    String serviceTime;
    
    WorkOrderTracking.fromJson(Map<String,dynamic> json):
    workOrderTypeId = json['workOrderTypeId'],
    description =json['description'],
    serviceDate =json['serviceDate'],
    serviceTime =json['serviceTime'];
}

class WorkOrderImages {  
    String workOrderId;
    String imageUrl;
    String imageName;
    String imageDescription;
    bool status;
    String createBy;
    String createDate;
    String lastUpdateBy;
    String lastUpdateDate;
    String id;

    WorkOrderImages.fromJson(Map<String,dynamic> json):
    imageUrl =json['imageUrl'],
    imageName =json['imageName'];
}
