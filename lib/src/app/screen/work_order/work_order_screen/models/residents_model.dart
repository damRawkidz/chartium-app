class Resident {
  String prefixId;
  String prefixCode;
  String prefixName;
  String name;
  String surName;
  String middleName;
  String renterId;
  String roomNo;
  bool status;
  String createDate;
  String lastUpdateDate;
  String id;
  String fullName;

  Resident.fromJson(Map<String,dynamic> json):
  prefixId =json['prefixId'],
  prefixCode =json['prefixCode'],
  prefixName =json['prefixName'],
  name =json['name'],
  surName =json['surName'],
  middleName =json['middleName'],
  renterId =json['renterId'],
  roomNo =json['roomNo'],
  status =json['status'],
  createDate =json['createDate'],
  lastUpdateDate =json['lastUpdateDate'],
  id =json['id'],
  fullName = '${json['prefixName']} ${json['name']} ${json['middleName']} ${json['surName']}';
}