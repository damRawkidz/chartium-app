class WorkOrderTypeModel {
  String code;
  String name;
  bool status;
  String createDate;
  String lastUpdateDate;
  String id;

  WorkOrderTypeModel.fromJson(Map<String,dynamic> json){
    code =json['code'];
    name =json['name'];
    status =json['status'];
    createDate =json['createDate'];
    lastUpdateDate =json['lastUpdateDate'];
    id =json['id'];    
  }
}
