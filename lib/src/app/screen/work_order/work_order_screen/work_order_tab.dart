import 'package:chartium/src/app/screen/work_order/work_order_screen/work_order_acknowledge_screen.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/work_order_finish_screen.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/work_order_news_screen.dart';


import 'package:chartium/src/app/screen/work_order/work_order_screen/work_order_progress_screen.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/work_order_screen.dart';
import 'package:flutter/material.dart';



class workOrderScreen extends  StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _workOrderScreenState();
  }

}
class _workOrderScreenState extends State<workOrderScreen> {
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
      length: 4,
      child: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.white,
          title: Text(
                'Work Order',
                style: TextStyle(color: Colors.black),
          ),
          centerTitle: true,
          actions: <Widget>[ 
                IconButton(
                  icon: Icon(Icons.add),
                  tooltip: 'Air it',
                  color: Colors.black,
                  onPressed: (){
                      Navigator.pushReplacement(context,
                        MaterialPageRoute(builder: (context) => WorkOrderScreen()),
                      );
                  },
                ),
              ],    
          bottom: TabBar(
            labelColor: Colors.black,
            indicatorColor: Color.fromRGBO(180, 151, 96, 1),
            unselectedLabelColor: Color.fromRGBO(180, 151, 96, 1),            
            tabs: <Widget>[
              Tab(text: 'New',),
              Tab(text: 'Acknowledge',),
              Tab(text: 'Progress',),
              Tab(text: 'Finish',)
            ],
          ),
        ),
        body: TabBarView(children: <Widget>[
          workOrderNews(),
          workOrderAcknowledge(),
          workOrderProgress(),
          workOrderFinish()
        ],
        ),
      ),
    );
  }
  
}