import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/screen/work_order/models-v2/work_order_c.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'dart:convert';
import 'work_order_detail_screen.dart';
import 'models/work_order_type_list_model.dart';
class workOrderProgress extends  StatefulWidget {
  @override
  _workOrderProgressState createState() => _workOrderProgressState();
}

class _workOrderProgressState extends State<workOrderProgress>  with BaseApi{
  List<WokrOrderC> list = [];
  // UserModel currentUser;
  int statusId = 3;
  @override
  Widget build(BuildContext context) {
    return content(context);
  }

  @override
  void initState() { 
    super.initState();
    // currentUser = Provider.of<UserState>(context,listen: false).getUser();    
    token = Provider.of<UserState>(context,listen: false).getToken();
    callHttp();  
  }

  callHttp() async {
    String url = '${baseAPi}papi/workorder';
    Map data = {"apiRequest":{"action":"list-mobile"}};
    var body = json.encode(data);
    print(body);
    var response = await http.post(url,
      body: body,
      headers: {
          "Content-Type": "application/json",
          "Authorization":"bearer "+token
        },
    );
    print((json.decode(response.body)['data'] as List).length);
    print(response.body);
    (json.decode(response.body)['data'] as List).forEach((workOrderType){      
      if (workOrderType['workOrderStatusId'] == this.statusId) {
        list.add(WokrOrderC.fromJson(workOrderType));  
      }      
    });
    
    setState(() {
    });
  }

  Widget content(BuildContext context){
  //  List<int> list = List.generate(10000, (i) => i+1);
    return list.isEmpty ? Text('No Data To Display') : ListView.builder(
      itemCount: list.length,
      itemBuilder: (BuildContext context,int index) {
        return GestureDetector(
          onTap: (){
          Navigator.push(context,
                MaterialPageRoute(builder: (context) => WorkOrderDetailScreen( workOrder: list[index])),
          );
          },
          child:Card(
            child: Container(
            height: 120,
              child: Container(
                decoration: BoxDecoration(
                    border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
                ),
                child: Container(
                  padding: EdgeInsets.all(5),
                  child: Row(
                    children: <Widget>[
                        ContainerImage(list[index].workOrderImages),
                        ContainerContent(context,list[index]),
                    ],
                  )
                ),
            ),
          )        ,
          )
        );
      },
    );
  } 

  Widget ContainerImage(List<WorkOrderImages> url){
      return Row(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
           url.isEmpty ? Image.asset('assets/images/noimage.jpg',width: 100,height: 100)
            : Image.network('$baseAPi'+url[0].name,width: 100,height: 100)
        ],
      );
  }

  Widget ContainerContent(BuildContext context,WokrOrderC workOrder) {
    return  Expanded(      
      flex: 2,
      child: Container(
        padding: EdgeInsets.only(top: 10,left: 5,right: 5,bottom: 5),
        // color: Colors.red,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('Case Id ${workOrder.workOrderNo}',style: TextStyle(
                    color:  Color.fromRGBO(139, 153, 159, 1)
                  ),),
                  Text('${workOrder.roomNo}',style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color:  Colors.black
                  ),),
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text('${workOrder.workOrderTypeName}'),
                  // Text('status'),
                ],
              ),
              Text('${workOrder.getFullname()}'),
              Text('Service Date: ${ formatter.format(DateTime.parse(workOrder.requestDate.substring(0,10)))} ${workOrder.requestTime.substring(0,5)}'),
              // Text('${workOrder.workOrderStatusName}',style: TextStyle(
              //   color:  Color.fromRGBO(139, 153, 159, 1)
              // ),),
          ],
        ),
      ),
    );    
  }
}