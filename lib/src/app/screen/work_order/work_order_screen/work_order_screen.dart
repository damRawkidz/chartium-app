import 'dart:io';

import 'package:chartium/src/app/components/chartium_progress_bar.dart';
import 'package:chartium/src/app/providers/user_provider.dart';

import 'package:chartium/src/app/screen/main/main_screen.dart';
import 'package:chartium/src/app/screen/work_order/models-v2/work_order_item_c.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'models/image_model.dart';

class WorkOrderScreen extends StatefulWidget {
  
@override
  _AddWorkOrderScreenState createState() => _AddWorkOrderScreenState();
  
}

class _AddWorkOrderScreenState extends State<WorkOrderScreen> with BaseApi {
  bool isNow = false;
  final List<WorkOrderItem> workOrderTypes = [];
  final desriptionWorkCOntroller = TextEditingController();
  final formkey = GlobalKey<FormState>();
  bool isShowProgress = false;
  String name;
  String selectDate ;
  String selectTime ;
  int workOrderTypeId = null;
  double heightboxPicture = 100;
  double widthboxPicture = 100;
  File imageFile;
  File imageFile2;
  File imageFile3;
  List<ImageModel> uploadedImages =[];
  String  displaySelectTiem;
  @override
  void initState() { 
    callHttp();
    
    token = Provider.of<UserState>(context,listen: false).getToken();
    name = Provider.of<UserState>(context,listen: false).getName();
    super.initState();    
  }

  callHttp() async {
    String url = '${baseAPi}papi/workorderitem';
    Map data = {"apiRequest":{"action":"list"}};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {"Content-Type": "application/json"},
    );
    print(response.body);
    (json.decode(response.body)['data'] as List).forEach((workOrderType){      
      workOrderTypes.add(WorkOrderItem.fromJson(workOrderType));
    });


    setState(() {
    });
  }
  
  @override
  Widget build(BuildContext context) {    
    return MaterialApp(      
      home: Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pushReplacement(context,
                MaterialPageRoute(builder: (context) => MainScreen(page: 0)),
              );
            },
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text('New Work Order',style: TextStyle(
            color: Colors.black
          ),),
        ),
        body: isShowProgress ? ChartiumProgressBar() : Content(context),
      ),
    );  
    
  }

  void _showTimePicker(){
      Future<DateTime> selectedDate = showTimePicker(
      context: context, 
      initialTime: TimeOfDay.now(),
      // currentTime: DateTime.now(), 
      // locale: LocaleType.en
  ).then((pickedTime) {
    
    if (pickedTime ==null) {
      return;
    } else {
      setState(() {
        String time = pickedTime.replacing(hour: pickedTime.hour).toString();
        int indexOfSemicolon = time.indexOf(":");
        String convertTime =time.substring(indexOfSemicolon - 2,indexOfSemicolon + 3);
        // pickedTime.period == DayPeriod.pm ? period = "PM" : period = "AM" ;
        selectTime = convertTime;
        print(selectTime);  
      });      
    }
  });
  }

  void _showDatePicker(){
    Future<DateTime> selectedDate = showDatePicker(
      context: context,
      selectableDayPredicate: (DateTime val) =>
            val.day >= DateTime.now().day ? true :false,
      initialDate: DateTime.now(),
      firstDate: DateTime(2018),
      lastDate: DateTime(3000),
  ).then((pickedDate) {
    if (pickedDate ==null) {
      return;
    } else {
      setState(() {        
        var formatter = DateFormat("yyyy-MM-dd");      
        selectDate = formatter.format(pickedDate);
        print(selectDate);
      });
    }
  });
  }

  Future<void> _takePicture() async{
  bool mode = true;
  showDialog(
    context: context,
    builder: (context) {
      return SimpleDialog(
        // title: Text('data'),
        children: <Widget>[
          SimpleDialogOption(
            onPressed: () async {
                mode = true;
                 imageFile = await ImagePicker.pickImage(
                  source:ImageSource.camera,
                  maxWidth: 600,  
                );
                setState(() {
                  
                });
                Navigator.of(context, rootNavigator: true).pop();
              },
            child:  const Text('CAMERA'),
          ),
          SimpleDialogOption(
            onPressed: () async { 
                imageFile = await ImagePicker.pickImage(
                source :ImageSource.gallery,
                maxWidth: 600,  
              );
              setState(() {
                  
              });
              Navigator.of(context, rootNavigator: true).pop();
              },
            child:  const Text('GALLERY'),
          )
        ],
      );
    }
  );
  }

  Future<void> _takePicture2() async{
  bool mode = true;
  showDialog(
    context: context,
    builder: (context) {
      return SimpleDialog(
        children: <Widget>[
          SimpleDialogOption(
            onPressed: () async {
                mode = true;
                 imageFile2 = await ImagePicker.pickImage(
                source:ImageSource.camera,
                maxWidth: 600,  
                );
                setState(() {
                  
                });
                Navigator.of(context, rootNavigator: true).pop();
              },
            child:  const Text('CAMERA'),
          ),
          SimpleDialogOption(
            onPressed: () async { 
                imageFile2 = await ImagePicker.pickImage(
                source :ImageSource.gallery,
                maxWidth: 600,  
              );
              setState(() {
                  
                });
                Navigator.of(context, rootNavigator: true).pop();
              },
            child:  const Text('GALLERY'),            
          )
        ],
      );
    }
  );
  
  }

  Future<void> _takePicture3() async{
  bool mode = true;
  showDialog(
    context: context,
    builder: (context) {
      return SimpleDialog(
        children: <Widget>[
          SimpleDialogOption(
            onPressed: () async {
                mode = true;
                 imageFile3 = await ImagePicker.pickImage(
                source:ImageSource.camera,
                maxWidth: 600,  
                );
                setState(() {
                  
                });
                Navigator.of(context, rootNavigator: true).pop();
              // _dismissDialog();
              },
            child:  const Text('CAMERA'),
          ),
          SimpleDialogOption(
            onPressed: () async { 
                imageFile3 = await ImagePicker.pickImage(
                source :ImageSource.gallery,
                maxWidth: 600,  
              );
              setState(() {
                  
                });
                Navigator.of(context, rootNavigator: true).pop();
              },
            child:  const Text('GALLERY'),
          )
        ],
      );
    }
  );
  
  }

  Widget Content(BuildContext context){
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.all(20.0),
      // color: Colors.red,
      child: SingleChildScrollView(
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          _profile(),
          _workOrder(),      
        ],
        ),
      )
    );
  }

  Widget _workOrder(){
    return Container(
      padding: EdgeInsets.only(top: 20),
      child: Form(
          key: formkey,
          child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[          
            Divider(),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text('Work Order Type'),
                dropDownWorkOrderType(),
                textArea(),
                Container(margin: EdgeInsets.only(top: 10)),              
                now(),
                Container(margin: EdgeInsets.only(top: 10)),
                isNow ? Container() : Text('Service Date'),
                datePicker(),
                Container(margin: EdgeInsets.only(top: 10)),
                isNow ? Container() : Text('Service Time'),
                _timePicker(),
                _addImages(),
                Container(margin: EdgeInsets.only(top: 10)),
                submitButton()
              ],
            )
          ],        
        ),
      ),
    );
  }

  Widget now(){
    return Row(
      children: <Widget>[
        Text('Service Now'),
        Checkbox(
          onChanged: (bool value) => setState(() {
              isNow = value;
              print(isNow);
              if (isNow) {
                  // selectTime =DateTime.now().toIso8601String().split("T")[1].substring(0,5);
                  // print(DateTime.now().toIso8601String());
                  // print(DateTime.now().toIso8601String().split("T")[0].substring(0,10));
                  // selectDate = DateTime.now().toIso8601String().split("T")[0].substring(0,5);
              } else {
                selectTime = 'Select Service Date';
                selectDate = 'Select Service Time';
              }
          }), 
          value: isNow,
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,                    
        ),
      ],
    );
  }

  Widget _addImages(){
    return Container(
      padding: EdgeInsets.only(top: 10),
      child: Row(      
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[        
          GestureDetector(
            onTap: _takePicture,
            child: Container(
              width: widthboxPicture,
              height: heightboxPicture,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey)
              ),            
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  imageFile == null ? 
                  IconButton(
                    icon: Icon(Icons.camera_alt),
                    onPressed: _takePicture,
                  ) :
                  Image.file(imageFile,fit: BoxFit.fill,),
                  Text('Add Photo')
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: _takePicture2,
            child: Container(
              width: widthboxPicture,
              height: heightboxPicture,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey)
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  imageFile2 == null ? 
                  IconButton(
                    icon: Icon(Icons.camera_alt),
                    onPressed: _takePicture2,
                  ) :
                  Image.file(imageFile2,fit: BoxFit.fill,),
                  Text('Add Photo')
                ],
              ),
            ),
          ),
          GestureDetector(
            onTap: _takePicture3,
            child: Container(
              width: widthboxPicture,
              height: heightboxPicture,
              decoration: BoxDecoration(
                border: Border.all(color: Colors.grey)
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  imageFile3 == null ? 
                  IconButton(
                    icon: Icon(Icons.camera_alt),
                    onPressed:_takePicture3,
                  ) :
                  Image.file(imageFile3,fit: BoxFit.fill,),
                  Text('Add Photo')
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget datePicker(){
    return !isNow ? InkWell(
      onTap: (){
          _showDatePicker();
      } ,
      child: IgnorePointer(
        
        child: TextFormField(        
          decoration: InputDecoration(
            hintText: selectDate == null ? 'Select Service Date': '${selectDate.toString()}',
            suffixIcon: Icon(Icons.calendar_today),
            border: OutlineInputBorder()
          ),
          validator: (String date){
            if (selectDate == null ) {
              return 'Please select Service date';
            }

            return null;
          },
        ),
      ),
    ):Container();
  }
  
  Widget _timePicker(){
    return !isNow ?  InkWell(
      onTap: (){
          _showTimePicker();
      },
      child: IgnorePointer(
        child: TextFormField(
          decoration: InputDecoration(
            suffixIcon: Icon(Icons.watch_later),
            hintText: selectTime == null ? 'Select Service Time' : selectTime,
            border: OutlineInputBorder()
          ),
          validator: (String time){
            print(time);
            if (selectTime ==null) {
              return 'Please select Service Time';
            }

            return null;
          },
        ),
      ),
    ) :Container();
  }

  Widget submitButton(){
    return Container(
      child: SizedBox(
      width: double.infinity,
      child: RaisedButton(
      color: Colors.deepOrange,
      child: Text('Add',style: TextStyle(color: Colors.white)),
      onPressed: (){
        if (formkey.currentState.validate()) {
            _saveWorkorder();
        } else {
          showDialog(
            context: context,
            barrierDismissible: true,
            builder: (BuildContext context){
              return AlertDialog(
                // title: Text('Can not log in!'),
                content: Text('Please finish required form'),
            );
          }
      );
        }
        
      },
      ),
    ),
    );
    
  }

  Widget textArea(){
    return Container(
      margin: EdgeInsets.only(top: 10),            
      child: TextFormField(
        controller: desriptionWorkCOntroller,
        keyboardType: TextInputType.multiline,
        decoration: InputDecoration(
          hintText: 'Description',
          border: OutlineInputBorder()
        ),
        validator: (String text){
          if (text.isEmpty) {
            return 'Plase enter description';
          }

          return null;
        },
        maxLines: 3,
        
      ),
      
    );
  }

  Widget dropDownWorkOrderType(){
    return  Container(
        width: double.infinity,
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(              
        borderRadius: BorderRadius.circular(5.0),
        border: Border.all(
          color: Colors.grey, style: BorderStyle.solid, width: 0.80),
        ),
        child: DropdownButton(
          isExpanded: true,
          hint: Text('Select Work Order Type'),
          items: workOrderTypes.map<DropdownMenuItem<int>>((value) {
            return DropdownMenuItem<int>(
              value: value.id,
              child: Text('${value?.name}'),
            );
          }).toList(),
          onChanged: (id) {
            print(workOrderTypeId);
            setState(() {
              workOrderTypeId = id;              
            });
          },
          value: workOrderTypeId,
      ),
    );
  }



  

  Widget _profile(){
    return Container(
      child: Column(
        children: <Widget>[
        Container(
        height: 50.0,
        width: double.infinity,
        decoration: BoxDecoration(
          border: Border(
            left: BorderSide(
              color: Color.fromRGBO(139, 153, 159, 1)
            ),
            right: BorderSide(
              color: Color.fromRGBO(139, 153, 159, 1)
            ),
            bottom: BorderSide(
              color: Color.fromRGBO(139, 153, 159, 1)
            ),
            top: BorderSide(
              color: Color.fromRGBO(139, 153, 159, 1)
            )
          )
          ),
            child: Container(
              padding: EdgeInsets.all(10.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  RichText(
                  text: TextSpan(
                    style: TextStyle(
                      color: Colors.black,
                      fontSize: 15
                  ),              
                  children: <TextSpan>[
                    TextSpan(text: 'Account Name: '),
                    TextSpan(text: '${name}',style: TextStyle(fontWeight: FontWeight.bold))
                 ]
                ),
              ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }


  Future _saveWorkorder() async {
    setState(() {
       isShowProgress =true;
    });
    await uploadImage();
    String url = '${baseAPi}papi/workorder';
    Map data = {"apiRequest":{"action":"new-mobile"},"data":[
          {
        "remark": desriptionWorkCOntroller.text,
        "workOrderItemId": workOrderTypeId,
        "requestDate": isNow ? DateTime.now().toIso8601String().split("T")[0].substring(0,10)  : selectDate,
        "requestTime": isNow ? DateTime.now().toIso8601String().split("T")[1].substring(0,5)  : selectTime,
        "scheduleDate": "",
        "scheduleTime": "",
        "serviceDate": "",
        "serviceTime": "",
        "workOrderImages":uploadedImages.map((image) => {              
          "name":image.path
        }).toList()
      }
      ]};      
    var body = json.encode(data);
    print(body);
    var response = await http.post(url,
      headers: {
          "Content-Type": "application/json",
          "Authorization":"bearer "+token
        },
      body: body,
    );
    setState(() {
      isShowProgress =false;
    });
    if (response.statusCode == 200) {
      showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(            
            content: Text('Success'),
          );
        }
      );
      Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (context) => MainScreen(page: 1,)),
    );
    } else {
      showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(            
            content: Text('Can not Save Work Order'),
          );
        }
      );
    }
  }

  uploadImage() async {
    var req =  http.MultipartRequest("POST",Uri.parse('${baseAPi}papi/upload/workOrderImage'));
    if(imageFile != null){
      var pic1 = await http.MultipartFile.fromPath("from_mobile", imageFile.path);
      req.files.add(pic1);
    }
    if(imageFile2 != null){
      var pic2 = await http.MultipartFile.fromPath("from_mobile", imageFile2.path);
      req.files.add(pic2);
    } 
    if (imageFile3 != null) {
        var pic3 = await http.MultipartFile.fromPath("from_mobile", imageFile3.path);
        req.files.add(pic3);
    }       
    var response = await req.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);
    // print(responseString);
    (json.decode(responseString)['data'] as List).forEach( (f){
        uploadedImages.add(ImageModel.fromJson(f));
    });
    print(uploadedImages.length);
  }
}
