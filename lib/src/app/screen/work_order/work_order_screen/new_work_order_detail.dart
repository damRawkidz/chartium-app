import 'package:chartium/src/app/screen/work_order/models-v2/work_order_c.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';

class newWorkOrderDetail extends StatelessWidget with BaseApi{
  WokrOrderC workOrder;
  newWorkOrderDetail({Key key,@required this.workOrder}):super();
  @override
  Widget build(BuildContext context) {    
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text('${workOrder.workOrderStatusName} Detail',style: TextStyle(
            color: Colors.black
          ),),
        ),
        body: content(context),
      ),
    );
  }
  Widget content(BuildContext context){
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        // color: Colors.red,
        border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
      ),
      child: ContainerContent(context),
    );
  }

  Widget ContainerContent(BuildContext context) {
    return SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Case ID: '),
                TextSpan(text: '${workOrder.workOrderNo}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Room No.: '),
                TextSpan(text: '${workOrder.roomNo}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Work Order Item: '),
                TextSpan(text: '${workOrder.workOrderItemName}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Service Date: '),
                TextSpan(text: '${ formatter.format(DateTime.parse(workOrder.requestDate.substring(0,10)))} ${workOrder.requestTime.substring(0,5)}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ),          
           ( workOrder.workOrderStatusId == 3 && workOrder.scheduleDate != null ) ? RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Schedule Date: '),
                TextSpan(text: '${ formatter.format(DateTime.parse(workOrder.scheduleDate.substring(0,10)))} ${workOrder.scheduleTime.substring(0,5)}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ) :Container(),
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Description: '),                
              ]
            ),
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: '${workOrder.remark}'),
              ]
            ),
          ),                    
           ( workOrder.workOrderStatusId != 1 && workOrder.assignToId != null ) ?  RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Service by: '),
                TextSpan(text: '${workOrder.getFullNameAssiagnBy()}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ) : Container(),
          Container(margin: EdgeInsets.only(bottom: 10.0),), 
          ContainerImage()
        ],
      ),
    );
  }





  Widget ContainerImage(){
      return Center( 
        child: Container(
          width: double.infinity,
          child: workOrder.workOrderImages.isEmpty ? Image.asset('assets/images/noimage.jpg',
                height: 300.0,
              ): 
              CarouselSlider(
              enableInfiniteScroll: false,
              // height:MediaQuery.of(context).size.height,
              height: 400,
              items: workOrder.workOrderImages.map((i) {
                return Builder(
              builder: (BuildContext context) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    child: Image.network('$baseAPi'+i.name,
                              height: 300.0,
                              alignment: Alignment.center,
                            ),
                  );
                },
              );
            }).toList(),
          )
        ),
      );
  }
}