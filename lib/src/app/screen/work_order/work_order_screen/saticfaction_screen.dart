import 'package:flutter/material.dart';

class SaticfactionScreen extends StatefulWidget {
  @override
  _SaticfactionScreenState createState() => _SaticfactionScreenState();
}

class _SaticfactionScreenState extends State<SaticfactionScreen> {
  @override
  Widget build(BuildContext context) {    
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
            leading: IconButton(
              icon: Icon(Icons.arrow_back),
              color: Colors.black,
              onPressed: (){
                Navigator.pop(context);
              },
            ),
            backgroundColor: Colors.white,
            title: Text('Room Clean ',style: TextStyle(
              color: Colors.black
            ),),
          ),
          body: _content(),
      ),
    );
  }
  Widget _content (){
    return Container(
      padding: EdgeInsets.all(10.0),
      width: double.infinity,
      height: double.infinity,
      // color: Colors.pink,
      // child: SingleChildScrollView(
        child: 
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            _saticfactionLevel(),
            _submitButtron(),
          ],
        ),
      // ),
      
    );
  }

  Widget _saticfactionLevel(){  
    return Container(
      child: Column(        
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text('Saticfaction level',style: TextStyle(
            fontWeight: FontWeight.bold
          ),),
          Container(margin: EdgeInsets.only(bottom: 20.0),),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              saticfactionButton('Very Good'),
              saticfactionButton('Good'),
              saticfactionButton('displease'),
            ],
          ),
          Container(margin: EdgeInsets.only(bottom: 30.0),),
          Text('Opinion',style: TextStyle(
            fontWeight: FontWeight.bold
          ),),
          Container(margin: EdgeInsets.only(bottom: 5.0),),
          Container(
          margin: EdgeInsets.only(top: 10),            
          child: TextField(
          // controller: desriptionWorkCOntroller,
          keyboardType: TextInputType.multiline,
          decoration: InputDecoration(
            hintText: 'Saticfaction',
            border: OutlineInputBorder()
          ),
        maxLines: 3,
      ),
    ),


        ],
      ),
    );
  }


  Widget saticfactionButton(String label){
    return Container(
      child: Column(
        children: <Widget>[
          IconButton(
            icon: Icon(Icons.insert_emoticon),    
            onPressed: (){},
          ),
          Text('$label')
        ],
      ),

    );
  }
  Widget _submitButtron(){
    return Container(
      // padding: EdgeInsets.all(10.0),
      width: double.infinity,
      child: RaisedButton(
        color: Colors.deepOrange,
        child: Text('Send',style: TextStyle(
          color: Colors.white
        ),),
        onPressed: (){},
      ),
    );
  }
}