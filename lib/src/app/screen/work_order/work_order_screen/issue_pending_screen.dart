import 'dart:convert';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:http/http.dart' as http;
import 'package:chartium/src/app/screen/work_order/models-v2/work_order_c.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class IssuePendingScreen extends StatefulWidget {
  WokrOrderC workOrder;

  IssuePendingScreen({ @required this.workOrder });

  @override
  _IssuePendingScreenState createState() => _IssuePendingScreenState();
}

class _IssuePendingScreenState extends State<IssuePendingScreen>  with BaseApi {
  final _inputController = TextEditingController();

  final formkey = GlobalKey<FormState>();


  @override
  void initState() { 
    super.initState();
    token = Provider.of<UserState>(context,listen: false).getToken();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset:false,
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: () => Navigator.pop(context),
          ),
          backgroundColor: Colors.white,
          title: Text('Issue',style: TextStyle(
            color: Colors.black
          ),),
        ),
        body: _content(context),
      ),
    );
  }

  Widget _content(BuildContext context){
    return  Container(
      height: double.infinity,
      width: double.infinity,
      padding: EdgeInsets.all(30.0),
      child: Form(
        key: formkey,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
              _label(),
              _form(),
              Container(margin: EdgeInsets.only(bottom: 30.0),),
            _submitButton(context),
          ], 
        ),
      ),
    );
  }

  Widget _label(){
    return RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Description'),
              ]
            ),
          );
  }

  Widget _form(){
    return Container(
          child: TextFormField(
          maxLines: 5, 
          controller: _inputController,
          decoration: InputDecoration(
            border: OutlineInputBorder(),
            hintText: "Enter your issue"
          ),
          validator: (String text){
              if (text.isEmpty) {
                return 'Plase enter issue';
              }

            return null;
        },
      ),
    );

  }

  Widget _submitButton(BuildContext context){
   return Container(
    width: double.infinity,
     child: RaisedButton(
          onPressed: (){            
          if (formkey.currentState.validate()) {
              updateWorkOrder(context);
          } else {
            showDialog(
              context: context,
              barrierDismissible: true,
              builder: (BuildContext context){
                return AlertDialog(
                  content: Text('Please finish required form'),
              );
          }
      );
        }
          },
            child: Text('Send',style: TextStyle(
              color: Colors.white
            ),),
            color: Colors.deepOrange,
          ),
   );
  }

  updateWorkOrder(BuildContext context) async {
     String url = '${baseAPi}papi/workorder';
    Map data = {"apiRequest":{"action":"update"},"data": [{
            "accountId": widget.workOrder.accountId,
            "accountTitle": widget.workOrder.accountTitle,
            "accountFirstName": widget.workOrder.accountFirstName,
            "accountLastName": widget.workOrder.accountLastName,
            "roomId": widget.workOrder.roomId,
            "roomNo": widget.workOrder.roomNo,
            "scheduleDate":widget.workOrder.scheduleDate,
            "scheduleTime":widget.workOrder.scheduleTime,
            "workOrderStatusId": 8,
            "evaluation":widget.workOrder.evaluation,
            "remark":widget.workOrder.remark,
            "workOrderItemId": widget.workOrder.workOrderItemId,
            "workOrderTypeId": widget.workOrder.workOrderTypeId,
            "workOrderNo": widget.workOrder.workOrderNo,
            "assignToId": widget.workOrder.assignToId,
            "requestDate": widget.workOrder.requestDate,
            "requestTime": widget.workOrder.requestTime,
            "issue": _inputController.text,
            "workOrderImages": widget.workOrder.workOrderImages.map((f) => {
              "workOrderId": f.workOrderId,
              "name": f.name,
              "id": f.id,
            }).toList(),           
            "id": widget.workOrder.id
    }]};
    // print(data);
    var body = json.encode(data);
    // print(body);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
        },
    );
    print(response.body);
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(            
            content: Text('${json.decode(response.body)['apiResponse']['desc']}'),
          );
        }
    );
      
   }
}