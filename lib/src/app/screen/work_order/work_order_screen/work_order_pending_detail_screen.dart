import 'package:carousel_slider/carousel_slider.dart';
import 'package:chartium/src/app/components/Saticfaction_dialog.dart';

import 'package:chartium/src/app/screen/work_order/models-v2/work_order_c.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';

import 'issue_pending_screen.dart';

class  WorkOrderPendingScreen extends StatefulWidget {
  WokrOrderC workOrder;
  WorkOrderPendingScreen({ this.workOrder });
  @override
  _WorkOrderPendingScreenState createState() => _WorkOrderPendingScreenState();
}

class _WorkOrderPendingScreenState extends State<WorkOrderPendingScreen>  with BaseApi{
  @override
  Widget build(BuildContext context) {    
    return MaterialApp(
      home: Scaffold(
        resizeToAvoidBottomInset:false,  
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          centerTitle: true,
          backgroundColor: Colors.white,
          title: Text("Pending detail",style: TextStyle(
            color: Colors.black
          ),),
        ),
        body: _content(context),

      ),    
    );
  }

  Widget _content(BuildContext context){
    return Container(
      width: double.infinity,
      height: double.infinity,
      padding: EdgeInsets.all(10),
      decoration: BoxDecoration(
        border: Border.all(color: Color.fromRGBO(226, 226, 226, 1)),
      ),
      child: ContainerContent(context),
    );
  }

  Widget ContainerContent(BuildContext context) {
    return SingleChildScrollView(
          child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Case ID: '),
                TextSpan(text: '${widget.workOrder.workOrderNo}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Room No.: '),
                TextSpan(text: '${widget.workOrder.roomNo}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Work Order Item: '),
                TextSpan(text: '${widget.workOrder.workOrderItemName}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Service Date: '),
                TextSpan(text: '${ formatter.format(DateTime.parse(widget.workOrder.requestDate.substring(0,10)))} ${widget.workOrder.requestTime.substring(0,5)}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ),
          RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Description: '),
                TextSpan(text: '${widget.workOrder.remark}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ),
            widget.workOrder.workOrderStatusId != 1 ?  RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Service by: '),
                TextSpan(text: '${widget.workOrder.getFullNameAssiagnBy()}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ):Container(),
          Container(margin: EdgeInsets.only(bottom: 30.0),),
          ContainerImage(),
          Container(margin: EdgeInsets.only(bottom: 30.0),),
          widget.workOrder.issue != null ? RichText(
            text: TextSpan(
              style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
              ),              
              children: <TextSpan>[
                TextSpan(text: 'Issue: '),
                TextSpan(text: '${widget.workOrder.issue}',style: TextStyle(fontWeight: FontWeight.bold))
              ]
            ),
          ): Container(),
          Container(margin: EdgeInsets.only(bottom: 30.0),),
          Row(
            children: <Widget>[
              Expanded(
                flex: 1,
                child: Container(                  
                  child: RaisedButton(
                      color: Colors.deepOrange,
                      child: Text('Complete',style: TextStyle(
                        color: Colors.white
                      ),),
                      onPressed: (){
                        showDialog(
                          context: context,                          
                          builder: (BuildContext context) => SaticfactionDailog(workOrder: widget.workOrder)
                        );
                      },
                    ),
                ),
              ),
              Container(margin: EdgeInsets.only(left: 30.0),),
              Expanded(
                flex: 1,
                child: Container(
                  child: RaisedButton(
                      child: Text('Issue'),
                      onPressed: (){
                        // IssuePendingScreen
                        Navigator.push(
                          context, 
                          MaterialPageRoute(builder: (context) => IssuePendingScreen(workOrder: widget.workOrder))
                          );
                      },
                    ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }

  Widget ContainerImage(){
      return Center( 
        child: Container(
          width: double.infinity,
          child: widget.workOrder.workOrderImages.isEmpty ? Image.asset('assets/images/noimage.jpg',
                height: 300.0,
              ): 
              CarouselSlider(
                enableInfiniteScroll: false,
              // reverse: true,
              height: 400.0,
              items: widget.workOrder.workOrderImages.map((i) {
                return Builder(
              builder: (BuildContext context) {
                  return Container(
                    width: MediaQuery.of(context).size.width,
                    margin: EdgeInsets.symmetric(horizontal: 5.0),
                    child: Image.network('$baseAPi'+i.name,
                              height: 300.0,
                              alignment: Alignment.center,
                            ),
                  );
                },
              );
            }).toList(),
)
        ),
      );
  }

}