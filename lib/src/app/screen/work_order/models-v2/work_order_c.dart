import 'package:intl/intl.dart';

class WokrOrderC {
  int accountId;
  String accountTitle;
  String accountFirstName;
  String accountLastName;
  int roomId;
  String roomNo;
  String remark;
  int workOrderStatusId;
  String workOrderStatusCode;
  String workOrderStatusName;
  int workOrderItemId;
  String workOrderItemCode;
  String workOrderItemName;
  int workOrderTypeId;
  String workOrderTypeCode;
  String workOrderTypeName;
  String workOrderNo;
  int assignToId;
  String requestDate;
  String requestTime;
  String scheduleDate;
  String scheduleTime;
  String serviceDate;
  String serviceTime;
  List<WorkOrderImages> workOrderImages;
  Topic topic;
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;
  String assignToTitle;
  String assignToFirstName;
  String assignToLastName;
  int evaluation;
  String issue;
  var _formatter = DateFormat("dd-MM-yyyy");
  WokrOrderC(
      {this.accountId,
      this.accountTitle,
      this.accountFirstName,
      this.accountLastName,
      this.roomId,
      this.roomNo,
      this.remark,
      this.workOrderStatusId,
      this.workOrderStatusCode,
      this.workOrderStatusName,
      this.workOrderItemId,
      this.workOrderItemCode,
      this.workOrderItemName,
      this.workOrderTypeId,
      this.workOrderTypeCode,
      this.workOrderTypeName,
      this.workOrderNo,
      this.assignToId,
      this.requestDate,
      this.requestTime,
      this.scheduleDate,
      this.scheduleTime,
      this.serviceDate,
      this.serviceTime,
      this.workOrderImages,
      this.topic,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.assignToTitle,
      this.assignToFirstName,
      this.assignToLastName,
      this.lastUpdateDate,
      this.evaluation,
      this.issue
      });
  String getFullname() {
    return '${this.accountTitle} ${this.accountFirstName} ${this.accountLastName}';
  }

  String getFullNameAssiagnBy(){
    return '${this.assignToTitle} ${this.assignToFirstName} ${this.assignToLastName}';
  }

  WokrOrderC.fromJson(Map<String, dynamic> json) {
    accountId = json['accountId'];
    accountTitle = json['accountTitle'];
    accountFirstName = json['accountFirstName'];
    accountLastName = json['accountLastName'];
    roomId = json['roomId'];
    roomNo = json['roomNo'];
    remark = json['remark'];
    evaluation =json['evaluation'];
    assignToId =json['assignToId'];
    assignToTitle =json['assignToTitle'];
    assignToFirstName =json['assignToFirstName'];
    assignToLastName =json['assignToLastName'];
    workOrderStatusId = json['workOrderStatusId'];
    workOrderStatusCode = json['workOrderStatusCode'];
    workOrderStatusName = json['workOrderStatusName'];
    workOrderItemId = json['workOrderItemId'];
    workOrderItemCode = json['workOrderItemCode'];
    workOrderItemName = json['workOrderItemName'];
    workOrderTypeId = json['workOrderTypeId'];
    workOrderTypeCode = json['workOrderTypeCode'];
    workOrderTypeName = json['workOrderTypeName'];
    workOrderNo = json['workOrderNo'];
    assignToId = json['assignToId'];
    requestDate = json['requestDate'];
    requestTime = json['requestTime'];
    scheduleDate = json['scheduleDate'];
    scheduleTime = json['scheduleTime'];
    serviceDate = json['serviceDate'];
    serviceTime = json['serviceTime'];
    issue =json['issue'];
    if (json['workOrderImages'] != null) {
      workOrderImages = new List<WorkOrderImages>();
      json['workOrderImages'].forEach((v) {
        workOrderImages.add(new WorkOrderImages.fromJson(v));
      });
    }
    topic = json['topic'] != null ? new Topic.fromJson(json['topic']) : null;
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accountId'] = this.accountId;
    data['accountTitle'] = this.accountTitle;
    data['accountFirstName'] = this.accountFirstName;
    data['accountLastName'] = this.accountLastName;
    data['roomId'] = this.roomId;
    data['roomNo'] = this.roomNo;
    data['remark'] = this.remark;
    data['workOrderStatusId'] = this.workOrderStatusId;
    data['workOrderStatusCode'] = this.workOrderStatusCode;
    data['workOrderStatusName'] = this.workOrderStatusName;
    data['workOrderItemId'] = this.workOrderItemId;
    data['workOrderItemCode'] = this.workOrderItemCode;
    data['workOrderItemName'] = this.workOrderItemName;
    data['workOrderTypeId'] = this.workOrderTypeId;
    data['workOrderTypeCode'] = this.workOrderTypeCode;
    data['workOrderTypeName'] = this.workOrderTypeName;
    data['workOrderNo'] = this.workOrderNo;
    data['assignToId'] = this.assignToId;
    data['requestDate'] = this.requestDate;
    data['requestTime'] = this.requestTime;
    data['scheduleDate'] = this.scheduleDate;
    data['scheduleTime'] = this.scheduleTime;
    data['serviceDate'] = this.serviceDate;
    data['serviceTime'] = this.serviceTime;
    if (this.workOrderImages != null) {
      data['workOrderImages'] =
          this.workOrderImages.map((v) => v.toJson()).toList();
    }
    if (this.topic != null) {
      data['topic'] = this.topic.toJson();
    }
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    data['issue'] = this.issue;
    return data;
  }
}

class WorkOrderImages {
  int workOrderId;
  String name;
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;

  WorkOrderImages(
      {this.workOrderId,
      this.name,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.lastUpdateDate});

  WorkOrderImages.fromJson(Map<String, dynamic> json) {
    workOrderId = json['workOrderId'];
    name = json['name'];
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['workOrderId'] = this.workOrderId;
    data['name'] = this.name;
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    return data;
  }
}

class Topic {
  int accountId;
  String accountTitle;
  String accountFirstName;
  String accountLastName;
  int roomId;
  String roomNo;
  int workOrderId;
  String workOrderNo;
  String workOrderItemCode;
  String workOrderItemName;
  int topicStatusId;
  String topicStatusCode;
  String topicStatusName;
  int unreadMessage;
  int customerUnreadMessage;
  String accountImageUrl;
  String lastMessage;
  List<Messages> messages;
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;

  Topic(
      {this.accountId,
      this.accountTitle,
      this.accountFirstName,
      this.accountLastName,
      this.roomId,
      this.roomNo,
      this.workOrderId,
      this.workOrderNo,
      this.workOrderItemCode,
      this.workOrderItemName,
      this.topicStatusId,
      this.topicStatusCode,
      this.topicStatusName,
      this.unreadMessage,
      this.customerUnreadMessage,
      this.accountImageUrl,
      this.lastMessage,
      this.messages,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.lastUpdateDate});

  Topic.fromJson(Map<String, dynamic> json) {
    accountId = json['accountId'];
    accountTitle = json['accountTitle'];
    accountFirstName = json['accountFirstName'];
    accountLastName = json['accountLastName'];
    roomId = json['roomId'];
    roomNo = json['roomNo'];
    workOrderId = json['workOrderId'];
    workOrderNo = json['workOrderNo'];
    workOrderItemCode = json['workOrderItemCode'];
    workOrderItemName = json['workOrderItemName'];
    topicStatusId = json['topicStatusId'];
    topicStatusCode = json['topicStatusCode'];
    topicStatusName = json['topicStatusName'];
    unreadMessage = json['unreadMessage'];
    customerUnreadMessage = json['customerUnreadMessage'];
    accountImageUrl = json['accountImageUrl'];
    lastMessage = json['lastMessage'];
    if (json['messages'] != null) {
      messages = new List<Messages>();
      json['messages'].forEach((v) {
        messages.add(new Messages.fromJson(v));
      });
    }
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['accountId'] = this.accountId;
    data['accountTitle'] = this.accountTitle;
    data['accountFirstName'] = this.accountFirstName;
    data['accountLastName'] = this.accountLastName;
    data['roomId'] = this.roomId;
    data['roomNo'] = this.roomNo;
    data['workOrderId'] = this.workOrderId;
    data['workOrderNo'] = this.workOrderNo;
    data['workOrderItemCode'] = this.workOrderItemCode;
    data['workOrderItemName'] = this.workOrderItemName;
    data['topicStatusId'] = this.topicStatusId;
    data['topicStatusCode'] = this.topicStatusCode;
    data['topicStatusName'] = this.topicStatusName;
    data['unreadMessage'] = this.unreadMessage;
    data['customerUnreadMessage'] = this.customerUnreadMessage;
    data['accountImageUrl'] = this.accountImageUrl;
    data['lastMessage'] = this.lastMessage;
    if (this.messages != null) {
      data['messages'] = this.messages.map((v) => v.toJson()).toList();
    }
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    return data;
  }
}

class Messages {
  int topicId;
  String message;
  int messageStatusId;
  String messageStatusCode;
  String messageStatusName;
  int replyById;
  int accountId;
  int workOrderId;
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;

  Messages(
      {this.topicId,
      this.message,
      this.messageStatusId,
      this.messageStatusCode,
      this.messageStatusName,
      this.replyById,
      this.accountId,
      this.workOrderId,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.lastUpdateDate});

  Messages.fromJson(Map<String, dynamic> json) {
    topicId = json['topicId'];
    message = json['message'];
    messageStatusId = json['messageStatusId'];
    messageStatusCode = json['messageStatusCode'];
    messageStatusName = json['messageStatusName'];
    replyById = json['replyById'];
    accountId = json['accountId'];
    workOrderId = json['workOrderId'];
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['topicId'] = this.topicId;
    data['message'] = this.message;
    data['messageStatusId'] = this.messageStatusId;
    data['messageStatusCode'] = this.messageStatusCode;
    data['messageStatusName'] = this.messageStatusName;
    data['replyById'] = this.replyById;
    data['accountId'] = this.accountId;
    data['workOrderId'] = this.workOrderId;
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    return data;
  }
}