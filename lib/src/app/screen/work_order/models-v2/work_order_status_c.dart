class WorkOrderStatusModels {
  String code;
  String name;
  int id;
  int rowStatus;
  String createBy;
  String createDate;
  String lastUpdateBy;
  String lastUpdateDate;

  WorkOrderStatusModels(
      {this.code,
      this.name,
      this.id,
      this.rowStatus,
      this.createBy,
      this.createDate,
      this.lastUpdateBy,
      this.lastUpdateDate});

  WorkOrderStatusModels.fromJson(Map<String, dynamic> json) {
    code = json['code'];
    name = json['name'];
    id = json['id'];
    rowStatus = json['rowStatus'];
    createBy = json['createBy'];
    createDate = json['createDate'];
    lastUpdateBy = json['lastUpdateBy'];
    lastUpdateDate = json['lastUpdateDate'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['code'] = this.code;
    data['name'] = this.name;
    data['id'] = this.id;
    data['rowStatus'] = this.rowStatus;
    data['createBy'] = this.createBy;
    data['createDate'] = this.createDate;
    data['lastUpdateBy'] = this.lastUpdateBy;
    data['lastUpdateDate'] = this.lastUpdateDate;
    return data;
  }
}