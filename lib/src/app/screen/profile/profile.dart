import 'dart:convert';

import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/forget_password.dart';
import 'package:chartium/src/app/screen/login_screen/login_screen.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'edit_profile_screen.dart';
import 'package:http/http.dart' as http;

import 'models/account_models.dart';
import 'package:package_info/package_info.dart';
class profile extends StatefulWidget {
  @override
  _profileState createState() => _profileState();
}

class _profileState extends State<profile> with BaseApi{
  UserModel currentUser;
  String nameAcc;
  String imageUrl;
  String propertyName;
  int accountid;
  AccountModel account;
  List<Widget> list = [];
  String versionApp;
  @override
  void initState() { 
    super.initState();     
    currentUser = Provider.of<UserState>(context,listen: false).getUser();   
    token = Provider.of<UserState>(context,listen: false).getToken();
    nameAcc = Provider.of<UserState>(context,listen: false).getName();
    imageUrl = Provider.of<UserState>(context,listen: false).getUrlImage();
    propertyName = Provider.of<UserState>(context,listen: false).getPropertyName();
    accountid = Provider.of<UserState>(context,listen: false).getId();
    print(token);
    getPackage();
    callHttp();
  }

  getPackage() async {    
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
      print('version');
      print(packageInfo.version);
      print(packageInfo.buildNumber);
      versionApp = '${packageInfo?.version}+${packageInfo?.buildNumber}';
      setState(() {});
  }

  callHttp() async {
  String url = '${baseAPi}papi/account';
  Map data = {"apiRequest":{"action":"get"},"data":[{
    "id": accountid
    }]};
  var body = json.encode(data);
  var response = await http.post(url,
    body: body,
    headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
      },
  );  
  print("account profile here");
  print(response.body);
  account = AccountModel.fromJson(json.decode(response.body)['data'][0]);
  setState(() {
  });
  }

  @override
  Widget build(BuildContext context) {    
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          centerTitle: true,
          title: Text('Profile',style: TextStyle(
            color:Colors.black
          ),),
          backgroundColor: Colors.white,
          actions: <Widget>[
            IconButton(
              icon: Icon(Icons.edit),
              color: Colors.black,
              onPressed: () {
                Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => editProfile()),
                );
              },
            ),
          ],
        ),
        body: content(context),
      ),
    );
  }

  Widget content(BuildContext context){
    // print('$baseAPi${imageUrl.substring(1)}');
    return  
    Container(
      padding: EdgeInsets.all(20),
      width: double.infinity,
      height: double.infinity,
      child: SingleChildScrollView(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          imageUrl.isNotEmpty ? 
          Container(
              width: 150,
              height: 150,
              child: CircleAvatar(
                    backgroundImage: NetworkImage('$baseAPi${imageUrl}'),
              ),
          ):
           Container(
            width: 150,
            height: 150,            
            decoration:  BoxDecoration(
              shape: BoxShape.circle,
              image:  DecorationImage(
                    fit: BoxFit.fill,
                    image:  AssetImage('assets/images/noimage.jpg')
              )
            )
          ),
            deteil('Account: ${nameAcc}'),
            Divider(),
            deteil('Room No.: ${account?.roomNo}'),
            Divider(),
            deteil('Property: ${propertyName}'),
            Divider(),
            GestureDetector(
              onTap: (){
                Navigator.push(context,
                          MaterialPageRoute(builder: (context) => forgetPasswordScreen()),
                      );
              },
             child: Container(
                  margin: EdgeInsets.only(bottom: 10),
                    child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Text('Reset Password',style: TextStyle(
                        color: Colors.black,
                        fontSize: 15
                      )                    
                      ,),
                      IconButton(
                        icon: Icon(Icons.chevron_right),
                        onPressed: (){
                          Navigator.push(context,
                            MaterialPageRoute(builder: (context) => forgetPasswordScreen()),
                        );
                        },
                      )
                    ],
                  ),
              ),
            ),
            Divider(),
            Row(
              children: <Widget>[
                Text('Logout',style: TextStyle(
                  color: Colors.black,
                  fontSize: 15
                ),),
                IconButton(
                  icon: Icon(Icons.exit_to_app),
                  onPressed: () async {
                        SharedPreferences sp = await SharedPreferences.getInstance();
                        await sp.clear();
                        Navigator.of(context).pushAndRemoveUntil(
                          MaterialPageRoute(builder: (context) => LoginScreen()),
                          (route) => false
                        );
                      },
                )
              ],
            ),
            Divider(),
            versionApp != null ? deteil('Version: '+versionApp) :Container(),
        ],
      ),
    ),
    );    
  }

  Widget deteil(String detail){
    return Container(      
      margin: EdgeInsets.only(
        top: 10.0,
        bottom: 10.0,
        right: 10.0
      ),
      child: Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Expanded(
                  child: Text('$detail',style: TextStyle(
              color: Colors.black,
              fontSize: 15
            ),
          ),
        ),
      ],
    ),
  );    
  }

  Widget doubleDetail (String left,String right){
    return Container(
      margin: EdgeInsets.all(10),
      child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('$left'),
        Text('$right'),
      ],
    ),
  );
  }
}