import 'dart:convert';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:http/http.dart' as http;
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'models/perfix_model.dart';

class resident extends StatefulWidget {
  @override
  _residentState createState() => _residentState();
}

class _residentState extends State<resident>  with BaseApi{
  final List<PrefixModel> titles =  [];
  UserModel currentUser;
  final firstNameController = TextEditingController();
  final lastNameController = TextEditingController();
  final middleNameController =TextEditingController();
  String preFixId = '10000000-0000-0000-0000-000000000000';

  @override
  void initState() { 
    super.initState();
    currentUser = Provider.of<UserState>(context,listen: false).getUser();
    callHttp();
  }
callHttp() async {
  String url = '${baseAPi}papi/configuration/prefixProfile';
  Map data = {"apiRequest":{"action":"list"}};
  var body = json.encode(data);
  var response = await http.post(url,
    body: body,
    headers: {"Content-Type": "application/json"},
  );
  // print(response.statusCode);
  print(json.decode(response.body)['data']);
  
  (json.decode(response.body)['data'] as List).forEach((prefix){      
    // list.add(doubleDetail('Resident', UserModel.formJson(resident).name));
    titles.add(PrefixModel.fromJson(prefix));
  });    
  print(titles.length);
  setState(() {
  });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pop(context);
            },
          ),
          title: Text('Add Resident',style: TextStyle(color: Colors.black)),
          backgroundColor: Colors.white,
          // actions: <Widget>[
          //   Center(
          //     child: GestureDetector(
          //       onTap: () => saveResident(),
          //       child: Text('save',style: TextStyle(
          //         color: Colors.black,
          //         fontSize: 20
          //       )),
          //     ),
          //   )
          // ],
          ),
        body: content(context),
      ),
    );
  }

  Widget content(BuildContext context){
    return Container(
      padding: EdgeInsets.only(top: 10,left: 20,right: 10,bottom: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Align(
            alignment: Alignment.centerLeft,
            child: Text('Resident'),
          ),
          _dropdownTitle(),
          Container(margin: EdgeInsets.only(bottom: 10)),
          _inputFirstName(),
          Container(margin: EdgeInsets.only(bottom: 10)),
          _inputLastname(),
          Container(margin: EdgeInsets.only(bottom: 10)),
          _inputMiddleName(),
          Container(margin: EdgeInsets.only(bottom: 10)),
          submitButton(context),
        ],
      ),
    );
  }

  Widget _inputFirstName(){
    return TextFormField(
      controller: firstNameController,
      decoration: InputDecoration(
        hintText: 'Enter FirstName',
        border: OutlineInputBorder()
      ),
    );      
  }
  Widget _inputLastname(){
    return TextFormField(
      controller: lastNameController,
      decoration: InputDecoration(        
        hintText: 'Enter LastName',
        border: OutlineInputBorder()
      ),
    );      
  }

  Widget _inputMiddleName(){
    return TextFormField(
      controller: middleNameController,
      decoration: InputDecoration(        
        hintText: 'Enter Middle Name',
        border: OutlineInputBorder()
      ),
    );      
  }  
  Widget _dropdownTitle(){
    return Container(
        width: double.infinity,
        margin: EdgeInsets.only(top: 10),
        padding: EdgeInsets.symmetric(horizontal: 10.0),
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(5.0),
          border: Border.all(
            color: Colors.grey, style: BorderStyle.solid, width: 0.80),
          ),
          child: DropdownButton(
          isExpanded: true,
          hint: Text('Prefix',textAlign: TextAlign.right,),
          items: titles.map<DropdownMenuItem<String>>((value) {
            return DropdownMenuItem<String>(
              value: value.id,
              child: Text('${value.name}',textAlign: TextAlign.center,),
            );
          }).toList(),
        onChanged: (id) {
          setState(() {
            print(id);
            preFixId = id;
          });
        },
        value: preFixId,
      ),
    );
  }

  Widget submitButton(BuildContext context){
    return Container(
      // padding: EdgeInsets.only(left: 10),
      child: SizedBox(
      width: double.infinity,
      child: RaisedButton(
      color: Colors.deepOrange,
      child: Text('Save',style: TextStyle(
        color: Colors.white
      ),),
      onPressed: (){
        saveResident();
      },
      ),
    ),
    );
  }
  saveResident() async {
    String url = '${baseAPi}papi/configuration/prefixProfile';
    Map data = {"apiRequest":{"action":"list"},"data":[{
      "prefixId": preFixId,
      "name": firstNameController.text,
      "surName": lastNameController.text,
      "middleName": middleNameController.text,
      // "renterId": currentUser.renterId,
      // "roomNo": "3041"
    }]};
    var body = json.encode(data);
    var response = await http.post(url,
      body: body,
      headers: {"Content-Type": "application/json"},
    );
   showDialog(
        context: context,
        builder: (context) {
        return AlertDialog(
          // title: Text('Can not login'),
          content: Text('${json.decode(response.body)['apiResponse']['desc']}'),
        );      
      }
    );
  setState(() {
  });
  }
}