import 'dart:convert';
import 'dart:io';
import 'package:chartium/src/app/screen/main/main_screen.dart';
// import 'package:chartium/src/app/screen/profile/profile.dart';
import 'package:image_picker/image_picker.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'add_resident_screen.dart';
import 'package:http/http.dart' as http;

import 'models/account_models.dart';
class editProfile extends StatefulWidget {
  @override
  _editProfileState createState() => _editProfileState();
}

class _editProfileState extends State<editProfile> with BaseApi {
  List<Widget> list = [];
  UserModel currentUser;
  File imageFile;
  String imagePath = '';
  String name;
  int id;
  String imageUrl;
  final firstNameControllel = TextEditingController();
  final lastNameController =TextEditingController();
  final passWordController =TextEditingController();
  final emailController =TextEditingController();
  final ageController =TextEditingController();
  AccountModel account;
  String prefixId = 'Mr.';
  final List<String> buildings = ['a','b','c'];
  final List<String> rooms = ['a','b','c'];
  String dropdownValueBuilding = 'a';
  String dropdownValueRoom = 'a';

  @override
  void initState() { 
    currentUser = Provider.of<UserState>(context,listen: false).getUser();
    id = Provider.of<UserState>(context,listen: false).getId();
    imageUrl = Provider.of<UserState>(context,listen: false).getUrlImage();
    token = Provider.of<UserState>(context,listen: false).getToken();
    print(imageUrl);
    callHttp();    
    super.initState();
  }

callHttp() async {
  String url = '${baseAPi}papi/account';
  Map data = {"apiRequest":{"action":"get"},"data":[{
    "id": id
    }]};
  var body = json.encode(data);
  var response = await http.post(url,
    body: body,
    headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
      },
  );  
  print(response.statusCode);
  account =AccountModel.fromJson(json.decode(response.body)['data'][0]);
  firstNameControllel.text = account.firstName;
  lastNameController.text =account.lastName;
  passWordController.text =account.password;
  emailController.text =account.email;
  ageController.text =account.title;
  setState(() {
  });
  }

   void _onChangeSelectAge(String value){
    print(value);
    setState(() {
      prefixId = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(Icons.arrow_back),
            color: Colors.black,
            onPressed: (){
              Navigator.pushReplacement(
                  context,
                  MaterialPageRoute(builder: (context) => MainScreen(page: 4)),
                );
            },
          ),
          backgroundColor: Colors.white,
          centerTitle: true,
          title: Text('Profile',style: TextStyle(
            color: Colors.black
          ),),
          
        ),
        body: Builder(
          builder: (context) => content(context),
        )
        
      ),
    );
  }

  Widget content(BuildContext context){
    return Container(
      width: double.infinity,
      padding: EdgeInsets.all(10),
      child: SingleChildScrollView(
        // controller: controller,
        child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          imageUrl.isNotEmpty ?
          GestureDetector(
            onTap: _takePicture,
            child: imageFile  != null ? CircleAvatar(backgroundImage: FileImage(imageFile),radius: 100.0) : 
            Container(
              width: 150,
              height: 150,
              child: CircleAvatar(
                    backgroundImage: NetworkImage('$baseAPi${imageUrl}'),
                  ),
            )
          )
           :           
          GestureDetector(
            onTap: _takePicture,
            child: imageFile  != null ? 
            Container(
              width: 150,
              height: 150,
              child: CircleAvatar(backgroundImage: FileImage(imageFile),radius: 100.0),
            )
            :
            Container(
              width: 150,
              height: 150,
              decoration:  BoxDecoration(
                  shape: BoxShape.circle,
                  image:  DecorationImage(
                      fit: BoxFit.fill,
                      image: AssetImage('assets/images/noimage.jpg')
              )
            )),
          ),
          Container(margin: EdgeInsets.only(bottom: 20),),
          _inputAge(),
          Container(margin: EdgeInsets.only(bottom: 20),),
          _inputFirstName(),
          Container(margin: EdgeInsets.only(bottom: 20),),          
          _inputLastName(),
          Container(margin: EdgeInsets.only(bottom: 20),),
          _inputEmail(),
          Container(margin: EdgeInsets.only(bottom: 20),),
          Container(margin: EdgeInsets.only(bottom: 20),),
          // Divider(),
          // Divider(),     
          submitButton(context),    
        ],
      ),
      ),
    );
  }


  Widget addResident(){
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text('Add Resident'),
          IconButton(
            icon: Icon(Icons.add),
            onPressed: (){
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => resident()),
              );      
            },
          )
        ],
      ),
    );
  }  

  Widget _masterAccount(){
    return Container(
      margin: EdgeInsets.only(top: 20,bottom: 20),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text('Master Account'),
          // Text('${currentUser.getFullname()}')
        ],
      ),
    );
  }
  Widget _resetPassword(){
    return Align(
      alignment: Alignment.centerLeft,
      child: Text('Reset password',style: TextStyle(
        decoration: TextDecoration.underline
      )),
    );
  }

  Widget ages() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
        Radio(
            
          groupValue:prefixId, 
          onChanged: null, 
          value: 'Mr.',
        ),
        Text('Mr.'),
        Radio(
          groupValue: prefixId, 
          // onChanged: _onChangeSelectAge, 
          onChanged: null,
          value: 'Miss.',          
        ),
        Text('Miss.'),
        Radio(
          groupValue: prefixId, 
          onChanged: null, 
          value: 'Mrs.',          
        ),
        Text('Mrs.')
      ],
    );
  }

  Widget _inputFirstName(){
    return TextFormField(
      enabled: false,
      // obscureText: true,
      controller: firstNameControllel,
      // initialValue: currentUser.name,
      decoration: InputDecoration(
        // labelText: 'Password',
        hintText: 'FirstName',
        border: OutlineInputBorder()
      ),
      // validator: validatorPassword,
      // onSaved: (String value){
      //   password = value;
      // },
    );      
  }

  Widget _inputAge(){
    return TextFormField(
      enabled: false,
      // obscureText: true,
      controller: ageController,
      decoration: InputDecoration(
        // labelText: 'Password',
        hintText: 'Prefix',
        border: OutlineInputBorder()
      ),
      // validator: validatorPassword,
      // onSaved: (String value){
      //   password = value;
      // },
    );      
  }

  Widget _inputLastName(){
    return TextFormField(
      enabled: false,
      controller: lastNameController,
      // obscureText: true,
      decoration: InputDecoration(
        // labelText: 'Password',
        hintText: 'Lastname',
        border: OutlineInputBorder()
      ),
      // validator: validatorPassword,
      // onSaved: (String value){
      //   password = value;
      // },
    );      
  }

  Widget _inputPhone(){
    
    return TextFormField(
      enabled: false,
      obscureText: true,
      decoration: InputDecoration(
        // labelText: 'Password',
        hintText: 'Phone',
        border: OutlineInputBorder()
      ),
      // validator: validatorPassword,
      // onSaved: (String value){
      //   password = value;
      // },
    );      
  }

  Widget _inputEmail(){
    return TextFormField(
      enabled: false,
      controller: emailController,
      // initialValue: currentUser.emailId,
      decoration: InputDecoration(
        // labelText: 'Password',
        hintText: 'Email',
        border: OutlineInputBorder()
      ),
      // validator: validatorPassword,
      // onSaved: (String value){
      //   password = value;
      // },
    );      
  }

  Widget _inputPassword(){
    return TextFormField(
      enabled: false,
      // obscureText: true,
      controller: passWordController,
      // initialValue: currentUser.password,
      decoration: InputDecoration(
        labelText: 'Password',
        hintText: 'Password',
        border: OutlineInputBorder()
      ),
      // validator: validatorPassword,
      // onSaved: (String value){
      //   password = value;
      // },
    );      
  }

  Widget _dropdown(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          flex: 1,
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              decoration: BoxDecoration(              
              borderRadius: BorderRadius.circular(5.0),
              border: Border.all(
                  color: Colors.grey, style: BorderStyle.solid, width: 0.80),
              ),
              child: DropdownButton<String>(
              hint: Text('Building',textAlign: TextAlign.right,),
              items: buildings.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value,textAlign: TextAlign.center,),
                );
              }).toList(),
              onChanged: (value) {
                setState(() {
                  dropdownValueBuilding = value;
                });
              },
              value: dropdownValueBuilding,
          ) ,
          )          
        ),
        Container(margin: EdgeInsets.only(right: 10)),
        Expanded(
          flex: 1,
          child: Container(
              padding: EdgeInsets.symmetric(horizontal: 10.0),
              decoration: BoxDecoration(              
              borderRadius: BorderRadius.circular(5.0),
              border: Border.all(
                  color: Colors.grey, style: BorderStyle.solid, width: 0.80),
              ),
              child: DropdownButton<String>(                  
              hint: Text('Room'),
              items: rooms.map<DropdownMenuItem<String>>((String value) {
                return DropdownMenuItem<String>(
                  value: value,
                  child: Text(value,textAlign: TextAlign.right,),
                );
              }).toList(),
              onChanged: (value) {
                print(value);
                setState(() {
                  dropdownValueRoom = value ;
                });
              },
              value: dropdownValueRoom,
            ),
          )
        )
      ],
    );    
  }
Widget doubleDetail (String left,String right){
    return Container(
      margin: EdgeInsets.all(10),
      child: Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Text('$left'),
        Text('$right'),
      ],
    ),
  );
  }

  Future updateUser(BuildContext context) async {
    String url = '${baseAPi}papi/account';
    Map data = {"apiRequest":{"action":"update"},"data":[
        {
            "roomId":account.roomId,
            "email": emailController.text,
            "firstName": firstNameControllel.text,
            "lastName": lastNameController.text,
            "title": ageController.text,
            "imageUrl": imagePath,
            "password": passWordController.text,
            "id": account.id,
            "countryId": account.countryId,
            "propertyId": account.propertyId,
            "roomNo" :account.roomNo
        }
    ]};    
    var body = json.encode(data);
    var response = await http.post(url,  
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
        },
    );
    // print(body);
    // print(data['data']);
    // Provider.of<UserState>(context,listen: false).setUserModel(UserModel.fromJson(json.decode(data['data'])));
    showDialog(
        context: context,
        builder: (context) {
        return AlertDialog(
          // title: Text('Can not login'),
          content: Text('${json.decode(response.body)['apiResponse']['desc']}'),
        );      
      }
    );
    // await callHttp();
    setState(() {
    });
  }

  Widget submitButton(BuildContext context){
    return Container(
      // padding: EdgeInsets.only(left: 10),
      child: SizedBox(
      width: double.infinity,
      child: RaisedButton(
      color: Colors.deepOrange,
      child: Text('Save',style: TextStyle(
        color: Colors.white
      ),),
      onPressed: () async {
        if ( imageFile !=null) {
          imagePath =  await uploadImage();  
        } 
        print(imagePath);
        Provider.of<UserState>(context,listen: false).updateImageUrl(imagePath);
        updateUser(context);
      },
      ),
    ),
    );
  }

Future<void> _takePicture() async{
  showDialog(
    context: context,
    builder: (context) {
      return SimpleDialog(
        // title: Text('data'),
        children: <Widget>[
          SimpleDialogOption(
            onPressed: () async {                
                 imageFile = await ImagePicker.pickImage(
                  source:ImageSource.camera,
                  maxWidth: 600,  
                );
                setState(() {
                  
                });
                Navigator.of(context, rootNavigator: true).pop();
              },
            child:  const Text('CAMERA'),
          ),
          SimpleDialogOption(
            onPressed: () async { 
                imageFile = await ImagePicker.pickImage(
                source :ImageSource.gallery,
                maxWidth: 600,  
              );
              setState(() {
                  
              });
              Navigator.of(context, rootNavigator: true).pop();
            },
            child:  const Text('GALLERY'),
          )
        ],
      );
    }
  );
  
  }



  uploadImage() async {
    var req =  http.MultipartRequest("POST",Uri.parse('${baseAPi}papi/upload/masterAccountImage'));
    // if(imageFile != null){
      var pic1 = await http.MultipartFile.fromPath("from_mobile", imageFile.path);
      req.files.add(pic1);
    // }
    var response = await req.send();
    var responseData = await response.stream.toBytes();
    var responseString = String.fromCharCodes(responseData);    
    // (json.decode(responseString)['data'] as List).forEach( (f){
    //     uploadedImages.add(ImageModel.fromJson(f));
    // });
    return json.decode(responseString)['data'][0]['path'] ;
    // print(json.decode(responseString)['data'][0]['path']);
  }
}