class AccountModel {
  int roomId;
  String roomNo;
  String title;
  String firstName;
  String lastName;
  String middleName;
  String phone;
  String email;
  String imageUrl;
  String password;

  int countryId;
  String countryCode;
  String countryName;
  int propertyId;
  int id;
  String createBy;

  AccountModel(
      {
      this.countryId,
      this.countryName,
      this.countryCode,
      this.propertyId,
      this.roomId,
      this.roomNo,
      this.title,
      this.firstName,
      this.lastName,
      this.middleName,
      this.phone,
      this.email,
      this.imageUrl,
      this.password,
      this.id,
      this.createBy});

  AccountModel.fromJson(Map<String, dynamic> json) {
    countryId =json['countryId'];
    countryName =json['countryName'];
    countryCode =json['countryCode'];
    propertyId =json['propertyId'];
    roomNo =json['roomNo'];
    roomId = json['roomId'];
    title = json['title'];
    firstName = json['firstName'];
    lastName = json['lastName'];
    middleName = json['middleName'];
    phone = json['phone'];
    email = json['email'];
    imageUrl = json['imageUrl'];
    password = json['password'];
    id = json['id'];
    createBy = json['createBy'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['roomNo'] = this.roomNo;
    data['roomId'] = this.roomId;
    data['title'] = this.title;
    data['firstName'] = this.firstName;
    data['lastName'] = this.lastName;
    data['middleName'] = this.middleName;
    data['phone'] = this.phone;
    data['email'] = this.email;
    data['imageUrl'] = this.imageUrl;
    data['password'] = this.password;
    data['id'] = this.id;
    data['createBy'] = this.createBy;
    return data;
  }
}