class PrefixModel {
  String code;
  String name;
  bool status;
  String createBy;
  String createDate;
  String id;

  PrefixModel.fromJson(Map<String,dynamic> json):
  code =json['code'],
  name =json['name'],
  status =json['state'],
  createBy =json['createBy'],
  createDate =json['createDate'],
  id = json['id'];
}