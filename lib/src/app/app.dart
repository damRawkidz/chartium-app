import 'package:chartium/src/app/providers/id_provider.dart';
import 'package:chartium/src/app/providers/renter_profile_provider.dart';
import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/providers/work_order_provider.dart';
import 'package:chartium/src/app/screen/login_screen/login_screen.dart';
import 'package:chartium/src/app/screen/main/main_screen.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'screen/splash_screen/splash_screen.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  

  @override
  void initState()  {
    super.initState();

  }


  Widget build(BuildContext context) {
    return MultiProvider(
        providers: [
          ChangeNotifierProvider(builder: (context) => UserState()),
          ChangeNotifierProvider(builder: (context) => WorkOrderProvider()),
          ChangeNotifierProvider(builder: (context) => RenterProfileProvider(),),
          ChangeNotifierProvider(builder: (context) => UserId(),)
        ],
        child: MaterialApp(
          title: "Log me in",
          // home: Scaffold(
          //   body: SplashScreen()
          // ) ,
          initialRoute: '/',
          routes: {
            '/': (context) => SplashScreen(),
            '/workorder':(context) => MainScreen(page: 0),
            '/login': (context) => LoginScreen(),
          },
      )
    );
  }
}