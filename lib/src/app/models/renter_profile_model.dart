import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';


class RenterProfileModel {
  String arrivalDate;
  String departureDate;
  String propertyId;
  String renterRoomId;
  String propertyName;
  String prefixId;
  String prefixCode;
  String prefixName;
  String firstName;
  String surName;
  String middleName;
  String nationalityId;
  String nationalityCode;
  String nationality ;
  String passport;
  String dateOfBirth;
  String address1;
  String address2;
  bool status;
  String createDate;
  String lastUpdateDate;
  List<UserModel> masterAccountsList;

  String getFullName() {
    return '$firstName $middleName $surName';
  }


  RenterProfileModel.fromJson(Map<String,dynamic> json){
    arrivalDate = json['arrivalDate'];
    departureDate =json['departureDate'];
    propertyId =json['propertyId'];
    renterRoomId =json['renterRoomId'];
    propertyName =json['propertyName'];
    prefixId = json['prefixId'];
    prefixCode = json['prefixCode'];
    prefixName =json['prefixName'];
    firstName =json['firstName'];
    surName =json['surName'];
    middleName =json['middleName'];
    nationalityId =json['nationalityId'];
    nationalityCode =json['nationalityCode'];
    nationality =json['nationality'];
    passport =json['passport'];
    dateOfBirth =json['dateOfBirth'];
    address1 =json['address1'];
    address2 =json['address2'];
    status =json['status'];
    createDate =json['createDate'];
    lastUpdateDate =json['lastUpdateDate'];
    // masterAccountsList =(json['masterAccountsList'] as List).map((f) => UserModel.formJson(f)).toList();

  }
}