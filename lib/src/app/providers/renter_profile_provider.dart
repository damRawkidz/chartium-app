
import 'package:chartium/src/app/models/renter_profile_model.dart';
import 'package:flutter/foundation.dart';

class RenterProfileProvider extends ChangeNotifier {
  RenterProfileModel _renterProfile;

  RenterProfileModel getRenterProfile(){
    return _renterProfile;
  }

  void setRenterProfile(RenterProfileModel renterPrifle){
    _renterProfile =renterPrifle;
    notifyListeners();
  }
}