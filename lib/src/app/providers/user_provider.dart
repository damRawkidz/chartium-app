import 'package:chartium/src/app/screen/login_screen/models/userModel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/foundation.dart';



class UserState extends ChangeNotifier{
  UserModel _userState;
  String name;
  String token;
  String refreshToken;
  String tokenExpiryDate;
  String propertyName;
  String imageUrl;
  int id;
  

  UserModel getUser(){
    return _userState;
  }
  
  void restoreState(String _token, String _name,String url,String _id,String _pty) {
    token = _token;
    name = _name;
    imageUrl =url;
    propertyName =_pty;
    id = int.parse(_id);
    notifyListeners();
  }

  void updateImageUrl(String path){
      _userState.imageUrl = path;
      imageUrl =path;
      notifyListeners();
  }

  String getName(){
    return name;
  }
  
  String getUrlImage(){
    if (imageUrl != '') {
      return imageUrl.substring(1);
    }
    return '';
  }

  String getToken() {
    return token;
  }

  int getId(){
    return id;
  }

  String getPropertyName(){
    return propertyName;
  }

  void setUserModel(UserModel currentState){
    _userState = currentState;
    name = currentState.name;
    token = currentState.token;
    refreshToken =currentState.refreshToken;
    tokenExpiryDate =currentState.tokenExpiryDate;
    propertyName =currentState.propertyName;
    imageUrl = currentState.imageUrl;
    id =currentState.id;
    notifyListeners();
  }
}