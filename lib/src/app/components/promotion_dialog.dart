import 'package:flutter/material.dart';

class ProMotionDialog  extends StatefulWidget{

  ProMotionDialog();

  @override
  _proMotionDialogState createState() => _proMotionDialogState();
}

class _proMotionDialogState extends State<ProMotionDialog> {
  int QTY = 0;
  @override
  Widget build(BuildContext context) {
    return Dialog(      
        backgroundColor: Colors.transparent,
        child:Container(          
          height: 200,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white,
          ),
          child: Column(
            children: <Widget>[
              _HeaderText(context),
              _Content(),
            ],
          ),
        ),
    );
  }

  Widget _Content(){
    return Expanded(
      flex: 3,
      child: Container(
      child: Column(
        children: <Widget>[
          Container(            
            padding: EdgeInsets.all(5),
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                ContainerImage(),
                Expanded(
                     child: Text('sasdsadsadsadsadsadsadsadasdsadsadsadsadasdasdasdasdasdsad'),
                ),                
              ],
            ),
          ),
          Expanded(
            flex: 1,
            child: Container(
            padding: EdgeInsets.all(5),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                  Text('50 PTS'),                  
                  Container(
                    child: Row(
                      children: <Widget>[
                      Text('QTY $QTY'),
                      Container(margin:EdgeInsets.only(left: 10.0)),
                      ButtonTheme(
                        minWidth: 10,
                        buttonColor: Colors.grey,                    
                        child: RaisedButton(
                          child: Text('+'),
                          onPressed: (){
                            
                            setState(() {
                            QTY ++;  
                          });                      
                        },
                      )
                  ),
                      ButtonTheme(
                        minWidth: 10,
                        buttonColor: Colors.grey,
                        child: RaisedButton(
                          child: Text('-'),
                          onPressed: QTY == 0 ?  null :(){
                            setState(() {
                            QTY --;
                          });                      
                        },
                      )
                    ),
                      ],
                    ),
                  ),                  
              ],
          ) ,
          ),
          )          
        ],
      ),
    )
    );    
  }

  Widget ContainerImage(){
    String url = 'https://scontent.fbkk8-3.fna.fbcdn.net/v/t31.0-8/26171463_1833351416715230_2166791531763826436_o.jpg?_nc_cat=111&_nc_eui2=AeGHtlihyUvmjcDWUvraVAmljxQgJ_ZPMTryIHHpQ4RPSmJra-78NLRzz3OTOtHirOIURQEwvtbswjJ3JtuPBiaOW6ajp8EQ1DmNy_Y_CwvO6w&_nc_oc=AQnLEDE-CH08SzxuhJ26N8uv5RIrBzACrS4UVXzntv41EDV5VG4Uv7AMJh86MyAUHAY&_nc_ht=scontent.fbkk8-3.fna&oh=c1cba603a922d268c794ee9561927a29&oe=5DB16172';
      return  Container(
        margin: EdgeInsets.only(right: 5.0),
        child: SizedBox(
        height: 100,
        child: FittedBox(
          child: Image.network('$url'),
          fit: BoxFit.cover
        ),
      ),
      );    
  }

  Widget _HeaderText(context){
    return Expanded(
      flex: 1,
      child: Container(
        decoration: BoxDecoration(
            borderRadius: BorderRadius.only(
              topLeft:  Radius.circular(10.0),
              topRight:  Radius.circular(10.0)
            ),
            color: Color.fromRGBO(180, 151, 96, 1)
        ),
        // color: Colors.red,
        padding: EdgeInsets.all(5),
        child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Text('Your Redeem'),
          IconButton(            
            icon: Icon(Icons.close),            
            onPressed: (){
              Navigator.of(context).pop();
            },
          )
        ],
      ),
      )
    );
  }
}