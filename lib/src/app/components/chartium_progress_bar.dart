import 'package:flutter/material.dart';

class ChartiumProgressBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: CircularProgressIndicator(
            backgroundColor: Colors.white,
            valueColor: new AlwaysStoppedAnimation<Color>(Color.fromRGBO(180, 151, 96, 1),)
      ),
    );
  }

}