import 'dart:convert';

import 'package:chartium/src/app/providers/user_provider.dart';
import 'package:chartium/src/app/screen/main/main_screen.dart';
import 'package:chartium/src/app/screen/work_order/models-v2/work_order_c.dart';
import 'package:chartium/src/app/screen/work_order/work_order_container.dart';
import 'package:chartium/src/app/screen/work_order/work_order_screen/work_order_tab.dart';
import 'package:http/http.dart' as http;
import 'package:chartium/src/app/util/base-api.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';


class SaticfactionDailog extends StatefulWidget {
  WokrOrderC workOrder;
  SaticfactionDailog({@required this.workOrder });
  @override
  _SaticfactionDailogState createState() => _SaticfactionDailogState();
}



class _SaticfactionDailogState extends State<SaticfactionDailog> with BaseApi {
  int saticfactionValue = 1;


  @override
  void initState() { 
    super.initState();
    token = Provider.of<UserState>(context,listen: false).getToken();
  }
  
  @override
  Widget build(BuildContext context) {
    return Dialog(
      backgroundColor: Colors.transparent,
        child:Container(
          padding: EdgeInsets.all(10.0),
          height: 250,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10.0),
            color: Colors.white,
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: <Widget>[
              Center(child: Text('Assignment')),
              _saticfactionWidget(),
              _submitButton(),
            ],
          ),
        ),
    );
  }


  Widget _saticfactionWidget(){
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Column(
          children: <Widget>[
              IconButton(
                color: saticfactionValue == 1 ?  Colors.red :Colors.black,
                icon: Icon(Icons.sentiment_very_satisfied),                
                iconSize: 80.0,
                onPressed: (){                  
                  
                  setState(() {
                    this.saticfactionValue = 1;  
                  });
                  print(saticfactionValue);
                },
            ),
            // 1
            Text('Very Good'),
          ],
        ),
        Column(
          children: <Widget>[
            IconButton(
              color: saticfactionValue == 2 ?  Colors.red :Colors.black,
              icon: Icon(Icons.sentiment_neutral),
              iconSize: 80.0,
              onPressed: (){
                setState(() {
                    this.saticfactionValue = 2;  
                  });
              },
            ),
            Text('Good'),
          ],
        ),
        Column(
          children: <Widget>[
            IconButton(
              color: saticfactionValue == 3?  Colors.red :Colors.black,
              icon: Icon(Icons.sentiment_very_dissatisfied,),
              iconSize: 80.0,
              onPressed: (){
                setState(() {
                    this.saticfactionValue = 3;  
                  });
              },
            ),
            Text('Bad')
          ],
        )
      ],
    );
  }

  Widget _submitButton(){
    return  SizedBox(
          width: 100,
          child: RaisedButton(
                color: Colors.deepOrange,
                child: Text('Send',style: TextStyle(
                  color: Colors.white,
                ),),
            onPressed: () async {
              print(widget.workOrder.roomNo);
              await updateWorkOrder(context);
              Navigator.pushReplacement(
                context, 
                MaterialPageRoute(builder: (context) => MainScreen(page: 1,)),
                );              
            },
        ),
    );
  }
  updateWorkOrder(BuildContext context) async {
     String url = '${baseAPi}papi/workorder';
    Map data = {"apiRequest":{"action":"update"},"data": [{
            "accountId": widget.workOrder.accountId,
            "accountTitle": widget.workOrder.accountTitle,
            "accountFirstName": widget.workOrder.accountFirstName,
            "accountLastName": widget.workOrder.accountLastName,
            "roomId": widget.workOrder.roomId,
            "roomNo": widget.workOrder.roomNo,
            "scheduleDate":widget.workOrder.scheduleDate,
            "scheduleTime":widget.workOrder.scheduleTime,
            "workOrderStatusId": 5,
            "evaluation":saticfactionValue,
            "remark":widget.workOrder.remark,
            "workOrderItemId": widget.workOrder.workOrderItemId,
            "workOrderTypeId": widget.workOrder.workOrderTypeId,
            "workOrderNo": widget.workOrder.workOrderNo,
            "assignToId": widget.workOrder.assignToId,
            "requestDate": widget.workOrder.requestDate,
            "requestTime": widget.workOrder.requestTime,
            "issue":widget.workOrder.issue,
            "workOrderImages": widget.workOrder.workOrderImages.map((f) => {
              "workOrderId": f.workOrderId,
              "name": f.name,
              "id": f.id,
            }).toList(),           
            "id": widget.workOrder.id
    }]};
    // print(data);
    var body = json.encode(data);
    // print(body);
    var response = await http.post(url,
      body: body,
      headers: {
        "Content-Type": "application/json",
        "Authorization":"bearer "+token
        },
    );
    print(response.body);
    showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext context){
          return AlertDialog(            
            content: Text('${json.decode(response.body)['apiResponse']['desc']}'),
          );
        }
    );
      
   }
}