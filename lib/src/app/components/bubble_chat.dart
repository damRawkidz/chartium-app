import 'package:flutter/material.dart';

class BubbleChat extends StatelessWidget {
  String message;
  bool isSender;
  BubbleChat({Key key,@required this.message,@required this.isSender}):super(key:key);
  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.min,      
      mainAxisAlignment: isSender ? MainAxisAlignment.start : MainAxisAlignment.end,
      children: <Widget>[
        isSender ?   Container() : Expanded(flex: 1,child: Container(),),
        isSender ?  Container(
            margin: EdgeInsets.only(bottom: 10),
            width: 40,
            height: 40,
            decoration:  BoxDecoration(
                shape: BoxShape.circle,
                image:  DecorationImage(
                    fit: BoxFit.fill,
                    image:  AssetImage('assets/images/icon.jpg')
                    )
            )
          )
        :Container(),
        Flexible(
        flex: 1,
        fit: FlexFit.loose,
        child: Container(
            padding: EdgeInsets.all(5.0),
            margin: EdgeInsets.all(5.0),
            decoration: BoxDecoration(
              // color: isSender ?  Colors.red :Colors.grey,
              color: Colors.grey,
              borderRadius: BorderRadius.circular(10.0),
            ),
            child: Text('${this.message}'),
          ),
        ),
        isSender ?  Expanded(flex: 1,child: Container(),) : Container()
      ],
    );
  }
  
}