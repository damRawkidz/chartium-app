import 'package:flutter/material.dart';

class EmailIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Icon(
        Icons.email,
        color: Color.fromRGBO(180, 151, 96, 1),
        size: 15,
      );
  }  
}