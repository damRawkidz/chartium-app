import 'package:flutter/material.dart';

class FaxIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image.asset('assets/images/fax.webp');
  }
}