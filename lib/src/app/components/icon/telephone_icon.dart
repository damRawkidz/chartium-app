import 'package:flutter/material.dart';

class TelePhoneIcon extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Icon(
        Icons.phone,
        color: Color.fromRGBO(180, 151, 96, 1),
        size: 15,
      );
  }
  
}